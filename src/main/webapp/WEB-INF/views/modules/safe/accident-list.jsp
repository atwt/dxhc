<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="accidentList" url="accidentController.do?list" sortName="registerDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat"></t:gridCol>
			<t:gridCol title="驾驶员" field="driverName"></t:gridCol>
			<t:gridCol title="事故地点" field="location"></t:gridCol>
			<t:gridCol title="事故时间" field="accidentDate" dateFormat="yyyy-MM-dd hh:mm:ss" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="事故经过" field="detail" width="90" showLen="15"></t:gridCol>
			<t:gridCol title="状态" field="status" width="30" replace="未处理_0,已处理_1" align="center" query="true"
				style="background-color:#ed5565;color:#FFF;_0"></t:gridCol>
			<t:gridCol title="登记人" field="registerBy"></t:gridCol>
			<t:gridCol title="登记时间" field="registerDate" width="50" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="处理" funname="handle(id)" style="btn_blue" exp="status#eq#0" />
			<t:gridBar title="增加" icon="icon-add" url="accidentController.do?goAdd" funname="add"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="accidentController.do?goUpdate" funname="update" height="600"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="accidentController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="accidentController.do?goDetail" funname="detail" height="600"></t:gridBar>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'accidentController.do?goDetail', 'accidentList', 800, 600);
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		if (vehicleId != undefined) {
			return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId
					+ '\')>' + value + '</a>';
		}
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'accidentList', 800, 580)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'accidentList', 800, 450)
	}

	function handle(id) {
		createAddUpdateWindow('处理', 'accidentList',
				'accidentController.do?goHandle&id=' + id, 800, 600);
	}
</script>