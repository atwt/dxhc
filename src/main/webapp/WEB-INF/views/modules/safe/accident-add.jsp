<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	function vehicleChange() {
		var vehicleId = $("#vehicleId").val();
		if (vehicleId != "" && vehicleId != null && vehicleId != undefined) {
			var url = "accidentController.do?vehicleChange&vehicleId="
					+ vehicleId;
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						$('#driverName').val(d.attributes.driverName);
					}
				}
			});
		}
	}
</script>
</head>
<body>
	<t:form id="accidentAdd" action="accidentController.do?doAdd">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicle.id" type="hidden" /> <input name="vehicle.plateNumber"
					id="vehiclePlateNumber" class="inputxt" readonly="readonly" onclick="choose_vehiclePlateNumber()" datatype="*"
					nullmsg="请填写车辆" /> <t:gridSelect nameId="vehiclePlateNumber" url="vehicleController.do?select" valueId="vehicleId"
						gridField="plateNumber" gridName="vehicleSelectList" callback="vehicleChange();" title="车辆列表" height="458"
						width="800"></t:gridSelect></td>
				<td align="right"><label class="Validform_label">驾驶员:</label></td>
				<td class="value"><input id="driverName" name="driverName" type="text" class="inputxt" datatype="*"
					nullmsg="请填写驾驶员" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">事故地点:</label></td>
				<td class="value"><input name="location" type="text" class="inputxt" datatype="*" nullmsg="请填写事故地点" /></td>
				<td align="right"><label class="Validform_label">事故时间:</label></td>
				<td class="value"><input name="accidentDate" type="text" class="Wdate"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
					value='<fmt:formatDate value='${registerDate}' type="date" pattern="yyyy-MM-dd HH:mm:ss"/>' datatype="date"
					nullmsg="请填写事故时间"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">事故经过:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="detail" style="width: 570px; height: 100px;"></textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>