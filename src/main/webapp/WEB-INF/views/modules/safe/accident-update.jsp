<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	function vehicleChange() {
		var vehicleId = $("#vehicleId").val();
		if (vehicleId != "" && vehicleId != null && vehicleId != undefined) {
			var url = "accidentController.do?vehicleChange&vehicleId="
					+ vehicleId;
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						$('#driverName').val(d.attributes.driverName);
					}
				}
			});
		}
	}
</script>
</head>
<body>
	<t:form id="accidentUpdate" action="accidentController.do?doUpdate">
		<input name="id" type="hidden" value="${accidentPage.id }">
		<input name="tenantId" type="hidden" value="${accidentPage.tenantId}">
		<input name="status" type="hidden" value="${accidentPage.status}">
		<input name="registerBy" type="hidden" value="${accidentPage.registerBy}">
		<input name="registerDate" type="hidden" value="${accidentPage.registerDate}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicle.id" type="hidden" value="${accidentPage.vehicle.id}" />
					<input name="vehicle.plateNumber" id="vehiclePlateNumber" class="inputxt" readonly="readonly"
					onclick="choose_vehiclePlateNumber()" datatype="*" nullmsg="请填写车辆" value="${accidentPage.vehicle.plateNumber}" />
					<t:gridSelect nameId="vehiclePlateNumber" url="vehicleController.do?select" valueId="vehicleId"
						gridField="plateNumber" gridName="vehicleSelectList" callback="vehicleChange();" title="车辆列表" height="458"
						width="800"></t:gridSelect></td>
				<td align="right"><label class="Validform_label">驾驶员:</label></td>
				<td class="value"><input id="driverName" name="driverName" type="text" class="inputxt" datatype="*"
					nullmsg="请填写驾驶员" value="${accidentPage.driverName}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">事故地点:</label></td>
				<td class="value"><input name="location" type="text" class="inputxt" datatype="*" nullmsg="请填写事故地点"
					value="${accidentPage.location}" /></td>
				<td align="right"><label class="Validform_label">事故时间:</label></td>
				<td class="value"><input name="accidentDate" type="text" class="Wdate"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"
					value='<fmt:formatDate value='${accidentPage.accidentDate}' type="date" pattern="yyyy-MM-dd HH:mm:ss"/>'
					datatype="date" nullmsg="请填写事故时间"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">事故经过:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="detail" style="width: 565px; height: 100px;">${accidentPage.detail}</textarea></td>
			</tr>
			<c:if test="${accidentPage.status == 1}">
				<tr>
					<td align="right"><label class="Validform_label">事故性质:</label></td>
					<td class="value"><select name="type" datatype="*" nullmsg="请填写事故性质">
							<option value="1" <c:if test="${accidentPage.type eq 1}">selected="selected"</c:if>>轻微事故</option>
							<option value="2" <c:if test="${accidentPage.type eq 2}">selected="selected"</c:if>>一般事故</option>
							<option value="3" <c:if test="${accidentPage.type eq 3}">selected="selected"</c:if>>重大事故</option>
							<option value="4" <c:if test="${accidentPage.type eq 4}">selected="selected"</c:if>>特大事故</option>
					</select></td>
					<td align="right"><label class="Validform_label">责任比例:</label></td>
					<td class="value"><input name="responsible" type="text" class="inputxt" datatype="d" ignore="ignore"
						nullmsg="请填写责任比例" value="${accidentPage.responsible}">%</td>
				</tr>
				<tr>
					<td align="right"><label class="Validform_label">报赔金额:</label></td>
					<td class="value"><input name="applyAmount" type="text" class="inputxt"
						datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" ignore="ignore"
						nullmsg="请填写报赔金额" errormsg="请填写正确金额" value="${accidentPage.applyAmount}"></td>
					<td align="right"><label class="Validform_label">实赔金额:</label></td>
					<td class="value"><input name="realAmount" type="text" class="inputxt"
						datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" ignore="ignore"
						nullmsg="请填写实赔金额" errormsg="请填写正确金额" value="${accidentPage.realAmount}"></td>
				</tr>
				<tr>
					<td align="right"><label class="Validform_label">保险公司:</label></td>
					<td class="value"><t:dict id="insCompany" name="insCompany" type="select" groupCode="insCompany" datatype="*"
							defaultVal="${accidentPage.insCompany}"></t:dict></td>
					<td align="right"><label class="Validform_label">结案日期:</label></td>
					<td class="value"><input name="endDate" type="text" class="Wdate" onClick="WdatePicker()" datatype="date"
						value='<fmt:formatDate value='${accidentPage.endDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
				</tr>
				<tr>
					<td align="right"><label class="Validform_label">备注:</label></td>
					<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 565px; height: 50px;">${accidentPage.remarks}</textarea></td>
				</tr>
			</c:if>
		</table>
	</t:form>
</body>
</html>