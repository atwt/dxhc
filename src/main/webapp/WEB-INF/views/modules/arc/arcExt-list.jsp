<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/views/common/tags.jsp" %>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
    <div region="center">
        <t:grid name="arcExtList" url="arcExtController.do?list" sortName="type">
            <t:gridCol title="id" field="id" hidden="true"></t:gridCol>
            <t:gridCol title="名称" field="name" query="true"></t:gridCol>
            <t:gridCol title="应用范围 " field="type" query="true" replace="车辆档案_1,责任人档案_2,司机档案_3"></t:gridCol>
            <t:gridBar title="增加" icon="icon-add" url="arcExtController.do?goAdd" funname="add" width="500"
                       height="200"></t:gridBar>
            <t:gridBar title="修改" icon="icon-update" url="arcExtController.do?goUpdate" funname="update" width="500"
                       height="200"></t:gridBar>
            <t:gridBar title="删除" icon="icon-delete" url="arcExtController.do?doDel" funname="del"></t:gridBar>
        </t:grid>
    </div>
</div>
