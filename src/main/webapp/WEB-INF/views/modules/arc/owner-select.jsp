<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="ownerSelectList" url="ownerController.do?list" onLoadSuccess="selectRecord" pageSize="10">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="姓名" field="name" query="true"></t:gridCol>
			<t:gridCol title="性别" field="sex" width="40" replace="男_1,女_2" align="center"></t:gridCol>
			<t:gridCol title="手机" field="phone" query="true"></t:gridCol>
			<t:gridCol title="身份证" field="idCard" width="90"></t:gridCol>
			<t:gridCol title="地址" field="address" width="150"></t:gridCol>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function selectRecord(data) {
		var ids = "${param.ids}";
		if (ids != "" && ids != null && ids != undefined) {
			var idArray = ids.split(",");
			for (i = 0; i < idArray.length; i++) {
				$("#ownerSelectList").datagrid("selectRecord", idArray[i]);
			}
		}
	}
</script>