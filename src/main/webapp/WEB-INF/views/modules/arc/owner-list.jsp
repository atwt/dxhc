<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div id="ownerPanel" class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="ownerList" url="ownerController.do?list" onLoadSuccess="loadSuccess" sortName="createDate"
			onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="姓名" field="name" query="true"></t:gridCol>
			<t:gridCol title="性别" field="sex" replace="男_1,女_2" width="30"></t:gridCol>
			<t:gridCol title="手机" field="phone" query="true"></t:gridCol>
			<t:gridCol title="身份证" field="idCard" query="true"></t:gridCol>
			<t:gridCol title="地址" field="address" width="80"></t:gridCol>
			<t:gridCol title="备注" field="remarks" width="80"></t:gridCol>
			<t:gridCol title="状态" field="status" width="30" replace="正常_0,注销_-1" align="center" query="true"
				style="background-color:#ed5565;color:#FFF;_-1"></t:gridCol>
			<t:gridBar title="增加" icon="icon-add" url="ownerController.do?goAdd" funname="add" height="450"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="ownerController.do?goUpdate" funname="update" height="450"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="ownerController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="ownerController.do?goDetail" funname="detail" height="450"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="车辆" style="btn_green" funname="loadVehiclePanel(id,name)"></t:gridOpt>
		</t:grid>
	</div>
</div>
<div
	data-options="region:'east',
	title:'ownerList',
	collapsed:true,
	split:true,
	border:false,
	onExpand : function(){
		li_east = 1;
	},
	onCollapse : function() {
	    li_east = 0;
	}"
	style="width: 500px; overflow: hidden;" id="eastPanel">
	<div class="easyui-panel" style="padding: 0px; border: 0px" fit="true" border="false" id="vehiclePanel"></div>
</div>
<script type="text/javascript">
	$(function() {
		var li_east = 0;
	});

	function loadVehiclePanel(ownerId, ownerName) {
		var title = ownerName + ':';
		if (li_east == 0) {
			$('#ownerPanel').layout('expand', 'east');
		}
		$('#ownerPanel').layout('panel', 'east').panel('setTitle', title);
		$('#vehiclePanel').panel("refresh",
				"ownerController.do?ownerVehicleIndex&ownerId=" + ownerId);
	}

	function loadSuccess() {
		$('#ownerPanel').layout('panel', 'east').panel('setTitle', "");
		$('#ownerPanel').layout('collapse', 'east');
		$('#vehiclePanel').empty();
	}

	function onDblClick() {
		detail('查看', 'ownerController.do?goDetail', 'ownerList', 800, 500);
	}
</script>
