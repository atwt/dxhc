<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						//初始化
						var vehicleId = "${vehicleId}";
						var idArry = vehicleId.split(",");
						var vehiclePlateNumber = "${vehiclePlateNumber}";
						var plateNumberArry = vehiclePlateNumber.split(",");
						for (var i = 0; i < idArry.length; i++) {
							if (idArry[i] != "") {
								var newRow = '<tr><td><input type="hidden" value='+idArry[i]+' id="id"><span>'
										+ plateNumberArry[i]
										+ '</span></td><td><a href="#" onclick="del(this)">移除</a></td></tr>';
								$("#vehicleTable").append(newRow);
							}
						}
						//去除查询、 重置按钮
						$("#vehicleSelectListtb").find(
								"div[name='searchColums']").find(
								"form#vehicleSelectListForm").find("#queryBtn")
								.remove();
						$("#vehicleSelectListtb").find(
								"div[name='searchColums']").find(
								"form#vehicleSelectListForm").find("#resetBtn")
								.remove();
						//添加全部选择按钮
						$("#vehicleSelectListtb").find(
								"div[name='searchColums']").find(
								"form#vehicleSelectListForm").append(
								$("#tempSearchColums div[name='searchColums']")
										.html());
						$("#tempSearchColums").html('');
					});
</script>
<div class="easyui-layout" style="width: 1240px; height: 460px;">
	<div data-options="region:'center'">
		<t:grid name="vehicleSelectList" url="vehicleController.do?selectList" onClick="select" checkbox="false"
			pagination="false" sortName="sn" sortOrder="asc">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn"></t:gridCol>
			<t:gridCol title="车牌" field="plateNumber"></t:gridCol>
			<t:gridCol title="责任人" field="owner.name"></t:gridCol>
			<t:gridCol title="手机" field="owner.phone"></t:gridCol>
			<t:gridCol title="类型" field="type" replace="牵引车_1,挂车_2,整车_3" query="true" width="40" queryShowType="checkbox"
				align="center"></t:gridCol>
			<t:gridCol title="状态" field="status" dictGroup="vehicleStatus" dictExt="注销_-1" query="true" width="40" align="center"
				style="background-color:#ed5565;color:#FFF;_-1"></t:gridCol>
			<t:gridCol title="普货/危货" field="generalDanger" replace="普货_1,危货_2" query="true" width="40" align="center"></t:gridCol>
			<t:gridCol title="登记日期" field="registerDate" dateFormat="yyyy-MM-dd"></t:gridCol>
		</t:grid>
	</div>
	<div data-options="region:'east',split:true" title="已选择" style="width: 200px;">
		<table class="table-list" cellspacing="0" id="vehicleTable">
			<thead>
				<tr>
					<th width="100">车牌</th>
					<th><a href="javascript:deleteAll()">移除全部</a></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="vertical-align: middle; display: inline-block; width: 115px; text-align: right; text-overflow: ellipsis;"
			title="编号|车牌|责任人 ">编号|车牌|责任人:</span><span style="display: -moz-inline-box; display: inline-block;"><input
			name="noPlateNumberOwner" class="inputxt" type="text" width="60px" /> </span> <span
			style="float: right; padding-right: 5px;">
			<button id="queryBtn" class="btn btn-info" onclick="vehicleSelectListSearch()" type="button">查询</button>
			<button id="selectAllBtn" class="btn btn-primary" onclick="selectAll()" type="button">选择全部 >></button>
		</span>
	</div>
</div>
<script type="text/javascript">
	function select() {
		var rows = $("#vehicleSelectList").datagrid("getChecked");
		if (rows.length >= 1) {
			for (var i = 0; i < rows.length; i++) {
				var rowsName = rows[i]['plateNumber'];
				if (rows[i]['sn'] != "" && rows[i]['sn'] != null
						&& rows[i]['sn'] != undefined) {
					rowsName = rows[i]['sn'] + '-' + rowsName;
				}
				var rowsId = rows[i]['id'];
				if (check(rowsId)) {
					var newRow = '<tr><td><input type="hidden" value='+rowsId+' id="id"><span>'
							+ rowsName
							+ '</span></td><td><a href="#" onclick="del(this)">移除</a></td></tr>';
					$("#vehicleTable").append(newRow);
				}
			}

		}
	}
	function selectAll() {
		var rows = $("#vehicleSelectList").datagrid("getRows");
		if (rows.length >= 1) {
			for (var i = 0; i < rows.length; i++) {
				var rowsName = rows[i]['plateNumber'];
				if (rows[i]['sn'] != "" && rows[i]['sn'] != null
						&& rows[i]['sn'] != undefined) {
					rowsName = rows[i]['sn'] + '-' + rowsName;
				}
				var rowsId = rows[i]['id'];
				if (check(rowsId)) {
					var newRow = '<tr><td><input type="hidden" value='+rowsId+' id="id"><span>'
							+ rowsName
							+ '</span></td><td><a href="#" onclick="del(this)">移除</a></td></tr>';
					$("#vehicleTable").append(newRow);
				}
			}
		}
	}
	function check(rowsId) {
		var flag = true;
		$("#vehicleTable").find("tbody").find("tr").each(function() {
			if ($(this).find("input").val() == rowsId) {
				flag = false;
				return false;
			}
		});
		return flag;
	}

	function del(row) {
		var tr = row.parentNode.parentNode;
		var tbody = tr.parentNode;
		tbody.removeChild(tr);
	}

	function deleteAll() {
		$("#vehicleTable tbody").html("");
	}
</script>