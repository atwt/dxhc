<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="driverDetail">
		<input name="id" type="hidden" value="${driverPage.id }">
		<input name="tenantId" type="hidden" value="${driverPage.tenantId}">
		<input name="createDate" type="hidden" value="${driverPage.createDate}">
		<input name="createBy" type="hidden" value="${driverPage.createBy}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right" width="100"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input id="name" name="name" type="text" class="inputxt" value="${driverPage.name}" /></td>
				<td align="right" width="100"><label class="Validform_label">性别:</label></td>
				<td class="value"><select name="sex" id="sex" datatype="*">
						<option value="1" <c:if test="${driverPage.sex eq 1}">selected="selected"</c:if>>男</option>
						<option value="2" <c:if test="${driverPage.sex eq 2}">selected="selected"</c:if>>女</option>
				</select></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">身份证:</label></td>
				<td class="value"><input id="idCard" name="idCard" type="text" class="inputxt" value="${driverPage.idCard}" /></td>
				<td align="right" width="100"><label class="Validform_label">手机:</label></td>
				<td class="value"><input id="phone" name="phone" class="inputxt" type="text" value="${driverPage.phone}"></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">地址:</label></td>
				<td class="value"><input id="address" name="address" class="inputxt" type="text" value="${driverPage.address}"></td>
				<td align="right" width="100"><label class="Validform_label">地址:</label></td>
				<td class="value"><input id="remarks" name="remarks" class="inputxt" type="text" value="${driverPage.remarks}"></td>
			</tr>
		</table>
		<div style="width: 800px; height: 200px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="extTab" title="扩展信息" href="driverController.do?ext&id=${driverPage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>