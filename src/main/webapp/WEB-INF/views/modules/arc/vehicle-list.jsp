<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="vehicleList" url="vehicleController.do?list" sortName="sn" sortOrder="asc" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="plateNumber" query="true"></t:gridCol>
			<t:gridCol title="责任人" field="owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="类型" field="type" replace="牵引车_1,挂车_2,整车_3" query="true" queryShowType="checkbox" align="center"></t:gridCol>
			<t:gridCol title="普货/危货" field="generalDanger" query="true" replace="普货_1,危货_2" align="center"></t:gridCol>
			<t:gridCol title="登记日期" field="registerDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" dictGroup="vehicleStatus" dictExt="注销_-1" align="center"
				style="background-color:#ed5565;color:#FFF;_-1"></t:gridCol>
			<t:gridBar title="增加" icon="icon-add" url="vehicleController.do?goAdd" funname="add" height="600"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="vehicleController.do?goUpdate" funname="update" height="600"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="vehicleController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="vehicleController.do?goDetail" funname="detail" height="600"></t:gridBar>
			<t:gridBar title="注销" icon="icon-disable" url="vehicleController.do?goDisable" funname="update"></t:gridBar>
			<t:gridBar title="图片" icon="icon-pic" funname="goPic"></t:gridBar>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'vehicleController.do?goDetail', 'vehicleList', 800, 580);
	}

	function goPic() {
		var rows = $('#vehicleList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择车辆');
			return;
		}
		addTab('车辆图片——' + rows[0].plateNumber,
				'vehicleController.do?picIndex&vehicleId=' + rows[0].id);
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["owner.id"];
		return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
				+ value + '</a>';
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'vehicleWarnList', 800, 450)
	}

	//导出
	function exportXls() {
		excelExport("vehicleController.do?exportXls", "vehicleList");
	}
</script>
