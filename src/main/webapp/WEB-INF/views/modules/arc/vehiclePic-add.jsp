<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="vehiclePicAdd" action="vehicleController.do?doAddPic">
		<input id="vehicleId" name="vehicle.id" type="hidden" value="${vehiclePage.id}">
		<input name="vehicle.plateNumber" type="hidden" value="${vehiclePage.plateNumber}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车牌:</label></td>
				<td class="value"><input name="title" type="text" class="inputxt" value="${vehiclePage.plateNumber}"
					readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">类型:</label></td>
				<td class="value"><t:dict id="type" name="type" type="select" groupCode="picType" datatype="*"></t:dict></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">图片:</label></td>
				<td class="value"><link rel="stylesheet" type="text/css" href="plug-in/webuploader/custom.css"></link> <script
						type="text/javascript" src="plug-in/webuploader/webuploader.min.js"></script>
					<div id='vehicleuploader' class='wu-example'>
						<div id='vehiclePicList' class='uploader-list'></div>
						<div class='btns'>
							<div id='vehiclepicker' class="">选择文件</div>
						</div>
					</div>
					<div id='tempdiv_vehicle'></div> <script type="text/javascript">
						$(function() {
							var $list = $('#vehiclePicList');
							var vehicleId = $('#vehicleId').val();
							var uploader = WebUploader
									.create({
										swf : 'plug-in/webuploader/Uploader.swf',
										server : 'vehicleController.do?uploadPic&vehicleId='
												+ vehicleId,
										pick : '#vehiclepicker',
										duplicate : false,
										auto : true,
										fileNumLimit : 4,
										fileSingleSizeLimit : 5242880,
										accept : {
											extensions : 'gif,jpg,jpeg,bmp,png',
											mimeTypes : 'image/*'
										}
									});
							$('#vehiclepicker').find('div:eq(0)').addClass(
									'webuploader-pick btn-green btn-S');
							$('#tempdiv_vehicle').addClass('tempIMGdiv')
									.append('<ul></ul>');
							$list.append('<table class="temptable"></table>');
							var ratio = window.devicePixelRatio || 1;
							var thumbnailWidth = 440 * ratio;
							var thumbnailHeight = 610 * ratio;
							var vehicleaddImgli = function(src, name, xpath,
									flag) {
								var titleclass = 'hidetitle';//if(flag==1){titleclass='hidetitle';}
								var img = '<li><img name="' + name + 'img" class="tempimg" src="' + src + '"><div class="' + titleclass + '"><span';img+=' class="titledel">'
										+ xpath + '</span><span';
								img += xpath == 0 ? ' style="display:none;"'
										: ' ';
								img += ' class="titledown">' + xpath
										+ '</span></div></li>';
								$('#tempdiv_vehicle').find('ul').append(img);
							}
							var addtrFile = function(id, name, text, downsrc,
									delflag) {
								var trhtml = '<tr  style="display:none"  class="item" id="'+id+'"><td>'
										+ name
										+ '</td><td class="state">'
										+ text
										+ '</td><td class="icontd"><span';
								trhtml += downsrc == 0 ? ' style="display:none;"'
										: ' ';
								trhtml += ' class="down icon-down">'
										+ downsrc
										+ '</span></td><td class="icontd"><span';trhtml+=' class="del icon-cha" style="overflow:hidden;">'
										+ delflag
										+ '</span></td><td></td></tr>';
								$list.children('table').append(trhtml);
							}
							var vehicleaddFile = function(file, filepath) {
								uploader.makeThumb(file, function(error, src) {
									if (error) {
										return false;
									}
									vehicleaddImgli(src, file.id, 0, 0);
								}, thumbnailWidth, thumbnailHeight);
							}
							var updatetdState = function(id, content) {
								$list.children('table').find('#vehicle' + id)
										.find('.state').text(
												'--' + content + '--');
							}
							uploader.on('fileQueued', function(file) {
								var id = 'vehicle' + file.id;
								var name = file.name;
								var text = '--等待上传--';
								addtrFile(id, name, text, 0, 0);
							});
							uploader
									.on(
											'uploadSuccess',
											function(file, response) {
												if (response.success) {
													updatetdState(file.id,
															'上传成功');
													var path = response.attributes.path;
													var thumbPath = response.attributes.thumbPath;
													$(
															'#vehicle'
																	+ file.id
																	+ ' td:first')
															.append(
																	'<input type="hidden" name="path" value="'+path+'" /><input type="hidden" name="thumbPath" value="'+thumbPath+'" />');
													vehicleaddFile(file,
															thumbPath);
												} else {
													updatetdState(
															file.id,
															'上传出错'
																	+ response.msg);
												}
											});
							uploader.on('uploadError', function(file, reason) {
								updatetdState(file.id, '上传出错-code:' + reason);
							});
							uploader.on('error', function(type) {
								if (type == 'Q_TYPE_DENIED') {
									tip('文件类型不识别');
								}
								if (type == 'Q_EXCEED_NUM_LIMIT') {
									tip('文件数量超标');
								}
								if (type == 'F_DUPLICATE') {
									tip('相同文件请不要重复上传');
								}
								if (type == 'F_EXCEED_SIZE') {
									tip('单个文件大小超标');
								}
								if (type == 'Q_EXCEED_SIZE_LIMIT') {
									tip('文件大小超标');
								}
							});
							uploader.on('uploadComplete', function(file) {
								$('#vehicle' + file.id).find('.progress')
										.fadeOut('slow');
							});
							$('#tempdiv_vehicle').on(
									'mouseenter',
									'li',
									function() {
										$(this).find('.hidetitle').slideDown(
												500);
									});
							$('#tempdiv_vehicle')
									.on(
											'mouseleave',
											'li',
											function() {
												$(this).find('.hidetitle')
														.slideUp(500);
											});
							$('#tempdiv_vehicle')
									.on(
											'click',
											'span',
											function() {
												var optimgname = $(this)
														.parent('.hidetitle')
														.prev('img').attr(
																'name');
												var img_file_div = 'vehicle'
														+ optimgname
																.substring(
																		0,
																		optimgname
																				.indexOf('img'));
												$('#' + img_file_div).find(
														'.del')
														.trigger('click');
											});
							$list.on("click", ".del", function() {
								var itemObj = $(this).closest(".item");
								var id = itemObj.attr("id").substring(7);
								var delThumbPath = itemObj.find(
										"input[name='thumbPath']").val();
								var delPath = itemObj
										.find("input[name='path']").val();
								$.post('vehicleController.do?delPic', {
									thumbPath : delThumbPath,
									path : delPath,
								}, function(aj) {
									var data = JSON.parse(aj);
									if (data.success) {
										itemObj.remove();
										uploader.removeFile(id, true);
										var myimgli = $('#tempdiv_vehicle')
												.find(
														"img[name='" + id
																+ "img']")
												.closest('li');
										myimgli.off().find('.hidetitle').off()
												.end().remove();
									}
								});
							});
						});
					</script></td>
			</tr>
		</table>
	</t:form>
</body>
</html>
