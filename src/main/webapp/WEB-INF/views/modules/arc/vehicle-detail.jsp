<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(document).ready(function() {
		var type = $("#type").val()
		if (type == '1') {
			$("#trailer").show();
			$("#tractor").hide();
		} else if (type == '2') {
			$("#trailer").hide();
			$("#tractor").show();
		} else if (type == '3') {
			$("#trailer").hide();
			$("#tractor").hide();
		}
	});
</script>
</head>
<body>
	<t:form id="vehicleDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right" width="100"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="sn" type="text" class="inputxt" value="${vehiclePage.sn}" readonly="readonly" /></td>
				<td align="right" width="100"><label class="Validform_label">类型:</label></td>
				<td class="value"><select name="type" id="type" disabled="disabled">
						<option value="1" <c:if test="${vehiclePage.type eq 1}">selected="selected"</c:if>>牵引车</option>
						<option value="2" <c:if test="${vehiclePage.type eq 2}">selected="selected"</c:if>>挂车</option>
						<option value="3" <c:if test="${vehiclePage.type eq 3}">selected="selected"</c:if>>整车</option>
				</select></td>

			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">车牌:</label></td>
				<td class="value"><input name="plateNumber" type="text" class="inputxt" value="${vehiclePage.plateNumber}"
					readonly="readonly" /></td>

				<td align="right" width="100"><label class="Validform_label">状态:</label></td>
				<td class="value"><t:dict name="status" id="status" type="select" groupCode="vehicleStatus" extend="注销_-1"
						defaultVal="${vehiclePage.status}"></t:dict></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">普货/危货:</label></td>
				<td class="value"><select name="generalDanger" disabled="disabled">
						<option value="1" <c:if test="${vehiclePage.generalDanger eq 1}">selected="selected"</c:if>>普货</option>
						<option value="2" <c:if test="${vehiclePage.generalDanger eq 2}">selected="selected"</c:if>>危货</option>
				</select></td>
				<td align="right" width="100"><label class="Validform_label">登记日期:</label></td>
				<td class="value"><input name="registerDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${vehiclePage.registerDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="owner.name" value='${vehiclePage.owner.name}' class="inputxt"
					readonly="readonly" /></td>
				<td align="right" width="100"><label class="Validform_label">司机:</label></td>
				<td class="value"><input name="driverName" class="inputxt" readonly="readonly" value="${driverName}" /></td>
			</tr>
			<tr id="trailer">
				<td align="right" width="100"><label class="Validform_label">挂车:</label></td>
				<td class="value" colspan="3"><input name="relationVehiclePlateNumber" type="text" class="inputxt"
					readonly="readonly" value="${relationVehiclePlateNumber}" /></td>
			</tr>
			<tr id="tractor">
				<td align="right" width="100"><label class="Validform_label">牵引车:</label></td>
				<td class="value" colspan="3"><input name="relationVehiclePlateNumber" type="text" class="inputxt"
					readonly="readonly" value="${relationVehiclePlateNumber}" /></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 500px; height: 30px;"
						readonly="readonly">${vehiclePage.remarks }</textarea></td>
			</tr>
		</table>
		<div style="width: 800px; height: 300px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="extTab" title="扩展信息" href="vehicleController.do?ext&id=${vehiclePage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>