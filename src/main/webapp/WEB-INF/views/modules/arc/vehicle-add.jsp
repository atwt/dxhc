<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/views/common/tags.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>${fne:getConfig('title')}</title>
    <meta name="author" content="${fne:getConfig('author')}">
    <meta name="keywords" content="${fne:getConfig('keywords')}">
    <meta name="description" content="${fne:getConfig('description')}">
    <t:base></t:base>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#trailer").show();
            $("#tractor").hide();
        });

        function snChange() {
            var sn = $("#sn").val();
            var url = "vehicleController.do?snChange&sn=" + sn;
            $
                .ajax({
                    type: 'POST',
                    url: url,
                    success: function (data) {
                        var d = $.parseJSON(data);
                        if (d.success) {
                            $('#relationVehicleId').val(
                                d.attributes.relationVehicleId);
                            $('#relationVehiclePlateNumber').val(
                                d.attributes.relationVehiclePlateNumber);
                        }
                    }
                });
        }

        function ownerChange() {
            var ownerId = $("#ownerId").val();
            var url = "vehicleController.do?ownerChange&ownerId=" + ownerId;
            $.ajax({
                type: 'POST',
                url: url,
                success: function (data) {
                    var d = $.parseJSON(data);
                    if (d.success) {
                        $('#driverId').val(d.attributes.driverId);
                        $('#driverName').val(d.attributes.driverName);
                    }
                }
            });
        }

        function typeChange() {
            var type = $("#type").val();
            if (type == '1') {
                $("#relationVehicle").show();
                $("#trailer").show();
                $("#tractor").hide();
                var plateNumber = $("#plateNumber").val();
                plateNumber = plateNumber.replace("挂", "")
                $('#plateNumber').val(plateNumber);
            } else if (type == '2') {
                $("#relationVehicle").show();
                $("#trailer").hide();
                $("#tractor").show();
                var plateNumber = $("#plateNumber").val();
                if (plateNumber.charAt(plateNumber.length - 1) != "挂") {
                    plateNumber = plateNumber + "挂";
                    $('#plateNumber').val(plateNumber);
                }
            } else if (type == '3') {
                $("#relationVehicle").hide();
                $('#relationVehicleId').val("");
                $('#relationVehiclePlateNumber').val("");
                var plateNumber = $("#plateNumber").val();
                plateNumber = plateNumber.replace("挂", "")
                $('#plateNumber').val(plateNumber);
            }
            $("#plateNumber").focus();
        }

        $(function () {
            //车牌栏位有输入时触发的事件
            $("#plateNumber").keydown(function () {
                if (event.keyCode >= 65 && event.keyCode <= 90) {
                    var txt = "";
                    switch (event.keyCode) {
                        case 65:
                            txt = "A";
                            break;
                        case 66:
                            txt = "B";
                            break;
                        case 67:
                            txt = "C";
                            break;
                        case 68:
                            txt = "D";
                            break;
                        case 69:
                            txt = "E";
                            break;
                        case 70:
                            txt = "F";
                            break;
                        case 71:
                            txt = "G";
                            break;
                        case 72:
                            txt = "H";
                            break;
                        case 73:
                            txt = "I";
                            break;
                        case 74:
                            txt = "J";
                            break;
                        case 75:
                            txt = "K";
                            break;
                        case 76:
                            txt = "L";
                            break;
                        case 77:
                            txt = "M";
                            break;
                        case 78:
                            txt = "N";
                            break;
                        case 79:
                            txt = "O";
                            break;
                        case 80:
                            txt = "P";
                            break;
                        case 81:
                            txt = "Q";
                            break;
                        case 82:
                            txt = "R";
                            break;
                        case 83:
                            txt = "S";
                            break;
                        case 84:
                            txt = "T";
                            break;
                        case 85:
                            txt = "U";
                            break;
                        case 86:
                            txt = "V";
                            break;
                        case 87:
                            txt = "W";
                            break;
                        case 88:
                            txt = "X";
                            break;
                        case 89:
                            txt = "Y";
                            break;
                        case 90:
                            txt = "Z";
                            break;

                    }
                    $('#plateNumber').val($('#plateNumber').val() + txt);
                    window.event.keyCode = 0;
                    event.returnValue = false;
                }
            });
        });

        function addDict(groupCode) {
            createAddUpdateWindow('增加', 'dictList',
                'dictController.do?goAddCustom&groupCode=' + groupCode, 600,
                300);
        }

        function addOwner() {
            createAddUpdateWindow('增加', 'vehicleList', 'ownerController.do?goAdd',
                800, 500);
        }
    </script>
</head>
<body>
<t:form id="vehicleAdd" action="vehicleController.do?doAdd">
    <table cellpadding="0" cellspacing="1" class="formtable">
        <tr>
            <td align="right" width="100"><label class="Validform_label">编号:</label></td>
            <td class="value"><input id="sn" name="sn" type="text" class="inputxt" onchange="snChange()"/></td>
            <td align="right" width="100"><label class="Validform_label">类型:</label></td>
            <td class="value"><select name="type" id="type" datatype="*" onchange="typeChange()">
                <option value="1">牵引车</option>
                <option value="2">挂车</option>
                <option value="3">整车</option>
            </select></td>
        </tr>
        <tr>
            <td align="right" width="100"><label class="Validform_label">车牌:</label></td>
            <td class="value"><input id="plateNumber" name="plateNumber" type="text" class="inputxt"
                                     value="${licensePlate}"
                                     datatype="/^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂]{1}$/"
                                     ajaxurl="vehicleController.do?validPlateNumber" nullmsg="请填写车牌号码"
                                     errormsg="请填写正确车牌号码"/></td>
            <td align="right" width="100"><label class="Validform_label">普货/危货:</label></td>
            <td class="value"><select name="generalDanger" id="generalDanger" datatype="*">
                <option value="1">普货</option>
                <option value="2">危货</option>
            </select></td>
        </tr>
        <tr>
            <td align="right" width="100"><label class="Validform_label">状态:</label></td>
            <td class="value"><t:dict id="status" name="status" type="select" groupCode="vehicleStatus" extend="注销_-1"
                                      datatype="*"></t:dict><a onclick="addDict('vehicleStatus');"><span
                    style="font-size: 12px; margin-left: 5px; color: #c1b7b7">&nbsp;增加</span></a></td>
            <td align="right" width="100"><label class="Validform_label">登记日期:</label></td>
            <td class="value"><input id="registerDate" name="registerDate" type="text" class="Wdate"
                                     onClick="WdatePicker()"></td>
        </tr>
        <tr>
            <td align="right" width="100"><label class="Validform_label">责任人:</label></td>
            <td class="value"><input id="ownerId" name="owner.id" type="hidden"/> <input name="owner.name"
                                                                                         id="ownerName"
                                                                                         class="inputxt"
                                                                                         readonly="readonly"
                                                                                         onclick="choose_ownerName()"
                                                                                         datatype="*"/> <t:gridSelect
                    nameId="ownerName"
                    url="ownerController.do?select" valueId="ownerId" gridField="name" gridName="ownerSelectList"
                    callback="ownerChange" title="责任人" height="459" width="800"></t:gridSelect><a onclick="addOwner();"><span
                    style="font-size: 12px; margin-left: 5px; color: #c1b7b7">增加</span></a></td>
            <td align="right" width="100"><label class="Validform_label">司机:</label></td>
            <td class="value"><input id="driverId" name="driverId" type="hidden"/> <input name="driverName"
                                                                                          id="driverName"
                                                                                          class="inputxt"
                                                                                          readonly="readonly"
                                                                                          onclick="driverSelect()"/>
            </td>
        </tr>
        <tr id="relationVehicle">
            <td align="right" width="100"><label class="Validform_label"><span id="trailer">挂车:</span><span
                    id="tractor">牵引车:</span></label></td>
            <td class="value" colspan="3"><input id="relationVehicleId" name="relationVehicleId" type="hidden"/> <input
                    name="relationVehiclePlateNumber" id="relationVehiclePlateNumber" class="inputxt"
                    readonly="readonly"
                    onclick="relationVehicleSelect()"/></td>
        </tr>
        <tr>
            <td align="right" width="100"><label class="Validform_label">备注:</label></td>
            <td class="value" colspan="3"><textarea class="inputxt" id="remarks" name="remarks"
                                                    style="width: 580px; height: 30px;"></textarea></td>
        </tr>
    </table>
    <div style="width: 800px; height: 300px;">
        <div id="tt" border="false" class="easyui-tabs" fit="true">
            <div id="warnTab" title="到期提醒" href="vehicleController.do?warn"></div>
            <div id="extTab" title="扩展信息" href="vehicleController.do?ext"></div>
        </div>
    </div>
</t:form>
<script type="text/javascript">
    function driverSelect() {
        var driverId = $("#driverId").val();
        var driverName = $("#driverName").val();
        var url = 'driverController.do?selectMore&driverId=' + driverId
            + "&driverName=" + driverName;
        $.dialog.setting.zIndex = getzIndex();
        $.dialog({
            content: 'url:' + encodeURI(url),
            zIndex: getzIndex(),
            title: '司机列表',
            lock: true,
            width: '900px',
            height: '460px',
            opacity: 0.4,
            button: [{
                name: '确定',
                callback: callbackDriverSelect,
                focus: true
            }, {
                name: '取消',
                callback: function () {
                }
            }]
        }).zindex();
    }

    function callbackDriverSelect() {
        var iframe = this.iframe.contentWindow;
        var table = iframe.$("#driverTable");
        var driverId = '', driverName = '';
        $(table).find("tbody tr").each(function () {
            driverId += $(this).find("input").val() + ",";
            driverName += $(this).find("span").text() + ",";

        })
        $("#driverName").val(driverName);
        $("#driverName").blur();
        $("#driverId").val(driverId);
    }
</script>
<script type="text/javascript">
    function relationVehicleSelect() {
        var ownerId = $('#ownerId').val();
        var type = $('#type').val();
        if (type == '1') {
            type = '2';
        } else if (type == '2') {
            type = '1';
        }
        var url = 'vehicleController.do?select&ownerId=' + ownerId
            + '&type=' + type;
        var initValue = $('#relationVehicleId').val();
        url += '&ids=' + initValue;
        $.dialog({
            content: 'url:' + url,
            zIndex: getzIndex(),
            title: '车辆列表',
            lock: true,
            parent: frameElement.api,
            width: '800',
            height: '459',
            left: '50%',
            top: '50%',
            opacity: 0.4,
            button: [{
                name: '确定',
                callback: callbackRelationVehicleSelect,
                focus: true
            }, {
                name: '取消',
                callback: function () {
                }
            }]
        });
    }

    function callbackRelationVehicleSelect() {
        iframe = this.iframe.contentWindow;
        var relationVehiclePlateNumber = iframe
            .getvehicleSelectListSelections('plateNumber');
        $('#relationVehiclePlateNumber').val(relationVehiclePlateNumber);
        var ids = iframe.getvehicleSelectListSelections('id');
        $('#relationVehicleId').val(ids);
        $('#relationVehiclePlateNumber').blur();
    }
</script>
</body>
</html>