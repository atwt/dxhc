<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="vehiclePicList" url="vehicleController.do?picList&vehicleId=${vehicleId}" sortName="createDate"
			pageSize="4">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="url" field="url" hidden="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" width="30"></t:gridCol>
			<t:gridCol title="类型" field="type" query="true" dictGroup="picType" align="center" width="30"></t:gridCol>
			<t:gridCol title="图片" field="thumbUrl" formatter="imgFormatter" sortable="false"></t:gridCol>
			<t:gridCol title="上传日期" field="createDate" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridBar title="上传" icon="icon-add" url="vehicleController.do?goAddPic&vehicleId=${vehicleId}" funname="add"
				width="980" height="500"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="vehicleController.do?doDelPic" funname="del"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="40"></t:gridCol>
			<t:gridOpt title="查看" style="btn_green" funname="viewPic(url)" />
			<t:gridOpt title="下载" style="btn_blue" funname="downloadPic(id)" />
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function imgFormatter(value, rec, index) {
		return '<img width="100" height="138" border="0"  onMouseOver="tipImg(this)" onMouseOut="moveTipImg()"  src="'
				+ value + '"/>'
	}

	function viewPic(url) {
		window.open(url);
	}

	function downloadPic(id) {
		window.location.href = 'vehicleController.do?downloadPic&id=' + id;
	}
</script>
