<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="ownerDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right" width="100"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input name="name" type="text" class="inputxt" value="${ownerPage.name}" readonly="readonly" /></td>
				<td align="right" width="100"><label class="Validform_label">性别:</label></td>
				<td class="value"><select name="sex" disabled="disabled">
						<option value="1" <c:if test="${ownerPage.sex eq 1}">selected="selected"</c:if>>男</option>
						<option value="2" <c:if test="${ownerPage.sex eq 2}">selected="selected"</c:if>>女</option>
				</select></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">身份证:</label></td>
				<td class="value"><input name="idCard" type="text" class="inputxt" value="${ownerPage.idCard}"
					readonly="readonly" /></td>
				<td align="right" width="100"><label class="Validform_label">手机:</label></td>
				<td class="value"><input name="phone" class="inputxt" type="text" value="${ownerPage.phone}"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">地址:</label></td>
				<td class="value"><input name="address" class="inputxt" type="text" value="${ownerPage.address}"
					readonly="readonly"></td>
				<td align="right" width="100"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" class="inputxt" type="text" value="${ownerPage.remarks}"
					readonly="readonly"></td>
			</tr>
		</table>
		<div style="width: 800px; height: 200px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="ext" title="扩展信息" href="ownerController.do?ext&type=2&id=${ownerPage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>