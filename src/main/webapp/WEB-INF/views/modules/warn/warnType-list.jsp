<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="warnTypeList" url="warnTypeController.do?list">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="名称" field="name" query="true"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="启用_0,禁用_1" style="background-color:#ed5565;color:#FFF;_1"
				align="center" width="30"></t:gridCol>
			<t:gridBar title="增加" icon="icon-add" url="warnTypeController.do?goAdd" funname="add" width="500" height="200"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="warnTypeController.do?goUpdate" funname="update" width="500"
				height="200"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="warnTypeController.do?doDel" funname="del"></t:gridBar>
		</t:grid>
	</div>
</div>
