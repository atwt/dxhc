<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script>
	$(function() {
		//初始化
		$("#insuranceWarnListtb").find("div[name='searchColums']").find(
				"form#insuranceWarnListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
		//事件绑定
		$("input[name='endDate_begin']").attr("onchange",
				"endDateBeginChange()");
		$("input[name='endDate_end']").attr("onchange", "endDateEndChange()");
		$("input[name='vehicle.sn']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("input[name='vehicle.plateNumber']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("input[name='vehicle.owner.name']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("select[name='type']").attr("onchange", "typeInsCompanyChange()");
		$("select[name='insCompany']").attr("onchange",
				"typeInsCompanyChange()");
	});
</script>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="insuranceWarnList" url="insuranceWarnController.do?list" sortName="endDate" sortOrder="asc">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" width="40" align="center"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true" formatter="ownerNameFormat" sortable="false"></t:gridCol>
			<t:gridCol title="手机" field="vehicle.owner.phone" sortable="false"></t:gridCol>
			<t:gridCol title="类型" field="type" width="40" query="true" replace="交强险_1,商业险_2" align="center"></t:gridCol>
			<t:gridCol title="保险公司" field="insCompany" query="true" dictGroup="insCompany"></t:gridCol>
			<t:gridCol title="保险状态" field="status" width="40" replace="有效_0,无效_1" align="center"></t:gridCol>
			<t:gridCol title="生效日期" field="startDate" width="50" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridCol title="到期日期" field="endDate" width="50" dateFormat="yyyy-MM-dd" query="true" queryMode="group"
				style="color:red"></t:gridCol>
			<t:gridCol title="提醒状态" field="warn" width="30" replace="正常_0,暂停_1" align="center"
				style="background-color:#ed5565;color:#FFF;_1"></t:gridCol>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="短信" funname="send(id)" style="btn_green" />
			<t:gridBar title="启动提醒" icon="icon-run" url="vehicleWarnController.do?doStart" funname="start"></t:gridBar>
			<t:gridBar title="暂停提醒" icon="icon-pause" url="vehicleWarnController.do?doPause" funname="pause"></t:gridBar>
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><select id="warnDateType" name="warnDateType"
			style="width: 90px" onchange="warnDateTypeChange()">
				<option value="1">下月到期</option>
				<option value="2">自定义</option>
		</select></span><span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 68px; text-align: right; text-overflow: ellipsis;"
			title="提醒状态">提醒状态:</span><select name="ws" style="width: 104px">
				<option value="0">正常</option>
				<option value="1">暂停</option>
				<option value="-1">全部</option>
		</select></span>
	</div>
</div>
<script type="text/javascript">
	function snPlateNumberOwnerChange() {
		var sn = $("input[name='vehicle.sn']").val();
		var plateNumber = $("input[name='vehicle.plateNumber']").val();
		var owner = $("input[name='vehicle.owner.name']").val();
		if (isNotBlank(sn) || isNotBlank(plateNumber) || isNotBlank(owner)) {
			$("#warnDateType").val('2')
			$("select[name='ws']").val('-1');
		}
	}

	function typeInsCompanyChange() {
		$("#warnDateType").val('2')
	}

	function endDateBeginChange() {
		var warnDateBegin = $("input[name='endDate_begin']").val();
		if (warnDateBegin.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function endDateEndChange() {
		var warnDateEnd = $("input[name='endDate_end']").val();
		if (warnDateEnd.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function warnDateTypeChange() {
		var warnDateType = $("#warnDateType").val();
		if (warnDateType == '1') {
			$("input[name='endDate_begin']").val('');
			$("input[name='endDate_end']").val('');
			insuranceWarnListSearch();
		}
	}

	function start() {
		var rows = $('#insuranceWarnList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择启动提醒');
			return;
		}
		createDialog('启动确认', '确定启动提醒 ?',
				'insuranceWarnController.do?doStart&id=' + rows[0].id,
				'insuranceWarnList');
	}

	function pause() {
		var rows = $('#insuranceWarnList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择暂停提醒');
			return;
		}
		createDialog('暂停确认', '确定暂停提醒 ?',
				'insuranceWarnController.do?doPause&id=' + rows[0].id,
				'insuranceWarnList');
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId + '\')>'
				+ value + '</a>';
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'insuranceWarnList', 800, 580)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
				+ value + '</a>';
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'insuranceWarnList', 800, 500)
	}

	function send(id) {
		createAddUpdateWindow('发送短信', 'insuranceWarnList',
				'insuranceWarnController.do?goSendSms&id=' + id, 800, 500);
	}

	function isNotBlank(value) {
		if (value != '' && value != null && value != undefined) {
			return true;
		}
		return false;
	}
</script>