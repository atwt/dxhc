<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="vehicleWarnHandle" action="vehicleWarnController.do?doHandle">
		<input name="id" type="hidden" value="${vehicleWarnPage.id }">
		<input name="tenantId" type="hidden" value="${vehicleWarnPage.tenantId}" />
		<input name="vehicle.id" type="hidden" value="${vehicleWarnPage.vehicle.id}" />
		<input name="warnType.id" type="hidden" value="${vehicleWarnPage.warnType.id}" />
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="vehicle.sn" type="text" class="inputxt" value="${vehicleWarnPage.vehicle.sn}"
					readonly="readonly" /></td>
				<td align="right"><label class="Validform_label">车牌:</label></td>
				<td class="value"><input name="vehicle.plateNumber" type="text" class="inputxt"
					value="${vehicleWarnPage.vehicle.plateNumber}" readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">提醒类型:</label></td>
				<td class="value"><input name="warnType.name" type="text" class="inputxt"
					value="${vehicleWarnPage.warnType.name}" readonly="readonly" /></td>
				<td align="right"><label class="Validform_label">到期日期:</label></td>
				<td class="value"><input name="warnDate" type="text" class="inputxt"
					value='<fmt:formatDate value='${vehicleWarnPage.warnDate}' type="date" pattern="yyyy-MM-dd"/>' readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label" style="color: red">下次到期:</label></td>
				<td class="value" colspan="3"><input name="nextDate" type="text" class="Wdate"
					onClick="WdatePicker({minDate:'%y-%M-{%d+1}'})" datatype="date"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>