<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="warnTypeAdd" action="warnTypeController.do?doAdd">
		<table style="width: 500px;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">名称:</label></td>
				<td class="value"><input name="name" type="text" class="inputxt" datatype="*" nullmsg="请填写名称" /></td>
			</tr>
		</table>
	</t:form>
</body>
</html>