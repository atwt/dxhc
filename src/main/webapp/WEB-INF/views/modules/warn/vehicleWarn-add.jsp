<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="vehicleWarnAdd" action="vehicleWarnController.do?doAdd" tiptype="1">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicleId" type="hidden" /> <input name="vehiclePlateNumber"
					id="vehiclePlateNumber" class="inputxt" readonly="readonly" onclick="vehicleSelect()" datatype="*"
					style="width: 400px;" /></td>
			</tr>
		</table>
		<div style="width: 800px; height: 450px;" id="warnTab">
			<div border="false" class="easyui-tabs" fit="true">
				<div title="提醒明细" href="vehicleWarnController.do?tab"></div>
			</div>
		</div>
	</t:form>
	<!-- 添加 明细 模版 -->
	<table style="display: none" id="warnListTable_template">
		<tbody>
			<tr>
				<td align="center"><div style="width: 35px;" name="xh"></div></td>
				<td align="center"><input style="width: 50px;" type="checkbox" name="ck" /></td>
				<td align="center"><select name="warnList[#index#].warnType.id" datatype="*" nullmsg="请填写提醒类型"
					style="width: 180px;">
						<c:forEach items="${warnTypeList}" var="warnType">
							<option value="${warnType.id}">${warnType.name}</option>
						</c:forEach>
				</select></td>
				<td align="center"><input name="warnList[#index#].warnDate" type="text" class="Wdate" onClick="WdatePicker()"
					datatype="date" nullmsg="请填写到期日期" style="width: 180px;"></td>
			</tr>
		</tbody>
	</table>
</body>
<script type="text/javascript">
	function vehicleSelect() {
		var vehicleId = $("#vehicleId").val();
		var vehiclePlateNumber = $("#vehiclePlateNumber").val();
		var url = 'vehicleController.do?selectMore&vehicleId=' + vehicleId
				+ "&vehiclePlateNumber=" + vehiclePlateNumber;
		$.dialog.setting.zIndex = getzIndex();
		$.dialog({
			content : 'url:' + encodeURI(url),
			zIndex : getzIndex(),
			title : '车辆列表',
			lock : true,
			width : '1240px',
			height : '460px',
			opacity : 0.4,
			button : [ {
				name : '确定',
				callback : callbackVehicleSelect,
				focus : true
			}, {
				name : '取消',
				callback : function() {
				}
			} ]
		}).zindex();
	}
	function callbackVehicleSelect() {
		var iframe = this.iframe.contentWindow;
		var table = iframe.$("#vehicleTable");
		var vehicleId = '', vehiclePlateNumber = '';
		$(table).find("tbody tr").each(function() {
			vehicleId += $(this).find("input").val() + ",";
			vehiclePlateNumber += $(this).find("span").text() + ",";

		})
		$("#vehiclePlateNumber").val(vehiclePlateNumber);
		$("#vehiclePlateNumber").blur();
		$("#vehicleId").val(vehicleId);
	}
</script>
</html>