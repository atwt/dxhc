<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="insuranceList" url="insuranceController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" width="40" align="center"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="类型" field="type" width="40" query="true" replace="交强险_1,商业险_2" align="center"></t:gridCol>
			<t:gridCol title="保费" field="premium" align="right"></t:gridCol>
			<t:gridCol title="车船税" field="vavt" align="right"></t:gridCol>
			<t:gridCol title="保险公司" field="insCompany" query="true" dictGroup="insCompany"></t:gridCol>
			<t:gridCol title="到期日期" field="endDate" width="50" dateFormat="yyyy-MM-dd" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="保险状态" field="status" width="30" replace="有效_0,无效_1" align="center"
				style="background-color:#ed5565;color:#FFF;_1"></t:gridCol>
			<t:gridBar title="增加" icon="icon-add" url="insuranceController.do?goAdd" funname="add" height="600"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="insuranceController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="insuranceController.do?goDetail" funname="detail" height="600"></t:gridBar>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'insuranceController.do?goDetail', 'insuranceList', 800,
				600);
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		if (vehicleId != undefined) {
			return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId
					+ '\')>' + value + '</a>';
		}
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'insuranceWarnList', 800, 600)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'insuranceWarnList', 800, 600)
	}
</script>