<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="loanList" url="loanController.do?list" sortName="createDate">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="loanContractId" field="loanContract.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="loanContract.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true" width="90" formatter="loanContractFormat"></t:gridCol>
			<t:gridCol title="期数" field="period" width="30"></t:gridCol>
			<t:gridCol title="责任人" field="loanContract.owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="应还本金 " field="principal" align="right"></t:gridCol>
			<t:gridCol title="应还利息 " field="interest" align="right"></t:gridCol>
			<t:gridCol title="应还总额 " field="totalAmount" align="right"></t:gridCol>
			<t:gridCol title="实还本金 " field="realPrincipal" align="right"></t:gridCol>
			<t:gridCol title="实还利息 " field="realInterest" align="right"></t:gridCol>
			<t:gridCol title="实还总额 " field="realTotalAmount" align="right"></t:gridCol>
			<t:gridCol title="尚欠金额" field="oweAmount" align="right"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="未还款_0,还款中_1,已还完_2"
				style="background-color:#f8ac59;color:#FFF;_1,background-color:#ed5565;color:#FFF;_0" align="center" width="40"></t:gridCol>
			<t:gridCol title="还款日" field="endDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group"></t:gridCol>
			<t:gridBar title="修改" icon="icon-update" url="loanController.do?goUpdate" funname="update"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="35" align="center"></t:gridCol>
			<t:gridOpt title="还款" funname="repayment(id)" style="btn_blue" exp="status#ne#2" />
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function update() {
		var rows = $('#loanList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '2') {
			tip('费用已还完,无法修改');
			return;
		}
		createAddUpdateWindow('修改', 'loanList',
				'loanController.do?goUpdate&id=' + rows[0].id, 800, 500);
	}

	function repayment(id) {
		createAddUpdateWindow('还款', 'loanList',
				'loanController.do?goRepayment&id=' + id, 800, 500);
	}

	function loanContractFormat(value, rec, index) {
		var loanContractId = rec["loanContract.id"];
		if (loanContractId != undefined) {
			return '<a href=\'#\' onclick=loanContractDetail(\''
					+ loanContractId + '\')>' + value + '</a>';
		}
	}

	function loanContractDetail(id) {
		openwindow('查看', 'loanContractController.do?goDetail&id=' + id,
				'loanList', 800, 700)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["loanContract.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id, 'loanList',
				800, 500)
	}
</script>

