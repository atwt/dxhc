<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<style>
.loanTable {
	margin: 0 auto;
	border-collapse: collapse;
}

.loanTable tr:nth-child(even) {
	background: #f5f5f5;
}

.loanThead {
	background: #EEEEEE;
	display: block
}

.loanTbody {
	height: 445px;
	overflow-y: scroll;
	display: block
}

.td30 {
	width: 30px;
	min-width: 30px;
	max-width: 30px;
}

.td60 {
	width: 60px;
	min-width: 60px;
	max-width: 60px;
}

.td100 {
	width: 100px;
	min-width: 100px;
	max-width: 100px;
}

.td110 {
	width: 110px;
	min-width: 110px;
	max-width: 110px;
}
</style>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="4" cellspacing="0" class="loanTable">
		<thead class="loanThead">
			<tr bgcolor="#E6E6E6">
				<td align="center" class="td30"><label class="Validform_label">期数</label></td>
				<td align="center" class="td100"><label class="Validform_label">应还本金</label></td>
				<td align="center" class="td100"><label class="Validform_label">应还利息</label></td>
				<td align="center" class="td100"><label class="Validform_label">实还本金</label></td>
				<td align="center" class="td100"><label class="Validform_label">实还利息</label></td>
				<td align="center" class="td100"><label class="Validform_label">尚欠金额</label></td>
				<td align="center" class="td60"><label class="Validform_label">状态</label></td>
				<td align="center" class="td110"><label class="Validform_label">还款日</label></td>
			</tr>
		</thead>
		<tbody id="loanListTable" class="loanTbody">
			<c:forEach items="${loanList}" var="loan" varStatus="stuts">
				<tr>
					<td align="center" class="td30"><c:out value="${loan.period}"></c:out></td>
					<td align="center" class="td100"><c:out value="${loan.principal}"></c:out></td>
					<td align="center" class="td100"><c:out value="${loan.interest}"></c:out></td>
					<td align="center" class="td100"><c:out value="${loan.realPrincipal}"></c:out></td>
					<td align="center" class="td100"><c:out value="${loan.realInterest}"></c:out></td>
					<td align="center" class="td100"><c:out value="${loan.oweAmount}"></c:out></td>
					<td align="center" class="td60"><c:if test="${loan.status eq 0}">
							<span style="background-color: #ed5565; color: #FFF;">未还款</span>
						</c:if> <c:if test="${loan.status eq 1}">
							<span style="background-color: #f8ac59; color: #FFF;">还款中</span>
						</c:if> <c:if test="${loan.status eq 2}">已还完</c:if></td>
					<td align="center" class="td110"><c:out value="${fne:getDate(loan.endDate)}"></c:out></td>
				</tr>
			</c:forEach>
			<tr>
				<td style="font-size: 12px; font-weight: 600;" align="center" class="td30">总计:</td>
				<td style="color: red; font-weight: 700;" class="td100" align="center">${loanContractPage.principal}</td>
				<td style="color: red; font-weight: 700;" class="td100" align="center">${loanContractPage.interest}</td>
				<td style="color: red; font-weight: 700;" class="td100" align="center">${loanContractPage.realPrincipal}</td>
				<td style="color: red; font-weight: 700;" class="td100" align="center">${loanContractPage.realInterest}</td>
				<td style="color: red; font-weight: 700;" class="td100" align="center">${loanContractPage.oweAmount}</td>
			</tr>
		</tbody>
	</table>
</div>

