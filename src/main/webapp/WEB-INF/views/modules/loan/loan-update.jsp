<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="loanUpdate" action="loanController.do?doUpdate">
		<input type="hidden" name="id" value="${loanPage.id}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="sn" type="text" class="inputxt" value="${loanPage.sn}" readonly="readonly"></td>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="ownerName" type="text" class="inputxt"
					value="${loanPage.loanContract.owner.name}" readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">应还本金:</label></td>
				<td class="value"><input name="principal" type="text" class="inputxt" value="${loanPage.principal}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">应还利息:</label></td>
				<td class="value"><input name="interest" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写利息"
					errormsg="请填写正确金额" value="${loanPage.interest}"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>