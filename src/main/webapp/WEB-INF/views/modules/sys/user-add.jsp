<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base />
<script type="text/javascript">
	function phoneChange() {
		var phone = $('#phone').val();
		var url = "userController.do?phoneChange&phone=" + phone;
		$.ajax({
			type : 'POST',
			url : url,
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.attributes.exist == 'yes') {
					$("#name").val(d.attributes.userName);
					$("#password").val('tempPassword');
					$("#repassword").val('tempPassword');
					$("#nameTR").hide();
					$("#departmentTR").hide();
					$("#passwordTR").hide();
					$("#repasswordTR").hide();
				} else if (d.attributes.exist == 'no') {
					$("#nameTR").show();
					$("#departmentTR").show();
					$("#passwordTR").show();
					$("#repasswordTR").show();
					$("#name").val('');
					$("#password").val('');
					$("#repassword").val('');
				}
			}
		});
	}
</script>
</head>
<body>
	<t:form id="userAdd" action="userController.do?doAdd">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">手机:</label></td>
				<td class="value"><input id="phone" name="phone" class="inputxt" type="text" datatype="m" nullmsg="请填写手机"
					ajaxurl="userController.do?validPhone" onchange="phoneChange()"></td>
			</tr>
			<tr id="nameTR">
				<td align="right"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input id="name" name="name" type="text" class="inputxt" datatype="s" nullmsg="请填写姓名" /></td>
			</tr>
			<tr id="departmentTR">
				<td align="right"><label class="Validform_label">部门:</label></td>
				<td class="value"><t:dict id="department" name="department" type="select" groupCode="department" datatype="*"></t:dict></td>
			</tr>
			<tr id="passwordTR">
				<td align="right"><label class="Validform_label">密码:</label></td>
				<td class="value"><input id="password" type="password" class="inputxt" name="password" datatype="*6-18"
					placeholder="6至18位" nullmsg="请填写密码" ajaxurl="userController.do?validPassword" /></td>
			</tr>
			<tr id="repasswordTR">
				<td align="right"><label class="Validform_label">重复密码:</label></td>
				<td class="value"><input id="repassword" class="inputxt" type="password" recheck="password" datatype="*6-18"
					nullmsg="请重复密码"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>