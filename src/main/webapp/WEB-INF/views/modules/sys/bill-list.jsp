<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="billList" url="billController.do?list" checkbox="false" sortName="dealDate">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="名称" field="name" query="true"></t:gridCol>
			<t:gridCol title="类型" field="type" query="true" replace="充值_1,消费_2"></t:gridCol>
			<t:gridCol title="金额" field="amount"></t:gridCol>
			<t:gridCol title="时间" field="dealDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="操作人" field="operator"></t:gridCol>
			<t:gridCol title="备注" field="remarks" width="200"></t:gridCol>
		</t:grid>
	</div>
</div>
