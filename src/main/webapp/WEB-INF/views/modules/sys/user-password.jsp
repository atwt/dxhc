<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="userPassword" action="userController.do?doPassword">
		<input name="id" type="hidden" value="${userPage.id }" />
		<input name="department" type="hidden" value="${userPage.department}" />
		<input name="createDate" type="hidden" value="${userPage.createDate}" />
		<input name="createBy" type="hidden" value="${userPage.createBy}" />
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">手机:</label></td>
				<td class="value"><input name="phone" class="inputxt" type="text" value="${userPage.phone }"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input name="name" type="text" class="inputxt" value="${userPage.name }" readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">密码:</label></td>
				<td class="value"><input id="password" type="password" class="inputxt" name="password" datatype="*6-18"
					placeholder="6至18位" nullmsg="请填写密码" ajaxurl="userController.do?validPassword" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">重复密码:</label></td>
				<td class="value"><input id="repassword" class="inputxt" type="password" recheck="password" datatype="*6-18"
					nullmsg="请重复密码"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>