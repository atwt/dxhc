<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="logList" url="logController.do?list" sortName="operateTime" pageSize="100" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="手机" field="phone" query="true" width="50"></t:gridCol>
			<t:gridCol title="姓名" field="userName" query="true" width="50"></t:gridCol>
			<t:gridCol title="操作类型 " field="operateType" query="true" replace="登录_1,退出_2,增加_3,删除_4,修改_5,上传_6,短信_7,异常_9"
				width="30"></t:gridCol>
			<t:gridCol title="时间" field="operateTime" width="50" dateFormat="yyyy-MM-dd hh:mm:ss" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="详情" field="content" width="300" query="true"></t:gridCol>
			<t:gridBar title="查看" icon="icon-detail" url="logController.do?goDetail" funname="detail"></t:gridBar>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'logController.do?goDetail', 'logList', 800, 400);
	}
</script>
