<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="logDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">手机:</label></td>
				<td class="value"><input id="phone" name="phone" type="text" class="inputxt" value="${logPage.phone}" /></td>
				<td align="right"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input id="userName" name="userName" type="text" class="inputxt" value="${logPage.userName}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">操作类型:</label></td>
				<td class="value"><select name="operateType" id="operateType" disabled="disabled">
						<option value="1" <c:if test="${logPage.operateType eq 1}">selected="selected"</c:if>>登陆</option>
						<option value="2" <c:if test="${logPage.operateType eq 2}">selected="selected"</c:if>>退出</option>
						<option value="3" <c:if test="${logPage.operateType eq 3}">selected="selected"</c:if>>增加</option>
						<option value="4" <c:if test="${logPage.operateType eq 4}">selected="selected"</c:if>>删除</option>
						<option value="5" <c:if test="${logPage.operateType eq 5}">selected="selected"</c:if>>修改</option>
						<option value="6" <c:if test="${logPage.operateType eq 6}">selected="selected"</c:if>>上传</option>
						<option value="7" <c:if test="${logPage.operateType eq 7}">selected="selected"</c:if>>短信</option>
						<option value="9" <c:if test="${logPage.operateType eq 9}">selected="selected"</c:if>>异常</option>
				</select></td>
				<td align="right"><label class="Validform_label">IP:</label></td>
				<td class="value"><input id="ip" name="ip" type="text" class="inputxt" value="${logPage.ip}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">时间:</label></td>
				<td class="value" colspan="3"><input id="operateTime" name="operateTime" type="text" class="inputxt"
					value="${logPage.operateTime}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">详情:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" id="content" name="content"
						style="width: 500px; height: 220px;">${logPage.content}</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>