<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="userUpdate" action="userController.do?doUpdate">
		<input name="id" type="hidden" value="${userPage.id }">
		<input name="password" type="hidden" value="${userPage.password}">
		<input name="createDate" type="hidden" value="${userPage.createDate}">
		<input name="createBy" type="hidden" value="${userPage.createBy}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">手机:</label></td>
				<td class="value"><input id="phone" name="phone" class="inputxt" type="text" value="${userPage.phone }"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input id="name" name="name" type="text" class="inputxt" datatype="s" nullmsg="请填写姓名"
					value="${userPage.name }" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">部门:</label></td>
				<td class="value"><t:dict id="department" name="department" type="select" groupCode="department" datatype="*"
						defaultVal="${userPage.department }"></t:dict></td>
			</tr>
		</table>
	</t:form>
</body>
</html>