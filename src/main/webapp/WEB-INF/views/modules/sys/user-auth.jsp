<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(function() {
		$('#authUL').tree({
			checkbox : true,
			url : 'userController.do?userAuthTree&userId=${userId}',
			onLoadSuccess : function(node) {
				expandAll();
			}
		});
	});
	function saveAuth() {
		var authIds = getAuthNode();
		$('#authIds').val(authIds);
	}
	function getAuthNode() {
		var node = $('#authUL').tree('getChecked');
		var cnodes = '';
		var pnodes = '';
		var pnode = null; //保存上一步所选父节点
		for (var i = 0; i < node.length; i++) {
			if ($('#authUL').tree('isLeaf', node[i].target)) {
				cnodes += node[i].id + ',';
				pnode = $('#authUL').tree('getParent', node[i].target); //获取当前节点的父节点
				while (pnode != null) {//添加全部父节点
					pnodes += pnode.id + ',';
					pnode = $('#authUL').tree('getParent', pnode.target);
				}
			}
		}
		cnodes = cnodes.substring(0, cnodes.length - 1);
		pnodes = pnodes.substring(0, pnodes.length - 1);
		return cnodes + "," + pnodes;
	};

	function expandAll() {
		var node = $('#authUL').tree('getSelected');
		if (node) {
			$('#authUL').tree('expandAll', node.target);
		} else {
			$('#authUL').tree('expandAll');
		}
	}
	function selectAll() {
		var node = $('#authUL').tree('getRoots');
		for (var i = 0; i < node.length; i++) {
			var childrenNode = $('#authUL').tree('getChildren', node[i].target);
			for (var j = 0; j < childrenNode.length; j++) {
				$('#authUL').tree("check", childrenNode[j].target);
			}
		}
	}
	function reset() {
		$('#authUL').tree('reload');
	}
</script>
</head>
<body>
	<t:form id="userAuth" action="userController.do?doAuth" beforeSubmit="saveAuth">
		<input type="hidden" name="userId" value="${userId}" id="userId">
		<input type="hidden" name="authIds" value="${authIds}" id="authIds">
		<div align="right">
			<a id="selectAllBtn" onclick="selectAll();" style="cursor: pointer;">全选</a>&nbsp;&nbsp;<a id="resetBtn"
				onclick="reset();" style="cursor: pointer;">重置</a>
		</div>
		<ul id="authUL"></ul>
	</t:form>
</body>
</html>
