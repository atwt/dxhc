<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:grid name="dictList" url="dictController.do?list&dictGroupId=${dictGroupId}" checkbox="false" pagination="false">
	<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
	<t:gridCol title="名称" field="name" sortable="false"></t:gridCol>
	<t:gridCol title="排序" field="order" sortable="false"></t:gridCol>
	<t:gridBar title="增加" icon="icon-add" url="dictController.do?goAdd&dictGroupId=${dictGroupId}" funname="add"
		width="400" height="150"></t:gridBar>
	<t:gridCol title="操作" field="opt"></t:gridCol>
	<t:gridOpt title="修改" style="btn_yellow" funname="updateDict(id)" />
	<t:gridOpt title="删除" style="btn_red" funname="delDict(id)" />
</t:grid>
<script type="text/javascript">
	function updateDict(id) {
		createAddUpdateWindow('修改', 'dictList',
				'dictController.do?goUpdate&id=' + id, 400, 150);
	}
	function delDict(id) {
		createDialog('删除确认', '确定删除该记录吗 ?', 'dictController.do?doDel&id=' + id,
				'dictList');
	}
</script>
