<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div id="dictGroupPanel" class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="dictGroupList" url="dictController.do?dictGroupList" pagination="false" onLoadSuccess="loadSuccess"
			checkbox="false">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="名称" field="name"></t:gridCol>
			<t:gridCol title="操作" field="opt"></t:gridCol>
			<t:gridOpt title="分类配置" funname="loadDictPanel(id,name)" style="btn_green"></t:gridOpt>
		</t:grid>
	</div>
</div>
<div
	data-options="region:'east',
	title:'dictGroupList',
	collapsed:true,
	split:true,
	border:false,
	onExpand : function(){
		li_east = 1;
	},
	onCollapse : function() {
	    li_east = 0;
	}"
	style="width: 500px; overflow: hidden;" id="eastPanel">
	<div class="easyui-panel" style="padding: 0px; border: 0px" fit="true" border="false" id="dictPanel"></div>
</div>
<script type="text/javascript">
	$(function() {
		var li_east = 0;
	});

	function loadDictPanel(dictGroupId, dictGroupName) {
		var title = dictGroupName + ':';
		if (li_east == 0) {
			$('#dictGroupPanel').layout('expand', 'east');
		}
		$('#dictGroupPanel').layout('panel', 'east').panel('setTitle', title);
		$('#dictPanel').panel("refresh",
				"dictController.do?index&dictGroupId=" + dictGroupId);
	}

	function loadSuccess() {
		$('#dictGroupPanel').layout('panel', 'east').panel('setTitle', "");
		$('#dictGroupPanel').layout('collapse', 'east');
		$('#dictPanel').empty();
	}
</script>
