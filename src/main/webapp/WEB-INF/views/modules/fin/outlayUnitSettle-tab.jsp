<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<style>
.vehicleTable {
	margin: 0 auto;
	border-collapse: collapse;
}

.vehicleThead {
	background: #EEEEEE;
	display: block
}

.vehicleTbody {
	height: 353px;
	overflow-y: scroll;
	display: block;
	overflow-y: scroll;
}

.td35 {
	width: 35px;
	min-width: 35px;
	max-width: 35px;
}

.td50 {
	width: 50px;
	min-width: 50px;
	max-width: 50px;
}

.td160 {
	width: 160px;
	min-width: 160px;
	max-width: 160px;
}

.td170 {
	width: 170px;
	min-width: 170px;
	max-width: 170px;
}

.td310 {
	width: 310px;
	min-width: 310px;
	max-width: 310px;
}

.td340 {
	width: 340px;
	min-width: 340px;
	max-width: 340px;
}
</style>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0" class="vehicleTable">
		<thead class="vehicleThead">
			<tr bgcolor="#E6E6E6">
				<td align="center" class="td35"><label class="Validform_label">序号</label></td>
				<td align="center" class="td50"><label class="Validform_label">操作</label></td>
				<td align="center" class="td170"><label class="Validform_label">车辆</label></td>
				<td align="center" class="td160"><label class="Validform_label">应付金额</label></td>
				<td align="center" class="td340"><label class="Validform_label">备注</label></td>
			</tr>
		</thead>
		<tbody id="outlayUnitSettleTable" class="vehicleTbody">
			<c:forEach items="${outlayUnitList}" var="outlayUnit" varStatus="stuts">
				<tr>
					<input name="outlayUnitList[${stuts.index }].id" type="hidden" value="${outlayUnit.id}">
					<input name="outlayUnitList[${stuts.index }].tenantId" type="hidden" value="${outlayUnit.tenantId}">
					<input name="outlayUnitList[${stuts.index }].unit" type="hidden" value="${outlayUnit.unit}">
					<input name="outlayUnitList[${stuts.index }].vehicle.id" type="hidden" value="${outlayUnit.vehicle.id}">
					<input name="outlayUnitList[${stuts.index }].expenses.id" type="hidden" value="${outlayUnit.expenses.id}">
					<input name="outlayUnitList[${stuts.index }].amount" type="hidden" value="${outlayUnit.amount}">
					<input name="outlayUnitList[${stuts.index }].createBy" type="hidden" value="${outlayUnit.createBy}">
					<input name="outlayUnitList[${stuts.index }].createDate" type="hidden" value="${outlayUnit.createDate}">
					<td align="center" class="td35"><div style="width: 30px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center" class="td50"><input type="checkbox" name="ck${stuts.index }" checked="checked"
						onchange="checkboxChange(${stuts.index})" /><input name="outlayUnitList[${stuts.index }].status" type="hidden"
						value="1"></td>
					<td align="center" class="td170"><c:if test="${not empty outlayUnit.vehicle.sn}">
							<c:out value="${outlayUnit.vehicle.sn}-${outlayUnit.vehicle.plateNumber}" />
						</c:if> <c:if test="${empty outlayUnit.vehicle.sn}">
							<c:out value="${outlayUnit.vehicle.plateNumber}" />
						</c:if></td>
					<td align="center" class="td160"><c:out value="${outlayUnit.amount}"></c:out></td>
					<td align="center" class="td340"><input name="outlayUnitList[${stuts.index }].remarks" type="text"
						class="inputxt" style="width: 330px;" value="${outlayUnit.remarks}"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<table>
		<tr>
			<td style="font-weight: 700;" align="center" class="td310">总计:</td>
			<td id="totalAmount" style="color: red; font-weight: 700; text-align: center;" class="td50">${totalAmount}</td>
		</tr>
	</table>
</div>

<script type="text/javascript">
	function checkboxChange(num){
		if($("#outlayUnitSettleTable").find("input[name='ck" + num+ "']").is(':checked')){
			$("#outlayUnitSettleTable").find("input[name='outlayUnitList[" + num+ "].status']").val(1);
		}else{
			$("#outlayUnitSettleTable").find("input[name='outlayUnitList[" + num+ "].status']").val(0);
		}
		//计算总金额
		var totalAmount=0;
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#outlayUnitSettleTable").find('>tr').each(function(i){
			if($("#outlayUnitSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('amount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalAmount = addAmount(totalAmount,val);
							}
						}	
				});
			}
		});
		$("#totalAmount").html(totalAmount);
	}
	
	function addAmount(arg1, arg2){
		var tem = arg1*10+arg2*10;
		tem = tem/10;
		return tem;
	}
</script>

