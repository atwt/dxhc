<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="left" bgcolor="#EEEEEE" width="20px;"><label class="Validform_label">序号</label></td>
			<td align="center" bgcolor="#EEEEEE" width="110px"><label class="Validform_label">车牌</label></td>
			<td align="center" bgcolor="#EEEEEE" width="90px"><label class="Validform_label">费用类型</label></td>
			<td align="center" bgcolor="#EEEEEE" width="85px"><label class="Validform_label">到期日期</label></td>
			<td align="center" bgcolor="#EEEEEE" width="90px"><label class="Validform_label">应收金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="75px"><label class="Validform_label">优惠金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="75px"><label class="Validform_label">核销金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="150px"><label class="Validform_label">备注</label></td>
		</tr>
		<tbody id="incomeVehicleSettleTable">
			<c:forEach items="${incomeList}" var="income" varStatus="stuts">
				<tr>
					<td align="center"><div style="width: 20px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center" width="110px"><c:out value="${income.vehicle.sn}-${income.vehicle.plateNumber}"></c:out></td>
					<td align="center" width="90px"><c:out value="${income.expenses.name}"></c:out></td>
					<td align="center" width="85px"><c:out value="${fne:getDate(income.endDate)}"></c:out></td>
					<td align="center" width="90px"><c:out value="${income.incomeAmount}"></c:out></td>
					<td align="center"><input name="incomeList[${stuts.index }].discountAmount" type="text" class="inputxt"
						style="width: 75px" readonly="readonly" value="${income.discountAmount}"></td>
					<td align="center"><input name="incomeList[${stuts.index }].discountAmount" type="text" class="inputxt"
						style="width: 75px" readonly="readonly" value="${income.settleAmount}"></td>
					<td align="center"><input name="incomeList[${stuts.index }].remarks" type="text" class="inputxt"
						style="width: 150px" readonly="readonly" value="${income.remarks}"></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="3"></td>
				<td style="font-size: 16px; font-weight: 700;" align="center">总计:</td>
				<td id="totalIncomeAmount" style="color: red; font-size: 14px; font-weight: 700;" align="center">${totalIncomeAmount}</td>
				<td id="totalDiscountAmount" style="color: red; font-size: 14px; font-weight: 700;">${totalDiscountAmount}</td>
				<td id="totalSettleAmount" style="color: red; font-size: 16px; font-weight: 700;">${totalSettleAmount}</td>
			</tr>
		</tbody>
	</table>
</div>
