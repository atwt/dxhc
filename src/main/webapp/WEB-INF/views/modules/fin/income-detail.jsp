<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">状态:</label></td>
				<td class="value"><select name="status" disabled="disabled">
						<option value="0" <c:if test="${incomePage.status eq 0}">selected="selected"</c:if>>未核销</option>
						<option value="1" <c:if test="${incomePage.status eq 1}">selected="selected"</c:if>>已核销</option>
				</select></td>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input name="vehicle.plateNumber" type="text" class="inputxt" readonly="readonly"
					value="${incomePage.vehicle.plateNumber }" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="vehicle.owner.name" type="text" class="inputxt" readonly="readonly"
					value="${incomePage.vehicle.owner.name }" /></td>
				<td align="right"><label class="Validform_label">费用:</label></td>
				<td class="value"><select name="expenses.id" disabled="disabled">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}" <c:if test="${incomePage.expenses.id eq expenses.id}">selected="selected"</c:if>>${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">应收金额:</label></td>
				<td class="value"><input name="incomeAmount" type="text" class="inputxt" value="${incomePage.incomeAmount}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">到期日期:</label></td>
				<td class="value"><input name="endDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${incomePage.endDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">优惠金额:</label></td>
				<td class="value"><input name="discountAmount" type="text" class="inputxt" value="${incomePage.discountAmount}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">核销金额:</label></td>
				<td class="value"><input name="settleAmount" type="text" class="inputxt" value="${incomePage.settleAmount}"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">登记人:</label></td>
				<td class="value"><input name="createBy" type="text" class="inputxt" value="${incomePage.createBy}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">登记日期:</label></td>
				<td class="value"><input name="createDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${incomePage.createDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">核销人:</label></td>
				<td class="value"><input name="settleBy" type="text" class="inputxt" value="${incomePage.settleBy}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">核销日期:</label></td>
				<td class="value"><input name="settleDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${incomePage.settleDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 595px; height: 40px;">${incomePage.remarks}</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>