<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="expensesAdd" action="expensesController.do?doAdd">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">名称:</label></td>
				<td class="value"><input name="name" type="text" class="inputxt" datatype="*" nullmsg="请填写名称" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">费用标准:</label></td>
				<td class="value"><input name="amount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写费用标准"
					errormsg="请填写正确金额" ignore="ignore"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">应用范围:</label></td>
				<td class="value"><select name="type" datatype="*" nullmsg="请填写应用范围">
						<option value="1">收车辆款</option>
						<option value="2">付车辆款</option>
						<option value="3">收单位款</option>
						<option value="4">付单位款</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">排序:</label></td>
				<td class="value"><input name="sort" type="text" class="inputxt" datatype="n1-3"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>