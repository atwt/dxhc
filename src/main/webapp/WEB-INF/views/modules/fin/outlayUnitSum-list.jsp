<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="outlayUnitSumList" url="outlayUnitSumController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true"></t:gridCol>
			<t:gridCol title="往来单位" field="unit" query="true" dictGroup="unit"></t:gridCol>
			<t:gridCol title="费用类型" field="expenses.name"></t:gridCol>
			<t:gridCol title="金额" field="amount" align="right"></t:gridCol>
			<t:gridCol title="付款方式" field="way" replace="现金_1,银行转账_2,微信_3,支付宝_4,POS_5" width="35" align="center"></t:gridCol>
			<t:gridCol title="状态" field="status" replace="正常_0,作废_1" style="background-color:#ed5565;color:#FFF;_1" width="30"
				align="center" query="true"></t:gridCol>
			<t:gridCol title="备注" field="remarks" width="80"></t:gridCol>
			<t:gridCol title="付款人" field="createBy" width="30" query="true"></t:gridCol>
			<t:gridCol title="付款日期" field="createDate" dateFormat="yyyy-MM-dd hh:mm:ss" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="作废人" field="voidBy" width="30"></t:gridCol>
			<t:gridCol title="作废日期" field="voidDate" dateFormat="yyyy-MM-dd hh:mm:ss"></t:gridCol>
			<t:gridBar title="查看" icon="icon-detail" url="outlayUnitSumController.do?goDetail" funname="detail" width="750"
				height="600"></t:gridBar>
			<t:gridBar title="作废" icon="icon-void" url="outlayUnitSumController.do?doVoid" funname="doVoid"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="30" align="center"></t:gridCol>
			<t:gridOpt title="打印" funname="print(id)" style="btn_blue" exp="status#eq#0" />
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'outlayUnitSumController.do?goDetail',
				'outlayUnitSumList', 800, 600);
	}

	function doVoid() {
		var rows = $('#outlayUnitSumList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('费用已作废');
			return;
		}
		createDialog('作废确认', '确定作废该记录 ?',
				'outlayUnitSumController.do?doVoid&id=' + rows[0].id,
				'outlayUnitSumList');
	}

	function print(id) {
		openPrintWindow('打印', 'outlayUnitSumController.do?goPrint&id=' + id,
				'outlayUnitSumList', 260, 700);
	}
</script>
