<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="outlayList" url="outlayController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="outlaySumId" field="outlaySum.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" align="center" width="40"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="费用类型" field="expenses.name"></t:gridCol>
			<t:gridCol title="应付金额" field="outlayAmount" align="right"></t:gridCol>
			<t:gridCol title="核销金额" field="settleAmount" align="right"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="未核销_0,已核销_1"
				style="background-color:#f8ac59;color:#FFF;_0" align="center" width="35"></t:gridCol>
			<t:gridCol title="核销编号" field="outlaySum.sn" formatter="outlaySumNoFormat"></t:gridCol>
			<t:gridBar title="登记" icon="icon-add" url="outlayController.do?goAdd" funname="add" height="450"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="outlayController.do?goUpdate" funname="update"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="outlayController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="outlayController.do?goDetail" funname="detail"></t:gridBar>
			<t:gridBar title="核销" icon="icon-amount" url="outlayController.do?goOwnerSettle" funname="add" width="750"
				height="550"></t:gridBar>
			<t:gridBar title="付款记录" icon="icon-record" funname="goRecord"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="35" align="center"></t:gridCol>
			<t:gridOpt title="核销" funname="settle(id)" style="btn_blue" exp="status#eq#0" />
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 70px; text-align: right; text-overflow: ellipsis;"
			title="费用类型">费用类型 :</span><select name="exs" style="width: 104px">
				<option value="">---请选择---</option>
				<c:forEach items="${expensesList}" var="expenses">
					<option value="${expenses.id}">${expenses.name}</option>
				</c:forEach>
		</select></span>
	</div>
</div>
<script>
	$(function() {
		$("#outlayListtb").find("div[name='searchColums']").find(
				"form#outlayListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
	});
</script>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'outlayController.do?goDetail', 'outlayList', 800, 500);
	}

	function update() {
		var rows = $('#outlayList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('费用已核销,无法修改');
			return;
		}
		createAddUpdateWindow('修改', 'outlayList',
				'outlayController.do?goUpdate&id=' + rows[0].id, 800, 500);
	}

	function del() {
		var rows = $('#outlayList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('费用已核销,无法删除');
			return;
		}
		createDialog('删除确认', '确定删除项目 ?', 'outlayController.do?doDel&id='
				+ rows[0].id, 'outlayList');
	}

	function settle(id) {
		createAddUpdateWindow('核销', 'outlayList',
				'outlayController.do?goVehicleSettle&id=' + id, 800, 500);
	}

	function goRecord() {
		addTab('付车辆款-付款记录', 'outlaySumController.do?index');
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		if (vehicleId != undefined) {
			return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId
					+ '\')>' + value + '</a>';
		}
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'outlayList', 800, 580)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id, 'outlayList',
				800, 500)
	}

	function outlaySumNoFormat(value, rec, index) {
		var outlaySumId = rec["outlaySum.id"];
		if (outlaySumId != undefined) {
			return '<a href=\'#\' onclick=outlaySumDetail(\'' + outlaySumId
					+ '\')>' + value + '</a>';
		}
	}

	function outlaySumDetail(id) {
		openwindow('查看', 'outlaySumController.do?goDetail&id=' + id,
				'outlayList', 750, 600)
	}
</script>
