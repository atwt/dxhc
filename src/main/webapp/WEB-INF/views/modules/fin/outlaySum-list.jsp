<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="outlaySumList" url="outlaySumController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true"></t:gridCol>
			<t:gridCol title="责任人" field="owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="手机" field="owner.phone" query="true"></t:gridCol>
			<t:gridCol title="金额" field="amount" align="right"></t:gridCol>
			<t:gridCol title="付款方式" field="way" replace="现金_1,微信_2,支付宝_3,银行转账_4" width="35" align="center" query="true"></t:gridCol>
			<t:gridCol title="状态" field="status" replace="正常_0,作废_1" style="background-color:#ed5565;color:#FFF;_1" width="30"
				align="center" query="true"></t:gridCol>
			<t:gridCol title="备注" field="remarks" width="80"></t:gridCol>
			<t:gridCol title="收款人" field="createBy" width="30" query="true"></t:gridCol>
			<t:gridCol title="收款日期" field="createDate" dateFormat="yyyy-MM-dd hh:mm:ss" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="作废人" field="updateBy" width="30"></t:gridCol>
			<t:gridCol title="作废日期" field="updateDate" dateFormat="yyyy-MM-dd hh:mm:ss"></t:gridCol>
			<t:gridBar title="查看" icon="icon-detail" url="outlaySumController.do?goDetail" funname="detail" width="750"
				height="600"></t:gridBar>
			<t:gridBar title="作废" icon="icon-void" url="outlaySumController.do?doVoid" funname="doVoid"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="30" align="center"></t:gridCol>
			<t:gridOpt title="打印" funname="print(id)" style="btn_blue" exp="status#eq#0" />
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'outlaySumController.do?goDetail', 'outlaySumList', 750,
				600);
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id, 'outlayList',
				800, 500)
	}

	function doVoid() {
		var rows = $('#outlaySumList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('费用已作废');
			return;
		}
		createDialog('作废确认', '确定作废该记录 ?', 'outlaySumController.do?doVoid&id='
				+ rows[0].id, 'outlaySumList');
	}

	function print(id) {
		openPrintWindow('打印', 'outlaySumController.do?goPrint&id=' + id,
				'outlaySumList', 260, 700);
	}
</script>
