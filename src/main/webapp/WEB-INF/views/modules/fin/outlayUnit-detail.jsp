<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="outlayDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">状态:</label></td>
				<td class="value" colspan="3"><select name="status" disabled="disabled">
						<option value="0" <c:if test="${outlayUnitPage.status eq 0}">selected="selected"</c:if>>未核销</option>
						<option value="1" <c:if test="${outlayUnitPage.status eq 1}">selected="selected"</c:if>>已核销</option>
				</select></td>
			</tr>
			<tr>

				<td align="right"><label class="Validform_label">往来单位:</label></td>
				<td class="value"><t:dict id="unit" name="unit" type="select" groupCode="unit" datatype="*"
						defaultVal="${outlayUnitPage.unit}"></t:dict></td>
				<td align="right"><label class="Validform_label">费用:</label></td>
				<td class="value"><select name="expenses.id" disabled="disabled">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}"
								<c:if test="${outlayUnitPage.expenses.id eq expenses.id}">selected="selected"</c:if>>${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input name="vehicle.plateNumber" type="text" class="inputxt" readonly="readonly"
					value="${outlayUnitPage.vehicle.plateNumber }" /></td>
				<td align="right"><label class="Validform_label">应付金额:</label></td>
				<td class="value"><input name="amount" type="text" class="inputxt" value="${outlayUnitPage.amount}"
					readonly="readonly"></td>

			</tr>
			<tr>
				<td align="right"><label class="Validform_label">登记人:</label></td>
				<td class="value"><input name="createBy" type="text" class="inputxt" value="${outlayUnitPage.createBy}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">登记日期:</label></td>
				<td class="value"><input name="createDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${outlayUnitPage.createDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">核销人:</label></td>
				<td class="value"><input name="settleBy" type="text" class="inputxt" value="${outlayUnitPage.settleBy}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">核销日期:</label></td>
				<td class="value"><input name="settleDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${outlayUnitPage.settleDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 580px; height: 40px;">${outlayUnitPage.remarks}</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>