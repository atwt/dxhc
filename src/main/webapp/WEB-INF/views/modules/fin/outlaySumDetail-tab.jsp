<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="left" bgcolor="#EEEEEE" width="30px;"><label class="Validform_label">序号</label></td>
			<td align="center" bgcolor="#EEEEEE" width="120px"><label class="Validform_label">车牌</label></td>
			<td align="center" bgcolor="#EEEEEE" width="120px"><label class="Validform_label">费用类型</label></td>
			<td align="center" bgcolor="#EEEEEE" width="100px"><label class="Validform_label">应收金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="100px"><label class="Validform_label">核销金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="250px"><label class="Validform_label">备注</label></td>
		</tr>
		<tbody id="outlayCarSettleTable">
			<c:forEach items="${outlayList}" var="outlay" varStatus="stuts">
				<tr>
					<td align="center"><div style="width: 30px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center" width="120px"><c:out value="${outlay.vehicle.sn}-${outlay.vehicle.plateNumber}"></c:out></td>
					<td align="center" width="100px"><c:out value="${outlay.expenses.name}"></c:out></td>
					<td align="center" width="100px"><c:out value="${outlay.outlayAmount}"></c:out></td>
					<td align="center"><input name="outlayList[${stuts.index }].discountAmount" type="text" class="inputxt"
						style="width: 100px" readonly="readonly" value="${outlay.settleAmount}"></td>
					<td align="center"><input name="outlayList[${stuts.index }].remarks" type="text" class="inputxt"
						style="width: 250px" readonly="readonly" value="${outlay.remarks}"></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="3"></td>
				<td style="font-size: 16px; font-weight: 700;" align="center">总计:</td>
				<td id="totalOutlayAmount" style="color: red; font-size: 14px; font-weight: 700;" align="center">${totalOutlayAmount}</td>
				<td id="totalSettleAmount" style="color: red; font-size: 16px; font-weight: 700;">${totalSettleAmount}</td>
			</tr>
		</tbody>
	</table>
</div>
