<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<style>
.vehicleTable {
	margin: 0 auto;
	border-collapse: collapse;
}

.vehicleThead {
	background: #EEEEEE;
	display: block
}

.vehicleTbody {
	height: 365px;
	overflow-y: scroll;
	display: block;
	overflow-y: scroll;
}

.td30 {
	width: 30px;
	min-width: 30px;
	max-width: 30px;
}

.td35 {
	width: 35px;
	min-width: 35px;
	max-width: 35px;
}

.td40 {
	width: 40px;
	min-width: 40px;
	max-width: 40px;
}

.td50 {
	width: 50px;
	min-width: 50px;
	max-width: 50px;
}

.td140 {
	width: 140px;
	min-width: 140px;
	max-width: 140px;
}

.td160 {
	width: 160px;
	min-width: 160px;
	max-width: 160px;
}

.td170 {
	width: 170px;
	min-width: 170px;
	max-width: 170px;
}

.td340 {
	width: 340px;
	min-width: 340px;
	max-width: 340px;
}
</style>
<script type="text/javascript">
	function addClick() {
		var tr = $("#incomeUnitListTable_template tr").clone();
		$("#incomeUnitListTable").append(tr);
		resetTrNum('incomeUnitListTable');
		calTotalAmount();
	}

	function delClick() {
		$("#incomeUnitListTable").find("input:checked").parent().parent()
				.remove();
		resetTrNum('incomeUnitListTable');
		calTotalAmount();
	}

	function resetTrNum(tableId) {
		$tbody = $("#" + tableId + "");
		$tbody
				.find('>tr')
				.each(
						function(i) {
							$(':input,button,a', this)
									.each(
											function() {
												var $this = $(this);
												var name = $this.attr('name');
												var onclick_str = $this
														.attr('onclick');
												if (name != null) {
													if (name.indexOf("#index#") >= 0) {
														$this
																.attr(
																		"name",
																		name
																				.replace(
																						'#index#',
																						i));
													} else {
														var s = name
																.indexOf("[");
														var e = name
																.indexOf("]");
														var new_name = name
																.substring(
																		s + 1,
																		e);
														$this
																.attr(
																		"name",
																		name
																				.replace(
																						new_name,
																						i));
													}
												}
												if (name.indexOf('plateNumber') >= 0
														&& onclick_str != null) {
													if (onclick_str
															.indexOf("#index#") >= 0) {
														$this
																.attr(
																		"onclick",
																		onclick_str
																				.replace(
																						/#index#/g,
																						i));
													} else {
														onclick_str = 'vehicleSelect('
																+ i + ')';
														$this.attr("onclick",
																onclick_str);
													}
												}
											});
							$(this).find('div[name=\'xh\']').html(i + 1);
						});
	}
</script>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<div style="height: 30px;">
		<a id="addBtn" onclick="addClick()" style="cursor: pointer; margin-left: 10px;"><img alt="增加"
			src="${ctx}/plug-in/easyui/icons/add.png"></a><a id="delBtn" onclick="delClick()"
			style="cursor: pointer; margin-left: 25px;"><img alt="删除" src="${ctx}/plug-in/easyui/icons/minus.png"></a> <img
			style="margin-left: 650px;" title="该功能用于收取往来单位费用.如保险返点,补贴等" src="${ctx}/plug-in/easyui/icons/tip.png">
	</div>
	<table border="0" cellpadding="2" cellspacing="0" class="vehicleTable">
		<thead class="vehicleThead">
			<tr bgcolor="#E6E6E6">
				<td align="center" class="td35"><label class="Validform_label">序号</label></td>
				<td align="center" class="td50"><label class="Validform_label">操作</label></td>
				<td align="center" class="td170"><label class="Validform_label">车辆</label></td>
				<td align="center" class="td160"><label class="Validform_label">应收金额</label></td>
				<td align="center" class="td340"><label class="Validform_label">备注</label></td>
			</tr>
		</thead>
		<tbody id="incomeUnitListTable" class="vehicleTbody">
			<tr>
				<td align="center" class="td30"><div style="width: 30px;" name="xh">1</div></td>
				<td align="center" class="td50"><input style="width: 50px;" type="checkbox" name="ck" /><input
					name="incomeUnitList[0].id" type="hidden" /></td>
				<td align="center" class="td160"><input id="incomeUnitList[0].vehicle.id" name="incomeUnitList[0].vehicle.id"
					type="hidden" /> <input id="incomeUnitList[0].plateNumber" name="incomeUnitList[0].plateNumber" class="inputxt"
					style="width: 150px;" type="text" readonly="readonly" onclick="vehicleSelect(0)" datatype="*" /></td>
				<td align="center" class="td140"><input name="incomeUnitList[0].amount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" errormsg="请填写正确金额"
					style="width: 130px; text-align: right;" onchange="calTotalAmount()"></td>
				<td align="center" class="td340"><input name="incomeUnitList[0].remarks" type="text" class="inputxt"
					style="width: 330px;"></td>
			</tr>
		</tbody>
	</table>
	<table>
		<tr>
			<td style="font-weight: 700;" align="center" class="td340">总计:</td>
			<td id="totalAmount" style="color: red; font-weight: 700; text-align: center;" class="td50"></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	function calTotalAmount() {
		var totalAmount = 0;
		var regExp = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#incomeUnitListTable").find('>tr').each(function(i) {
			$(':input', this).each(function() {
				var $this = $(this);
				var name = $this.attr('name');
				if (name != null && name.indexOf('amount') >= 0) {
					var val = $this.val();
					if (regExp.test(val)) {
						totalAmount = addAmount(totalAmount, val);
					}
				}
			});
		});
		$("#totalAmount").html(totalAmount);
	}

	function addAmount(arg1, arg2) {
		var tem = arg1 * 10 + arg2 * 10;
		tem = tem / 10;
		return tem;
	}

	function vehicleSelect(num) {
		var vehicleId = 'incomeUnitList[' + num + '].vehicleId';
		var url = 'vehicleController.do?select';
		var initValue = $('#' + vehicleId).val();
		url += '&ids=' + initValue;
		$
				.dialog({
					content : 'url:' + url,
					zIndex : getzIndex(),
					title : '车辆列表',
					lock : true,
					parent : frameElement.api,
					width : '800',
					height : '459',
					left : '50%',
					top : '50%',
					opacity : 0.4,
					button : [
							{
								name : '确定',
								callback : function() {
									iframe = this.iframe.contentWindow;
									var ids = iframe
											.getvehicleSelectListSelections('id');
									$("#incomeUnitListTable").find(
											"input[name='incomeUnitList[" + num
													+ "].vehicle.id']")
											.val(ids);
									var sn = iframe
											.getvehicleSelectListSelections('sn')
									var vehiclePlateNumber = iframe
											.getvehicleSelectListSelections('plateNumber');
									if (sn != "" && sn != null
											&& sn != undefined) {
										vehiclePlateNumber = sn + '-'
												+ vehiclePlateNumber;
									}
									$("#incomeUnitListTable").find(
											"input[name='incomeUnitList[" + num
													+ "].plateNumber']").val(
											vehiclePlateNumber);

								},
								focus : true
							}, {
								name : '取消',
								callback : function() {
								}
							} ]
				});
	}
</script>
