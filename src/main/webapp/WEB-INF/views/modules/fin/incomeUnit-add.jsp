<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeUnitAdd" action="incomeUnitController.do?doAdd" tiptype="1">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">往来单位:</label></td>
				<td class="value"><t:dict id="unit" name="unit" type="select" groupCode="unit" datatype="*"></t:dict><a
					onclick="addUnit('unit');"><span style="font-size: 12px; margin-left: 5px; color: #c1b7b7">增加</span></a></td>
				<td align="right"><label class="Validform_label">费用类型:</label></td>
				<td class="value"><select name="expenses.id" id="expensesId" datatype="*">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}">${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
		</table>
		<div style="width: 790px; height: 495px;" id="vehicleTab">
			<div border="false" class="easyui-tabs" fit="true">
				<div title="车辆明细" href="incomeUnitController.do?tab"></div>
			</div>
		</div>
	</t:form>
	<!-- 添加 明细 模版 -->
	<table style="display: none" id="incomeUnitListTable_template">
		<tbody>
			<tr>
				<td align="center" class="td30"><div style="width: 30px;" name="xh"></div></td>
				<td align="center" class="td50"><input style="width: 50px;" type="checkbox" name="ck" /></td>
				<td align="center" class="td160"><input id="incomeUnitList[#index#].vehicle.id"
					name="incomeUnitList[#index#].vehicle.id" type="hidden" /><input id="incomeUnitList[#index#].plateNumber"
					name="incomeUnitList[#index#].plateNumber" class="inputxt" type="text" style="width: 150px;" readonly="readonly"
					onclick="vehicleSelect(#index#)" datatype="*" /></td>
				<td align="center" class="td140"><input name="incomeUnitList[#index#].amount" type="text" class="inputxt"
					style="width: 130px; text-align: right;"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" errormsg="请填写正确金额"
					onchange="calTotalAmount()"></td>
				<td align="center" class="td340"><input name="incomeUnitList[#index#].remarks" type="text" class="inputxt"
					style="width: 330px;"></td>
			</tr>
		</tbody>
	</table>
</body>
</html>