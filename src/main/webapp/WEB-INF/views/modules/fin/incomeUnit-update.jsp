<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeUnitUpdate" action="incomeUnitController.do?doUpdate">
		<input name="id" type="hidden" value="${incomeUnitPage.id }">
		<input name="tenantId" type="hidden" value="${incomeUnitPage.tenantId }">
		<input name="status" type="hidden" value="${incomeUnitPage.status }">
		<input name="createDate" type="hidden" value="${incomeUnitPage.createDate}">
		<input name="createBy" type="hidden" value="${incomeUnitPage.createBy}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">往来单位:</label></td>
				<td class="value"><t:dict id="unit" name="unit" type="select" groupCode="unit" datatype="*"
						defaultVal="${incomeUnitPage.unit}"></t:dict><a onclick="addUnit('unit');"><span
						style="font-size: 12px; margin-left: 5px; color: #c1b7b7">增加</span></a></td>
				<td align="right"><label class="Validform_label">费用:</label></td>
				<td class="value"><select id="expenses" name="expenses.id" datatype="*" nullmsg="请填写费用">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}"
								<c:if test="${incomeUnitPage.expenses.id eq expenses.id}">selected="selected"</c:if>>${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicle.id" type="hidden" value="${incomeUnitPage.vehicle.id }" />
					<input name="vehicle.plateNumber" id="vehiclePlateNumber" class="inputxt" readonly="readonly"
					onclick="choose_vehiclePlateNumber()" datatype="*" nullmsg="请填写车辆" value="${incomeUnitPage.vehicle.plateNumber }" />
					<t:gridSelect nameId="vehiclePlateNumber" url="vehicleController.do?select" valueId="vehicleId"
						gridField="plateNumber" gridName="vehicleSelectList" title="车辆列表" height="459" width="800"></t:gridSelect></td>
				<td align="right"><label class="Validform_label">应收金额:</label></td>
				<td class="value"><input name="amount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写应收金额"
					errormsg="请填写正确金额" value="${incomeUnitPage.amount}"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 595px; height: 40px;">${incomeUnitPage.remarks}</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>