<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="rptIncomedList" url="rptIncomedController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" align="center" width="40"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name"></t:gridCol>
			<t:gridCol title="费用类型" field="expenses.name"></t:gridCol>
			<t:gridCol title="应收金额" field="incomeAmount" align="right"></t:gridCol>
			<t:gridCol title="到期日期" field="endDate" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridCol title="优惠金额" field="discountAmount" align="right"></t:gridCol>
			<t:gridCol title="核销金额" field="settleAmount" align="right"></t:gridCol>
			<t:gridCol title="核销日期" field="settleDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group"></t:gridCol>
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 70px; text-align: right; text-overflow: ellipsis;"
			title="费用类型">费用类型:</span><select name="exs" style="width: 120px">
				<option value="">---请选择---</option>
				<c:forEach items="${expensesList}" var="expenses">
					<option value="${expenses.id}">${expenses.name}</option>
				</c:forEach>
		</select></span>
	</div>
</div>
<script>
	$(function() {
		$("#rptIncomedListtb").find("div[name='searchColums']").find(
				"form#rptIncomedListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
	});
</script>
