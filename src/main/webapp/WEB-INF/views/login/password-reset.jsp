<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="${ctx}/plug-in/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" href="${ctx}/plug-in/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="${ctx}/plug-in/hplus/css/style.css" />
<script src="${ctx}/plug-in/jquery/jquery-2.1.4.min.js"></script>
<script src="${ctx}/plug-in/jquery/jquery.cookie.js"></script>
<script src="${ctx}/plug-in/bootstrap/bootstrap.min.js"></script>
<script src="${ctx}/plug-in/layer/layer.js"></script>
<script type="text/javascript">
	/**
	 * 刷新验证码
	 */
	function randCodeImageClick() {
		var date = new Date();
		var img = document.getElementById("randCodeImage");
		img.src = 'randCodeImage?a=' + date.getTime();
	}

	$(function() {
		/*仿刷新：检测是否存在cookie*/
		if ($.cookie("captcha")) {
			var count = $.cookie("captcha");
			var btn = $('#btnSms');
			btn.val(count + '秒后可获取').attr('disabled', true).css('cursor',
					'not-allowed');
			var resend = setInterval(function() {
				count--;
				if (count > 0) {
					btn.val(count + '秒后可获取').attr('disabled', true).css(
							'cursor', 'not-allowed');
					$.cookie("captcha", count, {
						path : '/',
						expires : (1 / 86400) * count
					});
				} else {
					clearInterval(resend);
					btn.val("获取短信").removeClass('disabled').removeAttr(
							'disabled style');
					btn.css({
						"height" : "32px",
						"width" : "106px"
					});
				}
			}, 1000);
		}

		/*点击改变按钮状态，已经简略掉ajax发送短信验证的代码*/
		$('#btnSms').click(
				function() {
					//表单校验
					if (sendSmsCheck()) {
						var formData = new Object();
						var data = $(":input").each(function() {
							formData[this.name] = $("#" + this.name).val();
						});
						$.ajax({
							async : false,
							cache : false,
							type : 'POST',
							url : 'index.do?sendSmsCheck',
							data : formData,
							error : function() {
							},
							success : function(data) {
								var d = $.parseJSON(data);
								if (d.success) {
									//校验成功发送短信验证码
									$.ajax({
										async : false,
										cache : false,
										type : 'POST',
										url : 'index.do?sendSms',
										data : formData,
										error : function() {
										},
										success : function(data) {
											var d = $.parseJSON(data);
											top.layer.msg(d.msg);
										}
									});
									var btn = $('#btnSms');
									var count = 60;
									var resend = setInterval(function() {
										count--;
										if (count > 0) {
											btn.val(count + "秒后可获取");
											$.cookie("captcha", count, {
												path : '/',
												expires : (1 / 86400) * count
											});
										} else {
											clearInterval(resend);
											btn.val("获取短信").removeAttr(
													'disabled style');
											btn.css({
												"height" : "32px",
												"width" : "106px"
											});
										}
									}, 1000);
									btn.attr('disabled', true).css('cursor',
											'not-allowed');
									//重新刷新验证码
									randCodeImageClick();
								} else {
									//校验失败
									top.layer.msg(d.msg);
									return;
								}
							}
						});

					}
				});
	});

	//密码重置
	function passwordReset() {
		if (resetCheck()) {
			var formData = new Object();
			var data = $(":input").each(function() {
				formData[this.name] = $("#" + this.name).val();
			});
			$.ajax({
				async : false,
				cache : false,
				type : 'POST',
				url : 'index.do?doPassword',
				data : formData,
				error : function() {
				},
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						top.layer.msg(d.msg);
						var count = 10;
						var resend = setInterval(function() {
							count--;
						}, 1000);
						window.location.href = 'index.do?login';
					} else {
						top.layer.msg(d.msg);
					}
				}
			});
		}
	}

	//表单校验
	function resetCheck() {
		var phone = $("#phone").val();
		var randCode = $("#randCode").val();
		var smsCode = $("#smsCode").val();
		var password = $("#password").val();
		var repassword = $("#repassword").val();
		if ($.isEmptyObject(phone)) {
			top.layer.msg("请输入手机");
			return false;
		} else if (!(/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/
				.test(phone))) {
			top.layer.msg("手机号码不正确");
			return false;
		} else if ($.isEmptyObject(randCode)) {
			top.layer.msg("请输入图形验证码");
			return false;
		} else if ($.isEmptyObject(smsCode)) {
			top.layer.msg("请输入短信验证码");
			return false;
		} else if ($.isEmptyObject(password)) {
			top.layer.msg("请输入密码");
			return false;
		} else if ($.isEmptyObject(repassword)) {
			top.layer.msg("请重复输入密码");
			return false;
		} else if (password.length < 6) {
			top.layer.msg("密码不能少于6位");
			return false;
		} else if (password != repassword) {
			top.layer.msg("两次密码不一致");
			return false;
		}
		return true;
	}

	//表单校验
	function sendSmsCheck() {
		var phone = $("#phone").val();
		var randCode = $("#randCode").val();
		if ($.isEmptyObject(phone)) {
			top.layer.msg("请输入手机");
			return false;
		} else if (!(/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/
				.test(phone))) {
			top.layer.msg("手机号码不正确");
			return false;
		} else if ($.isEmptyObject(randCode)) {
			top.layer.msg("请输入图形验证码");
			return false;
		}
		return true;
	}
</script>
</head>
<body class="gray-bg">
	<div class="middle-box text-center loginscreen  animated">
		<div>
			<div>
				<h3 class="logo-name" style="font-size: 60px; margin-top: 50px; margin-bottom: 50px;">大 象 货 车</h3>
			</div>
			<h3>重置密码</h3>
			<form class="m-t" role="form">
				<div class="form-group">
					<input type="text" id="phone" name="phone" required="required" class="form-control uname" placeholder="手机号" />
				</div>
				<div class="form-group">
					<div class="input-group">
						<input type="text" id="randCode" name="randCode" class="form-control" style="width: 194px" placeholder="图形验证码" />
						<span class="input-group-addon" style="padding: 0px;"><img id="randCodeImage" src="randCodeImage"
							onclick="randCodeImageClick()" /></span>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<input type="text" id="smsCode" name="smsCode" class="form-control" style="width: 194px" placeholder="短信验证码" /> <span
							class="input-group-addon" style="padding: 0px;"><input type="button" id="btnSms" class="btn btn-white"
							style="height: 32px; width: 106px;" value="获取短信" /></span>
					</div>
				</div>
				<div class="form-group">
					<input type="password" id="password" name="password" required="required" class="form-control pword m-b"
						placeholder="新密码" />
				</div>
				<div class="form-group">
					<input type="password" id="repassword" name="repassword" required="required" class="form-control pword m-b"
						placeholder="重复密码" />
				</div>
				<button type="button" id="btnLogin" class="btn btn-primary block full-width m-b" onclick="passwordReset()">重
					置</button>
			</form>
		</div>
	</div>
</body>
</html>
