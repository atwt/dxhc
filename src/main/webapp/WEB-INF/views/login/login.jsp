<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="${ctx}/plug-in/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" href="${ctx}/plug-in/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="${ctx}/plug-in/hplus/css/style.css" />
<link rel="stylesheet" href="${ctx}/plug-in/hplus/css/login.css" />
<script src="${ctx}/plug-in/jquery/jquery-2.1.4.min.js"></script>
<script src="${ctx}/plug-in/bootstrap/bootstrap.min.js"></script>
<script src="${ctx}/plug-in/layer/layer.js"></script>
<script src="${ctx}/plug-in/lhgDialog/lhgdialog.min.js?skin=metrole"></script>
<script type="text/javascript">
	$(document).keydown(function(e) {
		if (e.keyCode == 13) {
			$("#btnLogin").click();
		}
	});
	function login() {
		var phone = $("#phone").val();
		if ($.isEmptyObject(phone)) {
			top.layer.msg("请输入手机");
			return;
		} else if (!(/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/
				.test(phone))) {
			top.layer.msg("手机号码不正确");
			return;
		}
		var password = $("#password").val();
		if ($.isEmptyObject(password)) {
			top.layer.msg("请输入密码");
			return;
		}
		var formData = new Object();
		var data = $(":input").each(function() {
			formData[this.name] = $("#" + this.name).val();
		});
		$
				.ajax({
					async : false,
					cache : false,
					type : 'POST',
					url : 'index.do?check',
					data : formData,
					error : function() {
					},
					success : function(data) {
						var d = $.parseJSON(data);
						if (d.success) {
							if (d.attributes.tenantNum > 1) {
								$
										.dialog({
											id : 'selectTenantToLogin',
											title : '请选择公司 ',
											max : false,
											min : false,
											drag : false,
											resize : false,
											content : 'url:index.do?goTenantSelect&userId='
													+ d.attributes.userId,
											lock : true,
											button : [ {
												name : '确定',
												focus : true,
												callback : function() {
													iframe = this.iframe.contentWindow;
													var userId = $('#userId',
															iframe.document)
															.val();
													var tenantId = $(
															'#tenantId',
															iframe.document)
															.val();
													formData['userId'] = userId ? userId
															: "";
													formData['tenantId'] = tenantId ? tenantId
															: "";
													$
															.ajax({
																async : false,
																cache : false,
																type : 'POST',
																url : 'index.do?doTenantSelect',// 请求的action路径
																data : formData,
																error : function() {// 请求失败处理函数
																},
																success : function(
																		data) {
																	window.location.href = 'index.do?main';
																}
															});
													this.close();
													return false;
												}
											} ],
											close : function() {
												setTimeout(
														"window.location.href='"
																+ 'index.do?main'
																+ "'", 10);
											}
										});
							} else {
								window.location.href = 'index.do?main';
							}
						} else {
							top.layer.msg(d.msg);
						}
					}
				});
	}
</script>
</head>
<body class="signin">
	<div class="signinpanel">
		<div class="row">
			<div class="col-sm-7">
				<div class="signin-info">
					<div class="logopanel m-b">
						<h1>大象货车</h1>
					</div>
					<div class="m-b"></div>
					<h4>
						<strong>管车好帮手</strong>
					</h4>
					<ul class="m-b">
						<li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 更安全</li>
						<li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 更方便</li>
						<li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 更强大</li>
						<li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 更易用</li>
						<li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 更便宜</li>
					</ul>
					<strong>还没有账号？ <a href="http://www.daxianghuoche.com/" target="_blank"
						style="font-size: 16px; color: #f5f5f5;">免费开通&raquo;</a></strong>
				</div>
			</div>
			<div class="col-sm-5">
				<form>
					<h3 class="no-margins">用户登录：</h3>
					<input type="text" id="phone" name="phone" required="required" class="form-control uname" placeholder="手机"
						value="" /> <input type="password" id="password" name="password" required="required"
						class="form-control pword m-b" placeholder="密码" value="" /> <a href="index.do?passwordReset"
						target="_blank" style="font-size: 6px; color: #f5f5f5;">忘记密码？</a>
					<button type="button" id="btnLogin" class="btn btn-primary btn-block" onclick="login()">登录</button>
				</form>
			</div>
		</div>
		<div class="signup-footer">
			<div class="pull-left">&copy; All Rights Reserved. 大象货车</div>
		</div>
	</div>
</body>
</html>
