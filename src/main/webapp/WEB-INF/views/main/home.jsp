<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<link rel="shortcut icon" href="${ctx}/images/favicon.ico">
<link href="${ctx}/plug-in/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/plug-in/hplus/css/style.css?v=4.1.0" rel="stylesheet">
<script src="${ctx}/plug-in/echart/echarts.common.min.js"></script>
<script src="${ctx}/plug-in/jquery/jquery-2.1.4.min.js"></script>
<script src="${ctx}/plug-in/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// 车辆提醒图表，初始化echarts实例
		var warnChart = echarts.init(document.getElementById('warn'));
		// 指定图表的配置项和数据
		var warnOption = {
			color : [ '#3398DB' ],
			tooltip : {
				trigger : 'axis',
				axisPointer : { // 坐标轴指示器，坐标轴触发有效
					type : 'shadow' // 默认为直线，可选为：'line' | 'shadow'
				}
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			xAxis : [ {
				type : 'category',
				data : [],
				axisTick : {
					alignWithLabel : true
				}
			} ],
			yAxis : [ {
				type : 'value'
			} ],
			series : [ {
				name : '车辆数',
				type : 'bar',
				barWidth : '60%',
				data : []
			} ]
		};
		// 使用刚指定的配置项和数据显示图表。
		warnChart.setOption(warnOption);
		$.ajax({
			type : 'POST',
			url : 'index.do?warnChart',
			success : function(data) {
				var d = $.parseJSON(data);
				warnChart.setOption({
					xAxis : {
						data : d.attributes.categories
					},
					series : [ {
						// 根据名字对应到相应的系列
						data : d.attributes.series
					} ]
				});
			}
		});

		// 车辆状态图表，初始化echarts实例
		var statusChart = echarts.init(document.getElementById('status'));
		// 指定图表的配置项和数据
		var statusOption = {
			title : {
				text : '车辆状态',
				x : 'center'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				left : 'left',
				data : []
			},
			series : [ {
				name : '车辆数',
				type : 'pie',
				radius : '55%',
				center : [ '50%', '60%' ],
				data : [],
				itemStyle : {
					emphasis : {
						shadowBlur : 10,
						shadowOffsetX : 0,
						shadowColor : 'rgba(0, 0, 0, 0.5)'
					}
				}
			} ]
		};
		// 使用刚指定的配置项和数据显示图表。
		statusChart.setOption(statusOption);

		$.ajax({
			type : 'POST',
			url : 'index.do?statusChart',
			success : function(data) {
				var d = $.parseJSON(data);
				var serisData = d.attributes.serisData;
				//jquery遍历
				var value = [];
				$.each(serisData, function(i, p) {
					value[i] = {
						'name' : p['name'],
						'value' : p['value']
					};
				});
				statusChart.setOption({
					legend : {
						data : d.attributes.legend
					},
					series : [ {
						data : value
					} ]
				});
			}
		});
	});
</script>
<script type="text/javascript">
	function warnDaysChange(warnDayType) {
		// 车辆提醒图表，初始化echarts实例
		var warnChart = echarts.init(document.getElementById('warn'));
		// 指定图表的配置项和数据
		var warnOption = {
			color : [ '#3398DB' ],
			tooltip : {
				trigger : 'axis',
				axisPointer : { // 坐标轴指示器，坐标轴触发有效
					type : 'shadow' // 默认为直线，可选为：'line' | 'shadow'
				}
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			xAxis : [ {
				type : 'category',
				data : [],
				axisTick : {
					alignWithLabel : true
				}
			} ],
			yAxis : [ {
				type : 'value'
			} ],
			series : [ {
				name : '车辆数',
				type : 'bar',
				barWidth : '60%',
				data : []
			} ]
		};
		// 使用刚指定的配置项和数据显示图表。
		warnChart.setOption(warnOption);
		$.ajax({
			type : 'POST',
			url : 'index.do?warnChart&warnDayType=' + warnDayType,
			success : function(data) {
				var d = $.parseJSON(data);
				warnChart.setOption({
					xAxis : {
						data : d.attributes.categories
					},
					series : [ {
						// 根据名字对应到相应的系列
						data : d.attributes.series
					} ]
				});
			}
		});
	}
</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<span class="label label-success pull-right">当前</span>
						<h5>当前车辆</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">${cldtMap.currentAmount}</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<span class="label label-info pull-right">本月</span>
						<h5>本月新增</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">+${cldtMap.monthAdd}</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<span class="label label-danger pull-right">本月</span>
						<h5>本月注销</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">-${cldtMap.monthReduce}</h1>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<span class="label label-info pull-right">当年</span>
						<h5>当年变化</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">
							<c:if test="${cldtMap.yearDynamic > 0}">+${cldtMap.yearDynamic}</c:if>
							<c:if test="${cldtMap.yearDynamic <= 0}">${cldtMap.yearDynamic}</c:if>
						</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>到期信息</h5>
						<div class="pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-xs btn-white active" onclick="warnDaysChange(3)">10天</button>
								<button type="button" class="btn btn-xs btn-white" onclick="warnDaysChange(4)">30天</button>
								<button type="button" class="btn btn-xs btn-white" onclick="warnDaysChange(5)">60天</button>
							</div>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<div class="flot-chart" style="height: 360px;">
									<div class="flot-chart-content" id="warn"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>车辆状态</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<div class="flot-chart" style="height: 360px;">
									<div class="flot-chart-content" id="status"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		
	</script>
</body>
</html>
