<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<link rel="shortcut icon" href="${ctx}/images/favicon.ico">
<link href="${ctx}/plug-in/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/plug-in/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="${ctx}/plug-in/hplus/css/style.css?v=4.1.0" rel="stylesheet">
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow: hidden">
	<div id="wrapper">
		<!--左侧导航开始-->
		<nav class="navbar-default navbar-static-side" role="navigation" style="z-index: 1991;">
			<div class="nav-close">
				<i class="fa fa-times-circle"></i>
			</div>
			<div class="sidebar-collapse">
				<ul class="nav" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element">
							<span><img alt="image" width="200" height="100" src="${ctx}/images/logo.png" /></span>
						</div>
						<div class="logo-element">大象</div>
					</li>
					<t:menu menuMap="${userMenu}"></t:menu>
				</ul>
			</div>
		</nav>
		<!--左侧导航结束-->
		<!--右侧部分开始-->
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header" style="height: 60px;">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
						<form role="search" class="navbar-form-custom" action="javascript:window.open('http://www.daxianghuoche.com')"
							method="post">
							<div class="form-group">
								<input type="text" placeholder="大象货车 轻松管车" class="form-control" name="top-search" id="top-search">
							</div>
						</form>
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown"><span style="color: #999c9e"><strong class="font-bold">${tenantName}</strong></span></li>
						<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#"><span><strong
									class="font-bold" style="color: #ed5565">账户余额：¥ ${balance}</strong></span><span><b class="caret"></b></span> </a>
							<ul class="dropdown-menu dropdown-alerts">
								<li><a href="javascript:openwindow('充值','index.do?goRecharge','','650','400')">充值 </a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#"> <span><strong
									class="font-bold">${userName}</strong></span> <span><b class="caret"></b></span>
						</a>
							<ul class="dropdown-menu dropdown-alerts">
								<li><a href="javascript:add('修改密码','userController.do?goPassword','')">修改密码 </a></li>
							</ul></li>
						<li class="dropdown"><a href="javascript:openwindow('帮助','index.do?goHelp','','650','400')"
							class="roll-nav roll-right"><span><strong class="font-bold">帮助</strong></span></a></li>
						<li class="dropdown"><a href="javascript:logout()" class="roll-nav roll-right J_tabExit"><i
								class="fa fa fa-power-off"></i>退出</a></li>
					</ul>
				</nav>
			</div>
			<div class="row content-tabs">
				<button class="roll-nav roll-left J_tabLeft">
					<i class="fa fa-backward"></i>
				</button>
				<nav class="page-tabs J_menuTabs">
					<div class="page-tabs-content">
						<a href="" class="active J_menuTab" data-id="index.do?home">首页</a>
					</div>
					<button class="roll-nav roll-right J_tabRight">
						<i class="fa fa-forward"></i>
					</button>
					<div class="btn-group roll-nav roll-right">
						<button class="dropdown J_tabClose" data-toggle="dropdown">
							关闭操作<span class="caret"></span>
						</button>
						<ul role="menu" class="dropdown-menu dropdown-menu-right">
							<li class="J_tabShowActive"><a>定位当前页面</a></li>
							<li class="J_tabCloseAll"><a>关闭全部页面</a></li>
							<li class="J_tabCloseOther"><a>关闭其他页面</a></li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="row J_mainContent" id="content-main">
				<iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="index.do?home" frameborder="0"
					data-id="index.do?home" seamless></iframe>
			</div>
			<div class="footer">
				<div class="pull-right">
					<a href="http://www.daxianghuoche.com/" target="_blank"><span style="font-size: 12px;">大象货车
							QQ:635806128</span></a>
				</div>
			</div>
		</div>
	</div>

	<!-- 全局js -->
	<script src="${ctx}/plug-in/jquery/jquery-2.1.4.min.js"></script>
	<script src="${ctx}/plug-in/bootstrap/bootstrap.min.js"></script>
	<script src="${ctx}/plug-in/hplus/js/metisMenu.js"></script>
	<script src="${ctx}/plug-in/hplus/js/slimscroll.min.js"></script>
	<script src="${ctx}/plug-in/hplus/js/hplus.js?v=4.1.0"></script>
	<script src="${ctx}/plug-in/hplus/js/contabs.js"></script>
	<script src="${ctx}/plug-in/hplus/js/hplus-tab.js"></script>
	<script src="${ctx}/plug-in/hplus/js/pace.min.js"></script>
	<script src="${ctx}/plug-in/layer/layer.js"></script>
	<script src="${ctx}/plug-in/extend/crud.js"></script>
	<script src="${ctx}/plug-in/lhgDialog/lhgdialog.min.js?skin=metrole"></script>
	<script>
		function logout() {
			layer.confirm('您确定要注销吗？', {
				btn : [ '确定', '取消' ], //按钮
				shade : false
			//不显示遮罩
			}, function() {
				location.href = "index.do?logout";
			}, function() {
				return;
			});
		}
	</script>
</body>

</html>
