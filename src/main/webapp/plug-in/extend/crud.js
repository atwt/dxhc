﻿/**
 * 增删改工具栏
 */
var iframe;// iframe操作对象
var win;// 窗口对象
var gridName = "";// 操作datagrid对象名称
var windowapi;
var W;
try {
	windowapi = frameElement.api, W = windowapi.opener;// 内容页中调用窗口实例对象接口
} catch (e) {

}

/**
 * 设置 window的 zIndex
 * 
 * @param flag
 *            true: 不增量(因为 tip提示经常使用 zIndex, 所以如果是 tip的话 ,则不增量)
 * @returns
 */
function getzIndex(flag) {
	var zindexNumber = getCookie("ZINDEXNUMBER");
	if (zindexNumber == null) {
		zindexNumber = 2010;
		setCookie("ZINDEXNUMBER", zindexNumber);
		// zindexNumber = 1980;
	} else {
		if (zindexNumber < 2010) {
			zindexNumber = 2010;
		}
		var n = flag ? zindexNumber : parseInt(zindexNumber) + parseInt(10);
		setCookie("ZINDEXNUMBER", n);
	}
	return zindexNumber;
}

/**
 * 添加事件打开窗口
 * 
 * @param title
 *            编辑框标题
 * @param addurl//目标页面地址
 */
function add(title, addUrl, listName, width, height) {
	gridName = listName;
	width = width ? width : 800;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = window.top.document.body.offsetWidth;
		height = window.top.document.body.offsetHeight - 100;
	}
	if (typeof (windowapi) == 'undefined') {
		$.dialog({
			content : 'url:' + addUrl,
			lock : true,
			zIndex : getzIndex(),
			width : width,
			height : height,
			title : title,
			opacity : 0.3,
			cache : false,
			ok : function() {
				iframe = this.iframe.contentWindow;
				saveObj();
				return false;
			},
			cancelVal : '关闭',
			cancel : true
		/* 为true等价于function(){} */
		});
	} else {
		$.dialog({
			content : 'url:' + addUrl,
			lock : true,
			width : width,
			zIndex : getzIndex(),
			height : height,
			parent : windowapi,
			title : title,
			opacity : 0.3,
			cache : false,
			ok : function() {
				iframe = this.iframe.contentWindow;
				saveObj();
				return false;
			},
			cancelVal : '关闭',
			cancel : true
		/* 为true等价于function(){} */
		});
	}
}

/**
 * 修改事件打开窗口
 * 
 * @param title
 *            编辑框标题
 * @param addurl//目标页面地址
 * @param id//主键字段
 */
function update(title, updateUrl, listName, width, height) {
	gridName = listName;
	var rowsData = $('#' + listName).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择要修改数据');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条数据');
		return;
	}
	updateUrl += '&id=' + rowsData[0].id;
	// 创建窗口
	width = width ? width : 800;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = window.top.document.body.offsetWidth;
		height = window.top.document.body.offsetHeight - 100;
	}
	if (typeof (windowapi) == 'undefined') {
		$.dialog({
			content : 'url:' + updateUrl,
			lock : true,
			zIndex : getzIndex(),
			width : width,
			height : height,
			title : title,
			opacity : 0.3,
			cache : false,
			ok : function() {
				iframe = this.iframe.contentWindow;
				saveObj();
				return false;
			},
			cancelVal : '关闭',
			cancel : true
		/* 为true等价于function(){} */
		});
	} else {
		$.dialog({
			content : 'url:' + updateUrl,
			lock : true,
			width : width,
			zIndex : getzIndex(),
			height : height,
			parent : windowapi,
			title : title,
			opacity : 0.3,
			cache : false,
			ok : function() {
				iframe = this.iframe.contentWindow;
				saveObj();
				return false;
			},
			cancelVal : '关闭',
			cancel : true
		});
	}
}

/**
 * 删除单个
 * 
 * @param title
 * @param deleteUrl
 * @param listName
 */
function del(title, deleteUrl, listName, width, height) {
	gridName = listName;
	var rowsData = $('#' + listName).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择要修改数据');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条数据');
		return;
	}
	deleteUrl += '&id=' + rowsData[0].id;
	$.dialog.setting.zIndex = getzIndex(true);
	layer.open({
		title : '删除',
		content : '确定永久删除数据吗?',
		icon : 7,
		yes : function(index) {
			doSubmit(deleteUrl, gridName);
		},
		btn : [ '确定', '取消' ]
	});
}

/**
 * 删除多个
 * 
 * @param title
 * @param deleteUrl
 * @param listName
 */
function delMore(title, deleteUrl, listName, width, height) {
	gridName = listName;
	var rows = $("#" + gridName).datagrid('getSelections');
	if (!rows || rows.length == 0) {
		tip('请选择要删除数据');
		return;
	}
	var ids = [];
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i].id);
	}
	$.dialog.setting.zIndex = getzIndex(true);
	layer.open({
		title : '删除',
		content : '确定永久删除数据吗?',
		icon : 7,
		yes : function(index) {
			doSubmit(deleteUrl + '&ids=' + ids, gridName);
		},
		btn : [ '确定', '取消' ]
	});
}

/**
 * 如果页面是详细查看页面，无效化所有表单元素，只能进行查看
 */
$(function() {
	if (location.href.indexOf("load=detail") != -1) {
		$("input").attr("readonly", "readonly");
		$("textarea").attr("readonly", "readonly");
		$("button").attr("readonly", "readonly");
		$("select").attr("disabled", "disabled");
		$("checkbox").attr("disabled", "disabled");
		$("radio").attr("disabled", "disabled");
	}
});

/**
 * 查看详细事件打开窗口
 * 
 * @param title
 *            查看框标题
 * @param addurl//目标页面地址
 * @param id//主键字段
 */
function detail(title, detailUrl, listName, width, height) {
	gridName = listName;
	var rowsData = $('#' + listName).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择要查看数据');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条数据');
		return;
	}
	detailUrl += '&load=detail&id=' + rowsData[0].id;
	// 创建窗口
	width = width ? width : 800;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = window.top.document.body.offsetWidth;
		height = window.top.document.body.offsetHeight - 100;
	}
	if (typeof (windowapi) == 'undefined') {
		$.dialog({
			content : 'url:' + detailUrl,
			zIndex : getzIndex(),
			lock : true,
			width : width,
			height : height,
			title : title,
			opacity : 0.3,
			cache : false,
			cancelVal : '关闭',
			cancel : true
		});
	} else {
		$.dialog({
			content : 'url:' + detailUrl,
			zIndex : getzIndex(),
			lock : true,
			width : width,
			height : height,
			parent : windowapi,
			title : title,
			opacity : 0.3,
			cache : false,
			cancelVal : '关闭',
			cancel : true
		});
	}
}

// 普通询问操作调用函数
function confirm(url, content, name) {
	createDialog('提示信息 ', content, url, name);
}

function tip(msg) {
	try {
		$.dialog.setting.zIndex = getzIndex(true);

		var navigatorName = "Microsoft Internet Explorer";
		if (navigator.appName == navigatorName) {
			$.messager.show({
				title : '提示信息',
				msg : msg,
				timeout : 1000 * 6
			});
		} else {
			var icon = 7;
			if (msg.indexOf("成功") > -1) {
				icon = 1;
			} else if (msg.indexOf("失败") > -1) {
				icon = 2;
			}
			layer.open({
				title : '提示信息',
				offset : 'rb',
				content : msg,
				time : 3000,
				btn : false,
				shade : false,
				icon : icon,
				shift : 2
			});
		}

	} catch (e) {
		alertTipTop(msg, '10%');
	}
}

function alertTip(msg, top, title) {
	$.dialog.setting.zIndex = getzIndex(true);
	title = title ? title : "提示信息";
	$.dialog({
		title : title,
		zIndex : getzIndex(),
		icon : 'tips.gif',
		top : top,
		content : msg
	});
}

/**
 * 创建添加或修改窗口
 * 
 * @param title
 * @param addurl
 * @param saveurl
 */
function createAddUpdateWindow(title, listName, url, width, height) {
	gridName = listName;
	width = width ? width : 800;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = window.top.document.body.offsetWidth;
		height = window.top.document.body.offsetHeight - 100;
	}
	if (typeof (windowapi) == 'undefined') {
		$.dialog({
			content : 'url:' + url,
			lock : true,
			zIndex : getzIndex(),
			width : width,
			height : height,
			title : title,
			opacity : 0.3,
			cache : false,
			ok : function() {
				iframe = this.iframe.contentWindow;
				saveObj();
				return false;
			},
			cancelVal : '关闭',
			cancel : true
		});
	} else {
		$.dialog({
			content : 'url:' + url,
			lock : true,
			width : width,
			zIndex : getzIndex(),
			height : height,
			parent : windowapi,
			title : title,
			opacity : 0.3,
			cache : false,
			ok : function() {
				iframe = this.iframe.contentWindow;
				saveObj();
				return false;
			},
			cancelVal : '关闭',
			cancel : true
		});
	}
}
/**
 * 创建不带按钮的窗口
 * 
 * @param title
 * @param addurl
 * @param saveurl
 */
function openwindow(title, url, listName, width, height) {
		gridName = listName;
	width = width ? width : 800;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = window.top.document.body.offsetWidth;
		height = window.top.document.body.offsetHeight - 100;
	}
	if (typeof (windowapi) == 'undefined') {
		$.dialog({
			content : 'url:' + url,
			zIndex : getzIndex(),
			title : title,
			cache : false,
			lock : true,
			width : width,
			height : height,
			cancelVal : '关闭',
			cancel : true
		});
	} else {
		$.dialog({
			content : 'url:' + url,
			zIndex : getzIndex(),
			title : title,
			cache : false,
			parent : windowapi,
			lock : true,
			width : width,
			height : height,
			cancelVal : '关闭',
			cancel : true
		});
	}
}

/**
 * 创建不带按钮的窗口
 * 
 * @param title
 * @param addurl
 * @param saveurl
 */
function openPrintWindow(title, url, listName, width, height) {
	gridName = listName;
	width = width ? width : 800;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = window.top.document.body.offsetWidth;
		height = window.top.document.body.offsetHeight - 100;
	}
	if (typeof (windowapi) == 'undefined') {
		$.dialog({
			width : width,
			height : height,
			content : 'url:' + url,
			zIndex : getzIndex(),
			title : title,
			cache : false,
			lock : true
		});
	} else {
		$.dialog({
			width : width,
			height : height,
			content : 'url:' + url,
			zIndex : getzIndex(),
			parent : windowapi,
			title : title,
			cache : false,
			lock : true
		});
	}
}

/**
 * 创建询问窗口
 * 
 * @param title
 * @param content
 * @param url
 */
function createDialog(title, content, url, name) {
	$.dialog.setting.zIndex = getzIndex(true);
	layer.open({
		title : title,
		content : content,
		icon : 7,
		yes : function(index) {
			doSubmit(url, name);
		},
		btn : [ '确定', '取消' ],
	});
}
/**
 * 执行保存
 * 
 * @param url
 * @param gridName
 */
function saveObj() {
	$('#btn_sub', iframe.document).click();
}

/**
 * 执行查询
 * 
 * @param url
 * @param gridName
 */
function search() {
	$('#btn_sub', iframe.document).click();
	iframe.search();
}

/**
 * 执行操作
 * 
 * @param url
 * @param index
 */
function doSubmit(url, name, data) {
	gridName = name;
	var paramsData = data;
	if (!paramsData) {
		paramsData = new Object();
		if (url.indexOf("&") != -1) {
			var str = url.substr(url.indexOf("&") + 1);
			url = url.substr(0, url.indexOf("&"));
			var strs = str.split("&");
			for (var i = 0; i < strs.length; i++) {
				paramsData[strs[i].split("=")[0]] = (strs[i].split("=")[1]);
			}
		}
	}
	$.ajax({
		async : false,
		cache : false,
		type : 'POST',
		data : paramsData,
		url : url,// 请求的action路径
		error : function() {// 请求失败处理函数
		},
		success : function(data) {
			var d = $.parseJSON(data);
			if (d.success) {
				var msg = d.msg;
				tip(msg);
				reloadTable();
			} else {
				tip(d.msg);
			}
		}
	});

}
/*
 * 鼠标放在图片上方，显示大图
 */
var bigImgIndex = null;
function tipImg(obj) {
	try {
		var navigatorName = "Microsoft Internet Explorer";
		if (navigator.appName != navigatorName) {
			if (obj.nodeName == 'IMG') {
				var e = window.event;
				var x = e.clientX + document.body.scrollLeft
						+ document.documentElement.scrollLeft;
				var y = e.clientY + document.body.scrollTop
						+ document.documentElement.scrollTop;
				var src = obj.src;
				var width = obj.naturalWidth;
				var height = obj.naturalHeight;
				bigImgIndex = layer.open({
					content : [ src, 'no' ],
					type : 2,
					offset : [ y + "px", x + "px" ],
					title : false,
					area : [ width + "px", height + "px" ],
					shade : 0,
					closeBtn : 0
				});
			}
		}
	} catch (e) {
	}

}

function moveTipImg() {
	try {
		if (bigImgIndex != null) {
			layer.close(bigImgIndex);
		}
	} catch (e) {

	}
}
/**
 * 退出确认框
 * 
 * @param url
 * @param content
 * @param index
 */
function exit(url, content) {
	$.dialog.setting.zIndex = getzIndex(true);
	$.dialog.confirm(content, function() {
		window.location = url;
	}, function() {
	});
}

function generateTabId(str) {
	var val = "";
	for (var i = 0; i < str.length; i++) {
		val += str.charCodeAt(i).toString(16);
	}
	return val;
}
// 添加标签
function addTab(subtitle, url, icon) {
	var id = generateTabId(subtitle);
	window.top.addTabs({
		id : id,
		title : subtitle,
		close : true,
		url : url
	});
}
// 关闭自身TAB刷新父TABgrid
function closetab(title) {
	window.top.$('#maintabs').tabs('close', title);
}

/**
 * Excel 导出 代入查询条件
 */
function xlsExport(url, listName) {
	var queryParams = $('#' + listName).datagrid('options').queryParams;
	$('#' + listName + 'tb').find('*').each(function() {
		queryParams[$(this).attr('name')] = $(this).val();
	});
	var params = '&';
	$.each(queryParams, function(key, val) {
		params += '&' + key + '=' + val;
	});
	var fields = '&field=';
	$.each($('#' + listName).datagrid('options').columns[0], function(i, val) {
		if (val.field != 'opt') {
			fields += val.field + ',';
		}
	});

	var id = '&id=';
	$.each($('#' + listName).datagrid('getSelections'), function(i, val) {
		id += val.id + ",";
	});
	window.location.href = url + encodeURI(fields + params + id);

}

/* 获取Cookie值 */
function getCookie(c_name) {
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(c_name + "=")
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1
			c_end = document.cookie.indexOf(";", c_start)
			if (c_end == -1)
				c_end = document.cookie.length
			return unescape(document.cookie.substring(c_start, c_end))
		}
	}
	return ""
}
/* 设置 cookie */
function setCookie(c_name, value, expiredays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + expiredays);
	document.cookie = c_name + "=" + escape(value)
			+ ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}
