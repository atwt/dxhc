package manage.safe.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 试卷管理
 * 
 * @author zzc
 */
@Entity
@Table(name = "manage_paper")
@DynamicInsert
@DynamicUpdate
public class Paper implements Serializable {

	private static final long serialVersionUID = 6515718364426975939L;
	public static final String TYPE_GENERAL = "1";// 货运
	public static final String TYPE_DANGER = "2";// 危险品
	public static final String STATUS_OPEN = "0";// 开放
	public static final String STATUS_CLOSE = "1";// 关闭

	private String id;
	/** 名称 */
	private String name;
	/** 分类: 货运_1,危险品_2 */
	private String type;
	/** 状态:开放_0,关闭_1 */
	private String status;
	/** 创建时间 */
	private Date createDate;
	/** 试题选项 */
	private List<Question> questionList = new ArrayList<Question>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "分类不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinTable(name = "manage_paper_question", joinColumns = { @JoinColumn(name = "paper_id") }, inverseJoinColumns = {
			@JoinColumn(name = "question_id") })
	@Fetch(FetchMode.SUBSELECT)
	public List<Question> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<Question> questionList) {
		this.questionList = questionList;
	}

}