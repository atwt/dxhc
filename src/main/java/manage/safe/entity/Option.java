package manage.safe.entity;

import java.io.Serializable;

/**
 * 试题选项
 * 
 * @author zzc
 */
public class Option implements Serializable {

	private static final long serialVersionUID = -656215443772448856L;
	/** 序号 */
	private String sn;
	/** 内容 */
	private String content;

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}