package manage.safe.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 试题管理
 * 
 * @author zzc
 */
@Entity
@Table(name = "manage_question")
@DynamicInsert
@DynamicUpdate
public class Question implements Serializable {

	private static final long serialVersionUID = 4384764556485796374L;

	public static final String QDB_GENERAL = "1";// 货运
	public static final String QDB_DANGER = "2";// 危险品

	private String id;
	/** 题库:货运_1,危险品_2 */
	private String qdb;
	/** 状态:开放_0,关闭_1 */
	private String status;
	/** 题干 */
	private String content;
	/** 答案 */
	private String answer;
	/** 解析 */
	private String resolve;
	/** 选项 */
	private String optionData;
	/** 试题选项 */
	private List<Option> optionList = new ArrayList<Option>();
	/** 试卷 */
	private List<Paper> paperList = new ArrayList<Paper>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "qdb", nullable = false, length = 10)
	@NotBlank(message = "题库不能为空")
	public String getQdb() {
		return qdb;
	}

	public void setQdb(String qdb) {
		this.qdb = qdb;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "content", nullable = false)
	@NotBlank
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "answer", nullable = false, length = 10)
	@NotBlank(message = "答案不能为空")
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Column(name = "resolve")
	public String getResolve() {
		return resolve;
	}

	public void setResolve(String resolve) {
		this.resolve = resolve;
	}

	@Column(name = "option_data", nullable = false, length = 10)
	@NotBlank(message = "选项不能为空")
	public String getOptionData() {
		return optionData;
	}

	public void setOptionData(String optionData) {
		this.optionData = optionData;
	}

	@Transient
	@JsonIgnore
	public List<Option> getOptionList() {
		return optionList;
	}

	public void setOptionList(List<Option> optionList) {
		this.optionList = optionList;
	}

	@ManyToMany(mappedBy = "questionList", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SUBSELECT)
	@JsonIgnore
	public List<Paper> getPaperList() {
		return paperList;
	}

	public void setPaperList(List<Paper> paperList) {
		this.paperList = paperList;
	}

}