package manage.sys.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * TLog entity.
 * 
 * @author zzc
 */
@Entity
@Table(name = "manage_log")
@DynamicInsert
@DynamicUpdate
public class ManageLog implements Serializable {

	private static final long serialVersionUID = 8031761754790652666L;

	public static final String OPERATE_LOGIN = "1"; // 登陆
	public static final String OPERATE_EXIT = "2"; // 退出
	public static final String OPERATE_ADD = "3"; // 增加
	public static final String OPERATE_DEL = "4"; // 删除
	public static final String OPERATE_UPDATE = "5"; // 修改
	public static final String OPERATE_UPLOAD = "6"; // 上传
	public static final String OPERATE_EXCEPTION = "7"; // 异常

	/** id */
	private String id;
	/** 用户名 */
	private String userName;
	/** 内容 */
	private String content;
	/** 操作类型 */
	private String operateType;
	/** IP */
	private String ip;
	/** 时间 */
	private Date operateTime;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "user_name", length = 50)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "content", nullable = false, length = 1500)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "operate_type", nullable = false, length = 10)
	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	@Column(name = "ip", length = 50)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "operate_time", nullable = false)
	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

}