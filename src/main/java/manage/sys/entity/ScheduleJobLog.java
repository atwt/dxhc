package manage.sys.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * 定时任务执行日志
 * 
 * 
 */
@Entity
@Table(name = "manage_schedule_job_log")
@DynamicInsert
@DynamicUpdate
public class ScheduleJobLog implements Serializable {

	private static final long serialVersionUID = 3969640904109958866L;
	public static final String STATUS_SUCCESS = "0";
	public static final String STATUS_FAIL = "1";

	private String id;
	/** 任务名称 */
	private String jobName;
	/** spring bean名称 */
	private String beanName;
	/** 方法名称 */
	private String methodName;
	/** 参数 */
	private String params;
	/** 任务状态 */
	private String status;
	/** 失败信息 */
	private String error;
	/** 耗时(单位：毫秒) */
	private Integer times;
	/** 创建时间 */
	private Date createDate;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "job_name", nullable = false, length = 50)
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Column(name = "bean_name", nullable = false, length = 50)
	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	@Column(name = "method_name", nullable = false, length = 50)
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Column(name = "params", length = 50)
	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	@Column(name = "status", nullable = false, length = 10)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "error", length = 255)
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Column(name = "times")
	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
