package manage.sys.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.UUIDGenerator;
import manage.sys.entity.ScheduleJobLog;
import manage.sys.service.ScheduleJobLogService;

/**
 * 
 * @author zzc
 *
 */
@Service("scheduleJobLogService")
@Transactional
public class ScheduleJobLogServiceImpl extends CommonServiceImpl implements ScheduleJobLogService {

	public void save(ScheduleJobLog scheduleJobLog) {
		String sql = "insert into manage_schedule_job_log values(?,?,?,?,?,?,?,?,?)";
		executeSql(sql,
				new Object[] { UUIDGenerator.generate(), scheduleJobLog.getJobName(), scheduleJobLog.getBeanName(),
						scheduleJobLog.getMethodName(), scheduleJobLog.getParams(), scheduleJobLog.getStatus(),
						scheduleJobLog.getError(), scheduleJobLog.getTimes(), scheduleJobLog.getCreateDate() });
	}
}
