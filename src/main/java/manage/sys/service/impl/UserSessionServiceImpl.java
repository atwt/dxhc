package manage.sys.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import manage.sys.entity.UserSession;
import manage.sys.service.UserSessionService;

/**
 * 
 * @author zzc
 *
 */
@Service("userSessionService")
@Transactional
public class UserSessionServiceImpl extends CommonServiceImpl implements UserSessionService {

	@Cacheable(value = "userSessionCache", key = "#userId")
	public UserSession getByUser(String userId) {
		String hql = "from UserSession where userId=?";
		return getUniqueByHql(hql, userId);
	}

	@CacheEvict(value = "userSessionCache", key = "#userSession.userId", beforeInvocation = true)
	public void update(UserSession userSession) {
		saveOrUpdate(userSession);
	}

	@CacheEvict(value = "userSessionCache", key = "#userSession.userId", beforeInvocation = true)
	public void save(UserSession userSession) {
		super.save(userSession);
	}
}
