package manage.sys.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.SecretUtil;
import manage.sys.entity.ManageUser;
import manage.sys.service.ManageUserService;

/**
 * 
 * @author zzc
 *
 */
@Service("manageUserService")
@Transactional
public class ManageUserServiceImpl extends CommonServiceImpl implements ManageUserService {

	public ManageUser loginCheck(ManageUser manageUser) {
		String password = SecretUtil.encrypt(manageUser.getName(), manageUser.getPassword(),
				SecretUtil.getStaticSalt());
		String hql = "from ManageUser where name=? and password=?";
		return getUniqueByHql(hql, manageUser.getName(), password);
	}

	public ManageUser getByName(String name) {
		String hql = "from ManageUser where name=?";
		return getUniqueByHql(hql, name);
	}

	public void updatePassword(ManageUser manageUser) {
		saveOrUpdate(manageUser);
		// 删除登录记录
		String hql = "delete from ManageUserLogin where userName=?";
		executeHql(hql, manageUser.getName());
	}

}
