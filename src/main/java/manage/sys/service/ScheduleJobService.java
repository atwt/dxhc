package manage.sys.service;

import luxing.common.service.CommonService;
import manage.sys.entity.ScheduleJob;

/**
 * 
 * @author zzc
 *
 */
public interface ScheduleJobService extends CommonService {

	void save(ScheduleJob scheduleJob);

	void update(ScheduleJob scheduleJob);

	void delete(ScheduleJob scheduleJob);

	void run(ScheduleJob scheduleJob);

	void pause(ScheduleJob scheduleJob);

	void resume(ScheduleJob scheduleJob);

}
