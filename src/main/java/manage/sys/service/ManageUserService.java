package manage.sys.service;

import luxing.common.service.CommonService;
import manage.sys.entity.ManageUser;

/**
 * 
 * @author zzc
 *
 */
public interface ManageUserService extends CommonService {

	ManageUser loginCheck(ManageUser manageUser);

	ManageUser getByName(String name);

	void updatePassword(ManageUser manageUser);
}
