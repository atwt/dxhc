package manage.sys.service;

import java.util.List;

import luxing.common.service.CommonService;
import manage.sys.entity.ManageUserLogin;

/**
 * 
 * @author zzc
 *
 */
public interface ManageUserLoginService extends CommonService {

	ManageUserLogin getByUserName(String userName);

	List<ManageUserLogin> listByIP(String ip);

}
