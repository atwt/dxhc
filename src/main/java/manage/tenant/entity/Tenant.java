package manage.tenant.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import com.google.common.collect.Lists;

import modules.sys.entity.Region;
import modules.sys.entity.User;

/**
 * 租户
 * 
 * @author zzc
 */
@Entity
@Table(name = "manage_tenant")
@DynamicInsert
@DynamicUpdate
public class Tenant implements Serializable {

	private static final long serialVersionUID = -8981213054385616031L;

	public static final String STATUS_ENABLE = "0"; // 启用
	public static final String STATUS_DISABLE = "1"; // 禁用
	public static final BigDecimal BALANCE_MIN = new BigDecimal(1); // 账户最小余额

	private String id;
	/** 编号 */
	private String number;
	/** 租户名 */
	private String name;
	/** 省 */
	private Region province;
	/** 市 */
	private Region city;
	/** 地址 */
	private String address;
	/** 信用代码 */
	private String creditCode;
	/** 状态 */
	private String status;
	/** 账户余额 */
	private BigDecimal balance;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 修改人 */
	private String updateBy;
	/** 修改时间 */
	private Date updateDate;
	/** 拥有用户列表 */
	private List<User> userList = Lists.newArrayList();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "province_id")
	public Region getProvince() {
		return province;
	}

	public void setProvince(Region province) {
		this.province = province;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_id")
	public Region getCity() {
		return city;
	}

	public void setCity(Region city) {
		this.city = city;
	}

	@Column(name = "address", length = 100)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "number", nullable = false, length = 50)
	@NotBlank(message = "编号")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "credit_code", length = 50)
	public String getCreditCode() {
		return creditCode;
	}

	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "balance", nullable = false, scale = 1, length = 10)
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "update_date", nullable = true)
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "update_by", nullable = true, length = 32)
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	@ManyToMany(mappedBy = "tenantList", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SUBSELECT)
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
}