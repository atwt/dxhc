package manage.tenant.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.ResourceUtil;
import luxing.util.SecretUtil;
import manage.sys.entity.Menu;
import manage.tenant.entity.Recharge;
import manage.tenant.entity.Tenant;
import manage.tenant.service.TenantService;
import modules.arc.entity.ArcExt;
import modules.fin.entity.Expenses;
import modules.sys.entity.Bill;
import modules.sys.entity.Dict;
import modules.sys.entity.User;
import modules.warn.entity.WarnType;

/**
 * 
 * @author zzc
 *
 */
@Service("tenantService")
@Transactional
public class TenantServiceImpl extends CommonServiceImpl implements TenantService {

	public void save(Tenant tenant, User user, Recharge recharge) {
		// 1、保存租户
		super.save(tenant);
		// 2、数据字典
		String dictHql = "from Dict where tenantId=?";
		List<Dict> dictList = listByHql(dictHql, ResourceUtil.getConfig("template.tenant.id"));
		for (Dict entity : dictList) {
			Dict dict = new Dict();
			dict.setTenantId(tenant.getId());
			dict.setName(entity.getName());
			dict.setOrder(entity.getOrder());
			dict.setDictGroup(entity.getDictGroup());
			super.save(dict);
		}
		// 3、档案扩展
		String arcHql = "from ArcExt where tenantId=?";
		List<ArcExt> arcList = listByHql(arcHql, ResourceUtil.getConfig("template.tenant.id"));
		for (ArcExt entity : arcList) {
			ArcExt arcExt = new ArcExt();
			arcExt.setTenantId(tenant.getId());
			arcExt.setName(entity.getName());
			arcExt.setType(entity.getType());
			super.save(arcExt);
		}
		// 4、提醒扩展
		String warnHql = "from WarnType where status=? and tenantId=?";
		List<WarnType> warnList = listByHql(warnHql, WarnType.STATUS_ENABLE,
				ResourceUtil.getConfig("template.tenant.id"));
		for (WarnType entity : warnList) {
			WarnType warn = new WarnType();
			warn.setTenantId(tenant.getId());
			warn.setName(entity.getName());
			warn.setStatus(entity.getStatus());
			super.save(warn);
		}
		// 5、费用类型
		String expensesHql = "from Expenses where tenantId=?";
		List<Expenses> expensesList = listByHql(expensesHql, ResourceUtil.getConfig("template.tenant.id"));
		for (Expenses entity : expensesList) {
			Expenses expenses = new Expenses();
			expenses.setTenantId(tenant.getId());
			expenses.setName(entity.getName());
			expenses.setType(entity.getType());
			expenses.setAmount(entity.getAmount());
			expenses.setStatus(entity.getStatus());
			expenses.setSort(entity.getSort());
			super.save(expenses);
		}
		// 6、用户
		List<Menu> menuList = listByHql("from Menu where type=?", Menu.TYPE_TENANT);
		User userDB = getUniqueByHql("from User where phone=?", user.getPhone());
		if (userDB != null) {
			userDB.setMenuList(menuList);
			userDB.getTenantList().add(tenant);
			super.saveOrUpdate(user);
		} else {
			user.setPassword(SecretUtil.encrypt(user.getPhone(), user.getPhone(), SecretUtil.getStaticSalt()));
			user.setMenuList(menuList);
			List<Tenant> tenantList = new ArrayList<Tenant>();
			tenantList.add(tenant);
			user.setTenantList(tenantList);
			super.save(user);
		}
		// 7、交费信息
		recharge.setTenant(tenant);
		super.save(recharge);
		tenant.setBalance(recharge.getAmount());
		super.saveOrUpdate(tenant);
		// 8、账户明细
		Bill bill = new Bill(tenant, "充值", Bill.TYPE_PAY, recharge.getAmount(), new Date(), "系统",
				recharge.getRemarks());
		super.save(bill);
	}

	public void delete(Tenant tenant) {
		// TODO
		// 删除责任人
		// 删除车辆
		// 删除驾驶员
		// 删除数据字典
		String dictHql = "delete from Dict where tenantId=?";
		executeHql(dictHql, tenant.getId());
		// 删除档案扩展
		String arcExtHql = "delete from ArcExt where tenantId=?";
		executeHql(arcExtHql, tenant.getId());
		// 删除提醒扩展
		String warnHql = "delete from WarnType where tenantId=?";
		executeHql(warnHql, tenant.getId());
		// 删除费用类型
		String expensesHql = "delete from Expenses where tenantId=?";
		executeHql(expensesHql, tenant.getId());
		// 删除日志
		String logHql = "delete from Log where tenant.id=?";
		executeHql(logHql, tenant.getId());
		// 删除租户
		super.delete(tenant);
	}

	public List<Tenant> listByUser(String userId) {
		String hql = "select t from Tenant t,User u where t in elements(u.tenantList) and u.id=?";
		return listByHql(hql, userId);
	}

	public String getMaxNumber(String cityId) {
		String number = "";
		String hql = "from Tenant where city.id=? order by number desc";
		List<Tenant> list = listByHql(hql, cityId);
		if (CollectionUtils.isNotEmpty(list)) {
			number = list.get(0).getNumber();
		}
		return number;
	}

}
