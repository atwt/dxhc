package luxing.web.convert;

import java.beans.PropertyEditorSupport;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author zzc
 *
 */
public class StringConvertEditor extends PropertyEditorSupport {

	public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.isNotBlank(text)) {
			// String类型转换，将所有传递进来的String进行HTML编码，防止XSS攻击
			setValue(StringEscapeUtils.escapeHtml4(text.trim()));
		} else {
			setValue(null);
		}
	}

	public String getAsText() {
		Object value = getValue();
		return value != null ? value.toString() : "";
	}
}
