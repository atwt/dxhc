package luxing.web.interceptor;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import luxing.util.LoginUserUtil;
import luxing.util.ManageLoginUserUtil;
import luxing.util.RequestUtil;
import luxing.util.ResourceUtil;
import manage.sys.entity.Menu;
import manage.sys.entity.UserSession;
import manage.sys.service.MenuService;
import manage.sys.service.UserSessionService;

/**
 * 权限拦截器
 * 
 * @author zzc
 * 
 */
public class AuthInterceptor implements HandlerInterceptor {

	@Autowired
	private MenuService menuService;
	@Autowired
	private UserSessionService userSessionService;

	private List<String> excludeUrls;

	/**
	 * 在controller前拦截
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		// 用户访问的资源地址
		String url = RequestUtil.getRequestPath(request);
		boolean authSuccess = false;
		// 1、是否需要权限验证
		if (!excludeUrls.contains(url)) {
			// 2、是否登陆
			String userId = null;
			boolean isUser = true;
			if (LoginUserUtil.getUser() != null) {
				// 普通用户
				userId = LoginUserUtil.getUser().getId();
			} else if (ManageLoginUserUtil.getUser() != null) {
				// 后台管理员用户
				isUser = false;
				userId = ManageLoginUserUtil.getUser().getId();
			}
			if (StringUtils.isNotBlank(userId)) {
				// 判断用session是否一致
				if (isUser) {
					UserSession userSession = userSessionService.getByUser(userId);
					if (!StringUtils.equals(userSession.getSessionId(), request.getSession().getId())) {
						// 如果用户没有登录
						forward(request, response);
					}
				}
				if (ResourceUtil.menuMap.get(url) != null) {
					// url是菜单
					Menu menu = null;
					if (isUser) {
						menu = menuService.getByUserAndUrl(userId, url);
					} else {
						menu = menuService.getByManageUserAndUrl(userId, url);
					}
					if (menu != null) {
						// 是菜单url并且拥有权限
						authSuccess = true;
						return authSuccess;
					} else {
						// 没有菜单权限
						return authSuccess;
					}
				} else {
					// 不是菜单url，无需过滤。不需要验证
					authSuccess = true;
					return authSuccess;
				}
			} else {
				// 如果用户没有登录
				forward(request, response);
			}
		}
		// 不需要验证
		authSuccess = true;
		return authSuccess;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object,
			ModelAndView modelAndView) throws Exception {
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object,
			Exception exception) throws Exception {
	}

	private void forward(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect(request.getServletContext().getContextPath() + "/common/timeout.jsp");
	}

	public List<String> getExcludeUrls() {
		return excludeUrls;
	}

	public void setExcludeUrls(List<String> excludeUrls) {
		this.excludeUrls = excludeUrls;
	}

}
