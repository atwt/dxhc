package luxing.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

/**
 * 
 * @author zzc
 *
 */
public class BaseTag extends TagSupport {

	private static final long serialVersionUID = 2756240323491357222L;

	private static final Logger logger = Logger.getLogger(BaseTag.class);

	public int doStartTag() throws JspException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspException {
		JspWriter out = null;
		StringBuffer sb = new StringBuffer();
		try {
			out = this.pageContext.getOut();
			sb.append("<script type=\"text/javascript\" src=\"plug-in/jquery/jquery-1.8.3.min.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/extend/dateformat.js\"></script>");
			sb.append(
					"<link id=\"easyuiTheme\" rel=\"stylesheet\" href=\"plug-in/easyui/easyui.css\" type=\"text/css\"></link>");
			sb.append(
					"<link id=\"easyuiTheme\" rel=\"stylesheet\" href=\"plug-in/easyui/main.css\" type=\"text/css\"></link>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/easyui/jquery.easyui.min.1.3.2.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/easyui/easyui.extend.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/My97DatePicker/WdatePicker.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/layer/layer.js\"></script>");
			sb.append(
					"<script type=\"text/javascript\" src=\"plug-in/lhgDialog/lhgdialog.min.js?skin=metrole\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/extend/crud.js\"></script>");

			sb.append("<script type=\"text/javascript\" src=\"plug-in/extend/json2.js\" ></script>");
			out.print(sb.toString());
			out.flush();
		} catch (IOException e) {
			logger.error(e);
		} finally {
			if (out != null) {
				try {
					out.clearBuffer();
					sb.setLength(0);
					sb = null;
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		return EVAL_PAGE;
	}

}
