package luxing.web.tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 
 * 类描述：列表字段处理项目
 * 
 * zzc
 * 
 * @date： 日期：2012-12-7 时间：上午10:17:45
 * 
 * @version 1.0
 */
public class GridColumnTag extends TagSupport {
	private static final long serialVersionUID = -6558305079195004859L;
	protected String title;
	protected String field;
	protected Integer width = 60;
	protected Integer showLen;
	protected String rowspan;
	protected String colspan;
	protected String align = "left";
	protected String dateFormat;
	protected String formatter;
	protected String replace;
	protected String treeField;
	protected String dictGroup;
	protected String dictExt;
	protected String style;
	protected String queryMode = "single";
	protected String queryShowType;
	protected boolean hidden = false;
	protected boolean sortable = true;
	protected boolean query = false;

	public int doEndTag() throws JspTagException {
		Tag t = findAncestorWithClass(this, GridTag.class);
		GridTag parent = (GridTag) t;
		parent.setColumn(title, field, width, showLen, rowspan, colspan, align, dateFormat, formatter, replace,
				treeField, dictGroup, dictExt, style, queryMode, queryShowType, hidden, sortable, query);
		return EVAL_PAGE;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setField(String field) {
		this.field = field;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public void setShowLen(Integer showLen) {
		this.showLen = showLen;
	}

	public void setRowspan(String rowspan) {
		this.rowspan = rowspan;
	}

	public void setColspan(String colspan) {
		this.colspan = colspan;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

	public void setReplace(String replace) {
		this.replace = replace;
	}

	public void setTreeField(String treeField) {
		this.treeField = treeField;
	}

	public void setDictGroup(String dictGroup) {
		this.dictGroup = dictGroup;
	}

	public void setDictExt(String dictExt) {
		this.dictExt = dictExt;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public void setQueryMode(String queryMode) {
		this.queryMode = queryMode;
	}

	public void setQueryShowType(String queryShowType) {
		this.queryShowType = queryShowType;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public void setSortable(boolean sortable) {
		this.sortable = sortable;
	}

	public void setQuery(boolean query) {
		this.query = query;
	}

}
