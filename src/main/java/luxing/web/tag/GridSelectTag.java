package luxing.web.tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import luxing.util.StringUtil;

/**
 * 
 * 类描述：选择器标签
 * 
 * @author: zzc @date： 日期：2012-12-7 时间：上午10:17:45
 * @version 1.0
 */
public class GridSelectTag extends TagSupport {

	private static final long serialVersionUID = 6601115027520228621L;

	private static final Logger logger = Logger.getLogger(GridSelectTag.class);

	protected String valueId;
	protected String nameId;
	protected String gridName;
	protected String gridField;
	protected String title;
	protected String url;
	protected String callback;
	protected String width;
	protected String height;
	protected Boolean isclear = false;

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		JspWriter out = null;
		try {
			out = this.pageContext.getOut();
			out.print(page().toString());
			out.flush();
		} catch (Exception e) {
			logger.error(e);
		}
		return EVAL_PAGE;
	}

	public StringBuffer page() {
		StringBuffer sb = new StringBuffer();
		if (isclear) {
			sb.append("<a href=\"#\"  onClick=\"clearAll_" + nameId + StringUtil.replace("();\">{0}</a>", "{0}", "清空"));
		}
		sb.append("<script type=\"text/javascript\">");
		sb.append("var windowapi = frameElement.api, W = windowapi.opener;");
		sb.append("function choose_" + nameId + "(){");
		sb.append("var url = ").append("'").append(url).append("';");
		sb.append("var initValue = ").append("$(\'#" + valueId + "\').val();");
		sb.append("url += ").append("'&ids='+initValue;");
		sb.append("$.dialog({");
		sb.append("content: \'url:\'+url,");
		sb.append("zIndex: getzIndex(),");
		if (title != null) {
			sb.append("title: \'" + title + "\',");
		}
		sb.append("lock : true,");
		sb.append("parent:windowapi,");
		if (width != null) {
			sb.append("width :\'" + width + "\',");
		} else {
			sb.append("width :800,");
		}
		if (height != null) {
			sb.append("height :\'" + height + "\',");
		} else {
			sb.append("height :459,");
		}
		sb.append("left :'50%',");
		sb.append("top :'50%',");
		sb.append("opacity : 0.4,");
		sb.append("button : [ {");
		sb.append(StringUtil.replace("name : \'{0}\',", "{0}", "确定"));
		sb.append("callback : callback_" + nameId + ",");
		sb.append("focus : true");
		sb.append("}, {");
		sb.append(StringUtil.replace("name : \'{0}\',", "{0}", "取消"));
		sb.append("callback : function() {");
		sb.append("}");
		sb.append("} ]");
		sb.append("});");
		sb.append("}");
		clearAll(sb);
		callback(sb);
		sb.append("</script>");
		return sb;
	}

	/**
	 * 清除
	 * 
	 * @param sb
	 */
	private void clearAll(StringBuffer sb) {
		if (isclear) {
			sb.append("function clearAll_" + nameId + "(){");
			sb.append("if($(\'#" + nameId + "\').length>=1){");
			sb.append("$(\'#" + nameId + "\').val('');");
			sb.append("$(\'#" + nameId + "\').blur();");
			sb.append("}");
			sb.append("$(\'#" + valueId + "\').val(\"\");");
			sb.append("}");
		}
	}

	/**
	 * 点击确定回填
	 * 
	 * @param sb
	 */
	private void callback(StringBuffer sb) {
		sb.append("function callback_" + nameId + "(){");
		sb.append("iframe = this.iframe.contentWindow;");
		if (StringUtils.isNotBlank(nameId)) {
			sb.append("var colNames =iframe.get" + gridName + "Selections(\'" + gridField + "\');	");
			sb.append("if(colNames!=\"\" && colNames!=null && colNames!=undefined){");
			sb.append("$(\'#" + nameId + "\').val(colNames);");
			sb.append("$(\'#" + nameId + "\').blur();");
			sb.append("}");
		}
		if (StringUtils.isNotBlank(valueId)) {
			sb.append("var ids =iframe.get" + gridName + "Selections(\'id\');");
			sb.append("if (ids!=\"\" && ids!=null && ids!=undefined){");
			sb.append("$(\'#" + valueId + "\').val(ids);");
			sb.append("}");
		}
		if (StringUtils.isNotBlank(callback)) {
			sb.append(callback);// 执行自定义函数
		}
		sb.append("}");
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public void setIsclear(Boolean isclear) {
		this.isclear = isclear;
	}

	public void setValueId(String valueId) {
		this.valueId = valueId;
	}

	public void setNameId(String nameId) {
		this.nameId = nameId;
	}

	public void setGridName(String gridName) {
		this.gridName = gridName;
	}

	public void setGridField(String gridField) {
		this.gridField = gridField;
	}

}
