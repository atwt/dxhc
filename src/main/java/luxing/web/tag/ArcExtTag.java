package luxing.web.tag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import luxing.util.ApplicationContextUtil;
import modules.arc.entity.ArcExt;
import modules.arc.service.ArcExtService;
import modules.arc.service.DriverExtService;
import modules.arc.service.OwnerExtService;
import modules.arc.service.VehicleExtService;

/**
 * 扩展属性
 * 
 * @author Administrator
 *
 */
public class ArcExtTag extends TagSupport {

	private static final long serialVersionUID = -5126152668627979784L;

	private static final Logger logger = Logger.getLogger(ArcExtTag.class);

	protected String type;// 类别1:tractor、2:trailer、3:whole、4:owner、5:driver
	protected String entityId;// 对象ID

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		JspWriter out = null;
		try {
			out = this.pageContext.getOut();
			out.print(page().toString());
			out.flush();
		} catch (Exception e) {
			logger.error(e);
		}
		return EVAL_PAGE;
	}

	public StringBuffer page() {
		// 扩展信息集合
		ArcExtService arcExtService = ApplicationContextUtil.getContext().getBean(ArcExtService.class);
		List<ArcExt> arcExtList = new ArrayList<ArcExt>();
		if (type.equals(ArcExt.TYPE_OWNER_DRIVER)) {
			arcExtList.addAll(arcExtService.listByType(ArcExt.TYPE_OWNER));
			arcExtList.addAll(arcExtService.listByType(ArcExt.TYPE_DRIVER));
		} else {
			arcExtList = arcExtService.listByType(type);
		}
		// 数据库记录
		Map<String, String> valueMap = new HashMap<>();
		if (type.equals(ArcExt.TYPE_VEHICLE)) {
			VehicleExtService vehicleExtService = ApplicationContextUtil.getContext().getBean(VehicleExtService.class);
			valueMap = vehicleExtService.mapByVehicle(entityId);
		} else if (type.equals(ArcExt.TYPE_OWNER) || type.equals(ArcExt.TYPE_OWNER_DRIVER)) {
			OwnerExtService ownerExtService = ApplicationContextUtil.getContext().getBean(OwnerExtService.class);
			valueMap = ownerExtService.mapByOwner(entityId);
		} else if (type.equals(ArcExt.TYPE_DRIVER)) {
			DriverExtService driverExtService = ApplicationContextUtil.getContext().getBean(DriverExtService.class);
			valueMap = driverExtService.mapByDriver(entityId);
		}
		StringBuffer sb = new StringBuffer();
		if (CollectionUtils.isNotEmpty(arcExtList)) {
			int rem = arcExtList.size() % 2;
			if (rem != 0) {
				// 如果不能整除
				if (StringUtils.isNotBlank(entityId)) {
					for (int i = 0; i < arcExtList.size() - 1;) {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i).getId());
						if (StringUtils.isNotBlank(valueMap.get(arcExtList.get(i).getId()))) {
							sb.append("\" value=\"");
							sb.append(valueMap.get(arcExtList.get(i).getId()));
						}
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						// two
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i + 1).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i + 1).getId());
						if (StringUtils.isNotBlank(valueMap.get(arcExtList.get(i + 1).getId()))) {
							sb.append("\" value=\"");
							sb.append(valueMap.get(arcExtList.get(i + 1).getId()));
						}
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						sb.append("</tr>");
						i = i + 2;
					}
					// last
					sb.append("<tr>");
					sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
					sb.append(arcExtList.get(arcExtList.size() - 1).getName());
					sb.append(":</label></td>");
					sb.append("<td class=\"value\" colspan=\"3\"><input id=\"");
					sb.append(arcExtList.get(arcExtList.size() - 1).getId());
					sb.append("\" name=\"");
					sb.append(arcExtList.get(arcExtList.size() - 1).getId());
					if (StringUtils.isNotBlank(valueMap.get(arcExtList.get(arcExtList.size() - 1).getId()))) {
						sb.append("\" value=\"");
						sb.append(valueMap.get(arcExtList.get(arcExtList.size() - 1).getId()));
					}
					sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
				} else {
					for (int i = 0; i < arcExtList.size() - 1;) {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i).getId());
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						// two
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i + 1).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i + 1).getId());
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						sb.append("</tr>");
						i = i + 2;
					}
					// last
					sb.append("<tr>");
					sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
					sb.append(arcExtList.get(arcExtList.size() - 1).getName());
					sb.append(":</label></td>");
					sb.append("<td class=\"value\" colspan=\"3\"><input id=\"");
					sb.append(arcExtList.get(arcExtList.size() - 1).getId());
					sb.append("\" name=\"");
					sb.append(arcExtList.get(arcExtList.size() - 1).getId());
					sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
				}
			} else {
				// 如果能被整除
				for (int i = 0; i < arcExtList.size();) {
					if (StringUtils.isNotBlank(entityId)) {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i).getId());
						if (StringUtils.isNotBlank(valueMap.get(arcExtList.get(i).getId()))) {
							sb.append("\" value=\"");
							sb.append(valueMap.get(arcExtList.get(i).getId()));
						}
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						// two
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i + 1).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i + 1).getId());
						if (StringUtils.isNotBlank(valueMap.get(arcExtList.get(i + 1).getId()))) {
							sb.append("\" value=\"");
							sb.append(valueMap.get(arcExtList.get(i + 1).getId()));
						}
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						sb.append("</tr>");
						i = i + 2;
					} else {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i).getId());
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						// two
						sb.append("<td align=\"right\" width=\"100\"><label class=\"Validform_label\">");
						sb.append(arcExtList.get(i + 1).getName());
						sb.append(":</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(arcExtList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(arcExtList.get(i + 1).getId());
						sb.append("\" type=\"text\" class=\"inputxt\" /></td>");
						sb.append("</tr>");
						i = i + 2;
					}
				}
			}
		}
		return sb;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
