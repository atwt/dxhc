package luxing.web.tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 
 * 类描述：列表操作项处理标签
 * 
 * zzc
 * 
 * @date： 日期：2012-12-7 时间：上午10:17:45
 * 
 * @version 1.0
 */
public class GridOptTag extends TagSupport {
	private static final long serialVersionUID = 700762680731755421L;
	private String title;
	private String funname;// 自定义函数名称
	private String exp;// 判断链接是否显示的表达式
	private String style;// 样式

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		Tag t = findAncestorWithClass(this, GridTag.class);
		GridTag parent = (GridTag) t;
		parent.setOpt(title, funname, exp, style);
		return EVAL_PAGE;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFunname(String funname) {
		this.funname = funname;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public void setStyle(String style) {
		this.style = style;
	}

}
