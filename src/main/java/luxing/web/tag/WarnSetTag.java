package luxing.web.tag;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import luxing.util.ApplicationContextUtil;
import modules.arc.service.VehicleWarnService;
import modules.warn.entity.WarnType;
import modules.warn.service.WarnTypeService;

/**
 * 提醒设置
 * 
 * @author Administrator
 *
 */
public class WarnSetTag extends TagSupport {

	private static final long serialVersionUID = 7027577051154298489L;

	private static final Logger logger = Logger.getLogger(WarnSetTag.class);

	protected String entityId;// 对象ID

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		JspWriter out = null;
		try {
			out = this.pageContext.getOut();
			out.print(page().toString());
			out.flush();
		} catch (Exception e) {
			logger.error(e);
		}
		return EVAL_PAGE;
	}

	public StringBuffer page() {
		WarnTypeService warnTypeService = ApplicationContextUtil.getContext().getBean(WarnTypeService.class);
		List<WarnType> warnTypeList = warnTypeService.listByStatus(WarnType.STATUS_ENABLE);
		VehicleWarnService vehicleWarnService = ApplicationContextUtil.getContext().getBean(VehicleWarnService.class);
		Map<String, Date> dateMap = vehicleWarnService.mapByVehicle(entityId);
		StringBuffer sb = new StringBuffer();
		if (CollectionUtils.isNotEmpty(warnTypeList)) {
			int rem = warnTypeList.size() % 2;
			if (rem != 0) {
				// 如果不能整除
				if (StringUtils.isNotBlank(entityId)) {
					for (int i = 0; i < warnTypeList.size() - 1;) {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i).getId());
						if (dateMap.get(warnTypeList.get(i).getId()) != null) {
							sb.append("\" value=\"");
							sb.append(dateMap.get(warnTypeList.get(i).getId()));
						}
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						// two
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i + 1).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						if (dateMap.get(warnTypeList.get(i + 1).getId()) != null) {
							sb.append("\" value=\"");
							sb.append(dateMap.get(warnTypeList.get(i + 1).getId()));
						}
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						sb.append("</tr>");
						i = i + 2;
					}
					// last
					sb.append("<tr>");
					sb.append("<td align=\"right\"><label class=\"Validform_label\">");
					sb.append(warnTypeList.get(warnTypeList.size() - 1).getName());
					sb.append("</label></td>");
					sb.append("<td class=\"value\" colspan=\"3\"><input id=\"");
					sb.append(warnTypeList.get(warnTypeList.size() - 1).getId());
					sb.append("\" name=\"");
					sb.append(warnTypeList.get(warnTypeList.size() - 1).getId());
					if (dateMap.get(warnTypeList.get(warnTypeList.size() - 1).getId()) != null) {
						sb.append("\" value=\"");
						sb.append(dateMap.get(warnTypeList.get(warnTypeList.size() - 1).getId()));
					}
					sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
				} else {
					for (int i = 0; i < warnTypeList.size() - 1;) {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i).getId());
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						// two
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i + 1).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						sb.append("</tr>");
						i = i + 2;
					}
					// last
					sb.append("<tr>");
					sb.append("<td align=\"right\"><label class=\"Validform_label\">");
					sb.append(warnTypeList.get(warnTypeList.size() - 1).getName());
					sb.append("</label></td>");
					sb.append("<td class=\"value\" colspan=\"3\"><input id=\"");
					sb.append(warnTypeList.get(warnTypeList.size() - 1).getId());
					sb.append("\" name=\"");
					sb.append(warnTypeList.get(warnTypeList.size() - 1).getId());
					sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
				}
			} else {
				// 如果能被整除
				for (int i = 0; i < warnTypeList.size();) {
					if (StringUtils.isNotBlank(entityId)) {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i).getId());
						if (dateMap.get(warnTypeList.get(i).getId()) != null) {
							sb.append("\" value=\"");
							sb.append(dateMap.get(warnTypeList.get(i).getId()));
						}
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						// two
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i + 1).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						if (dateMap.get(warnTypeList.get(i + 1).getId()) != null) {
							sb.append("\" value=\"");
							sb.append(dateMap.get(warnTypeList.get(i + 1).getId()));
						}
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						sb.append("</tr>");
						i = i + 2;
					} else {
						sb.append("<tr>");
						// one
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i).getId());
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						// two
						sb.append("<td align=\"right\"><label class=\"Validform_label\">");
						sb.append(warnTypeList.get(i + 1).getName());
						sb.append("</label></td>");
						sb.append("<td class=\"value\"><input id=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						sb.append("\" name=\"");
						sb.append(warnTypeList.get(i + 1).getId());
						sb.append("\" type=\"text\"  class=\"Wdate\" onClick=\"WdatePicker()\"/></td>");
						sb.append("</tr>");
						i = i + 2;
					}
				}
			}
		}
		return sb;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
