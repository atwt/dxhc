package luxing.web.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @author zzc
 *
 */
public class FormTag extends TagSupport {
	private static final long serialVersionUID = 8360534826228271024L;
	private static final Logger logger = Logger.getLogger(FormTag.class);
	private String id = "formobj";// 表单FORM ID
	private Boolean refresh = true;
	private String callback;// 回调函数
	private String beforeSubmit;// 提交前处理函数
	private String btnsub = "btn_sub";// 以ID为标记触发提交事件
	private String btnreset = "btn_reset";// 以ID为标记触发提交事件
	private Boolean dialog = true;// 是否是弹出窗口模式
	private String tiptype = "3";// 提示方式，1、弹出框3、侧边提示
	private String action;// 表单提交路径

	public int doStartTag() throws JspException {
		JspWriter out = null;
		StringBuffer sb = new StringBuffer();
		try {
			out = this.pageContext.getOut();
			sb.append("<link rel=\"stylesheet\" href=\"plug-in/Validform/css/tablefrom.css\" type=\"text/css\"/>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/Validform/js/Validform_v5.3.2.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/Validform/js/Validform_datatype.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/Validform/js/Validform_extend.js\"></script>");
			sb.append("<script type=\"text/javascript\" src=\"plug-in/layer/layer.js\"></script>");
			sb.append("<script type=\"text/javascript\">");
			sb.append("$(function(){");
			sb.append("$(\"#" + id + "\").Validform({");
			sb.append("tiptype:" + tiptype + ",");
			sb.append("btnSubmit:\"#" + btnsub + "\",");
			sb.append("btnReset:\"#" + btnreset + "\",");
			sb.append("ajaxPost:true,");
			if (beforeSubmit != null) {
				sb.append("beforeSubmit:function(curform){var tag=false;");
				sb.append("return " + beforeSubmit);
				if (beforeSubmit.indexOf("(") < 0) {
					sb.append("(curform);");
				}
				sb.append("},");
			}
			sb.append("callback:function(data){");
			if (dialog) {
				if (callback != null && callback.contains("@Override")) {// 复写默认callback
					sb.append(callback.replaceAll("@Override", "") + "(data);");
				} else {
					sb.append("var win = frameElement.api.opener;");
					// 先判断是否成功，成功再刷新父页面，否则return false
					// 如果不成功，返回值接受使用data.msg. 原有的data.responseText会报null
					sb.append(
							"if(data.success==true){frameElement.api.close();win.tip(data.msg);}else{if(data.responseText==''||data.responseText==undefined){$.messager.alert('错误', data.msg);$.Hidemsg();}else{try{var emsg = data.responseText.substring(data.responseText.indexOf('错误描述'),data.responseText.indexOf('错误信息')); $.messager.alert('错误',emsg);$.Hidemsg();}catch(ex){$.messager.alert('错误',data.responseText+\"\");$.Hidemsg();}} return false;}");
					if (refresh) {
						sb.append("win.reloadTable();");
					}
					if (StringUtils.isNotBlank(callback)) {
						sb.append("win." + callback + "(data);");
					}
				}
				// 失败tip不提示
				// sb.append("win.tip(data.msg);");
			} else {
				sb.append("" + callback + "(data);");
			}
			sb.append("}" + "});" + "});" + "</script>");
			sb.append("<form id=\"" + id + "\" ");
			sb.append(" action=\"" + action + "\" name=\"" + id + "\" autocomplete=\"off\" method=\"post\">");
			if ("btn_sub".equals(btnsub) && dialog) {
				sb.append("<input type=\"hidden\" id=\"" + btnsub + "\" class=\"" + btnsub + "\"/>");
			}
			out.print(sb.toString());
			out.flush();
		} catch (Exception e) {
			logger.error(e);
		}
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspException {
		JspWriter out = null;
		try {
			out = this.pageContext.getOut();
			out.print("</form>");
			out.flush();
		} catch (Exception e) {
			logger.error(e);
		}
		return EVAL_PAGE;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setRefresh(Boolean refresh) {
		this.refresh = refresh;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public void setBeforeSubmit(String beforeSubmit) {
		this.beforeSubmit = beforeSubmit;
	}

	public void setBtnsub(String btnsub) {
		this.btnsub = btnsub;
	}

	public void setBtnreset(String btnreset) {
		this.btnreset = btnreset;
	}

	public void setDialog(Boolean dialog) {
		this.dialog = dialog;
	}

	public void setTiptype(String tiptype) {
		this.tiptype = tiptype;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
