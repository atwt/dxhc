package luxing.web.model;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zzc desc 降序
 */
public enum SortDirection {
	// 升序
	asc,
	// 降序
	desc;

	public static SortDirection toEnum(String order) {
		if (StringUtils.isEmpty(order)) {
			// 默认排序
			return asc;
		}
		for (SortDirection item : SortDirection.values()) {
			if (item.toString().equals(order)) {
				return item;
			}
		}
		// 默认排序
		return asc;
	}
}
