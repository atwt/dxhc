package luxing.web.model;

import java.util.Map;

/**
 * 操作返回
 * 
 * @author
 * 
 */
public class Result {

	private String code = "200";// 操作返回码
	private String msg = "操作成功";// 提示信息
	private Object data = null;// 单个对象信息
	private boolean success = true;// 是否成功,页面使用
	private Map<String, Object> attributes;// 其他参数

	public static Result success(String msg) {
		Result result = new Result();
		result.setMsg(msg);
		return result;
	}

	public static Result success(Object data, String msg) {
		Result result = new Result();
		result.setData(data);
		result.setMsg(msg);
		return result;
	}

	public static Result error(String msg) {
		Result result = new Result();
		result.setCode("500");
		result.setSuccess(false);
		result.setMsg(msg);
		return result;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

}
