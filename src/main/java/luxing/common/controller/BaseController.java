package luxing.common.controller;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;

import luxing.util.ListViewUtils;
import luxing.web.convert.DateConvertEditor;
import luxing.web.convert.StringConvertEditor;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/baseController")
public class BaseController {

	private static final Logger logger = Logger.getLogger(BaseController.class);

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new DateConvertEditor());
		binder.registerCustomEditor(String.class, new StringConvertEditor());
	}

	protected void listView(HttpServletResponse response, DataGrid dataGrid) {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		JSONObject jsonObject = ListViewUtils.getJson(dataGrid);
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
			pw.write(jsonObject.toString());
			pw.flush();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			try {
				pw.close();
				jsonObject.clear();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

}
