package luxing.common.service;

import manage.tenant.entity.Tenant;
import modules.sys.entity.User;

/**
 * 短信发送
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 14:58
 */
public interface SmsService {

	String sendSms(String phone, String templateCode, String templateParam, Tenant tenant);

	String sendSms(String phone, String templateCode, String templateParam, Tenant tenant, User user);
}
