package luxing.common.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.SmsService;
import luxing.util.LoginUserUtil;
import luxing.util.SmsUtil;
import manage.tenant.entity.Tenant;
import modules.sys.entity.Bill;
import modules.sys.entity.User;

/**
 * 短信发送
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-26 16:22
 */
@Service("smsService")
@Transactional
public class SmsServiceImpl extends CommonServiceImpl implements SmsService {

	public String sendSms(String phone, String templateCode, String templateParam, Tenant tenant) {
		// 扣费
		BigDecimal balance = tenant.getBalance().subtract(new BigDecimal(0.1));
		tenant.setBalance(balance);
		// 更新租户信息
		saveOrUpdate(tenant);
		// 账户明细
		Bill bill = new Bill(tenant, "短信费", Bill.TYPE_EXPENSE, new BigDecimal(0.1), new Date(),
				LoginUserUtil.getUser().getName(), phone);
		save(bill);
		// 发送短信
		return SmsUtil.Send(phone, templateCode, templateParam);
	}

	public String sendSms(String phone, String templateCode, String templateParam, Tenant tenant,
			User user) {
		// 扣费
		BigDecimal balance = tenant.getBalance().subtract(new BigDecimal(0.1));
		tenant.setBalance(balance);
		// 更新租户信息
		saveOrUpdate(tenant);
		// 账户明细
		Bill bill = new Bill(tenant, "短信费", Bill.TYPE_EXPENSE, new BigDecimal(0.1), new Date(),
				user.getName(), phone);
		save(bill);
		// 发送短信
		return SmsUtil.Send(phone, templateCode, templateParam);
	}
}
