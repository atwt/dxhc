package luxing.common.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.dao.CommonDao;
import luxing.common.service.CommonService;
import luxing.hibernate.CriteriaQuery;

@Service("commonService")
@Transactional
public class CommonServiceImpl implements CommonService {

    @Resource
    public CommonDao commonDao;

    public <T> Serializable save(T entity) {
        return commonDao.save(entity);
    }

    public <T> void saveOrUpdate(T entity) {
        commonDao.saveOrUpdate(entity);
    }

    public <T> void batchSave(List<T> entitys) {
        commonDao.batchSave(entitys);
    }

    public <T> void delete(T entity) {
        commonDao.delete(entity);
    }

    public <T> T get(Class<T> class1, Serializable id) {
        return commonDao.get(class1, id);
    }

    public <T> List<T> listAll(Class<T> class1) {
        return commonDao.listAll(class1);
    }

    public <T> T getUniqueByProperty(Class<T> entityClass, String propertyName, Object value) {
        return commonDao.getUniqueByProperty(entityClass, propertyName, value);
    }

    public <T> List<T> listByProperty(Class<T> entityClass, String propertyName, Object value) {
        return commonDao.listByProperty(entityClass, propertyName, value);
    }

    public <T> T getUniqueByHql(String hql, Object... param) {
        return commonDao.getUniqueByHql(hql, param);
    }

    public <T> List<T> listBySql(String query) {
        return commonDao.listBySql(query);
    }

    public <T> List<T> listByPropertyOrder(Class<T> entityClass, String propertyName, Object value, boolean isAsc) {
        return commonDao.listByPropertyOrder(entityClass, propertyName, value, isAsc);
    }

    public void listByPage(final CriteriaQuery cq, final boolean ispage) {
        commonDao.listByPage(cq, ispage);
    }

    public <T> List<T> listByCriteria(final CriteriaQuery cq) {
        return commonDao.listByCriteria(cq);
    }

    public <T> List<T> listByHql(String hql, Object... param) {
        return this.commonDao.listByHql(hql, param);
    }

    public Integer executeHql(String hql, Object... param) {
        return commonDao.executeHql(hql, param);
    }

    public List<Map<String, Object>> listBySql(String sql, Object... objs) {
        return commonDao.listBySql(sql, objs);
    }

    public <T> List<T> listBySql(final String sql, final Class<T> entityClass, Map<String, Object> parameters) {
        return commonDao.listBySql(sql, entityClass, parameters);
    }

    public Integer executeSql(String sql, Object... param) {
        return commonDao.executeSql(sql, param);
    }
}
