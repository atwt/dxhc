package luxing.common.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import luxing.hibernate.CriteriaQuery;

public interface CommonService {

	<T> Serializable save(T entity);

	<T> void saveOrUpdate(T entity);

	<T> void delete(T entity);

	<T> void batchSave(List<T> entitys);

	<T> T get(Class<T> entityClass, Serializable id);

	<T> List<T> listAll(Class<T> entityClass);

	<T> T getUniqueByProperty(Class<T> entityClass, String propertyName, Object value);

	<T> List<T> listByProperty(Class<T> entityClass, String propertyName, Object value);

	<T> List<T> listByPropertyOrder(Class<T> entityClass, String propertyName, Object value, boolean isAsc);

	<T> T getUniqueByHql(String hql, Object... param);

	void listByPage(final CriteriaQuery cq, final boolean ispage);

	<T> List<T> listByCriteria(final CriteriaQuery cq);

	<T> List<T> listByHql(String hql, Object... param);

	Integer executeHql(String hql, Object... param);

	<T> List<T> listBySql(String sql);

	List<Map<String, Object>> listBySql(String sql, Object... objs);

	<T> List<T> listBySql(final String sql, final Class<T> entityClass, Map<String, Object> parameters);

	Integer executeSql(String sql, Object... param);
}
