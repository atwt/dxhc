package luxing.hibernate;

/**
 * HQL 规则 常量 Created by jue on 14-8-23.
 */
public enum QueryRuleEnum {

	GE(">=", "大于等于"), LE("<=", "小于等于"), EQ("=", "等于"), LIKE("LIKE", "左右模糊");

	private String value;

	private String msg;

	QueryRuleEnum(String value, String msg) {
		this.value = value;
		this.msg = msg;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public static QueryRuleEnum getByValue(String value) {
		for (QueryRuleEnum val : values()) {
			if (val.getValue().equals(value)) {
				return val;
			}
		}
		return null;
	}
}
