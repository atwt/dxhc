package luxing.util;

import org.apache.commons.lang3.StringUtils;

import luxing.web.model.LoginUser;
import modules.sys.entity.User;

/**
 * 对在线用户的管理
 * 
 * @author JueYue
 * @date 2013-9-28
 * @version 1.0
 */
public class LoginUserUtil {

	/**
	 * 用户登录，向session中增加用户信息
	 * 
	 * @param sessionId
	 * @param loginUser
	 */
	public static void addLoginUser(String sessionId, LoginUser loginUser) {
		RequestUtil.getSession().setAttribute(sessionId, loginUser);
	}

	/**
	 * 用户退出登录 从Session中删除用户信息 sessionId
	 */
	public static void removeLoginUser(String sessionId) {
		RequestUtil.getSession().removeAttribute(sessionId);
	}

	/**
	 * 根据sessionId 得到Client 对象
	 * 
	 * @param sessionId
	 */
	public static LoginUser getLoginUser(String sessionId) {
		if (StringUtils.isNotBlank(sessionId) && RequestUtil.getSession() != null
				&& RequestUtil.getSession().getAttribute(sessionId) != null) {
			Object obj = RequestUtil.getSession().getAttribute(sessionId);
			if (obj instanceof LoginUser) {
				return (LoginUser) obj;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * 得到Client 对象
	 */
	public static LoginUser getLoginUser() {
		String sessionId = null;
		if (RequestUtil.getSession() != null) {
			sessionId = RequestUtil.getSession().getId();
		}
		if (StringUtils.isNotBlank(sessionId) && RequestUtil.getSession() != null
				&& RequestUtil.getSession().getAttribute(sessionId) != null) {
			Object obj = RequestUtil.getSession().getAttribute(sessionId);
			if (obj instanceof LoginUser) {
				return (LoginUser) obj;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static User getUser() {
		LoginUser loginUser = getLoginUser();
		if (loginUser != null) {
			return loginUser.getUser();
		}
		return null;
	}

}
