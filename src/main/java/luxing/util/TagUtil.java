package luxing.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * 类描述：标签工具类
 * 
 * @author: zzc @date： 日期：2012-12-28 时间：上午09:58:00
 * @version 1.1
 * @author liuht 修改不能输入双引号问题解决
 */
public class TagUtil {

	/**
	 * 
	 * 获取对象内对应字段的值
	 * 
	 * @param fields
	 */
	public static Object fieldNametoValues(String FiledName, Object o) {
		Object value = "";
		String fieldName = "";
		String childFieldName = null;
		ReflectUtil reflectHelper = new ReflectUtil(o);
		if (FiledName.indexOf("_") == -1) {
			if (FiledName.indexOf(".") == -1) {
				fieldName = FiledName;
			} else {
				fieldName = FiledName.substring(0, FiledName.indexOf("."));// 外键字段引用名
				childFieldName = FiledName.substring(FiledName.indexOf(".") + 1);// 外键字段名
			}
		} else {
			fieldName = FiledName.substring(0, FiledName.indexOf("_"));// 外键字段引用名
			childFieldName = FiledName.substring(FiledName.indexOf("_") + 1);// 外键字段名
		}
		value = reflectHelper.getMethodValue(fieldName) == null ? "" : reflectHelper.getMethodValue(fieldName);
		if (value != "" && value != null && (FiledName.indexOf("_") != -1 || FiledName.indexOf(".") != -1)) {

			if (value instanceof List) {
				Object tempValue = "";
				for (Object listValue : (List) value) {
					tempValue = tempValue.toString() + fieldNametoValues(childFieldName, listValue) + ",";
				}
				value = tempValue;
			} else {
				value = fieldNametoValues(childFieldName, value);
			}

		}
		if (value != "" && value != null) {

			value = converunicode(value.toString());
		}
		return value;
	}

	static Object converunicode(String jsonValue) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < jsonValue.length(); i++) {
			char c = jsonValue.charAt(i);
			switch (c) {
			case '\'':
				sb.append("\\\'");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			default:
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * 获取自定义函数名
	 * 
	 * @param functionname
	 * @return
	 */
	public static String getFunction(String functionname) {
		int index = functionname.indexOf("(");
		if (index == -1) {
			return functionname;
		} else {
			return functionname.substring(0, functionname.indexOf("("));
		}
	}

	/**
	 * 获取自定义函数的参数
	 * 
	 * @param functionname
	 * @return
	 */
	public static String getFunParams(String functionname) {
		int index = functionname.indexOf("(");
		String param = "";
		if (index != -1) {
			String tempParam = functionname.substring(functionname.indexOf("(") + 1, functionname.length() - 1);
			if (StringUtils.isNotBlank(tempParam)) {
				String[] params = tempParam.split(",");
				for (String string : params) {
					param += (string.indexOf("{") != -1) ? ("'\"+" + string.substring(1, string.length() - 1) + "+\"',")
							: ("'\"+rec." + string + "+\"',");
				}
			}
		}
		param += "'\"+index+\"'";// 传出行索引号参数
		return param;
	}

}
