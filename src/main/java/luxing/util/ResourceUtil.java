package luxing.util;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import manage.sys.entity.Menu;

/**
 * 项目参数工具类
 * 
 */
public class ResourceUtil {

	private static final ResourceBundle bundle = ResourceBundle.getBundle("sysconfig");

	// 菜单缓存
	public static Map<String, Menu> menuMap = new HashMap<String, Menu>();

	/**
	 * 获取配置文件参数
	 * 
	 * @param name
	 * @return
	 */
	public static final String getConfig(String key) {
		return bundle.getString(key);
	}
}
