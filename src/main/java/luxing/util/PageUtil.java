package luxing.util;

import org.apache.log4j.Logger;

import luxing.exception.ExceptionResolver;

/**
 * 类描述：分页工具类 zzc @date： 日期：2012-12-7 时间：上午10:19:14
 * 
 * @version 1.0
 */
public class PageUtil {

	private static final Logger logger = Logger.getLogger(ExceptionResolver.class);

	public static int getOffset(int rowCounts, int curPageNO, int pageSize) {
		int offset = 0;
		try {
			if (curPageNO > (int) Math.ceil((double) rowCounts / pageSize)) {
				curPageNO = (int) Math.ceil((double) rowCounts / pageSize);
			}
			// 得到第几页
			if (curPageNO <= 1) {
				curPageNO = 1;
			}
			// 得到offset
			offset = (curPageNO - 1) * pageSize;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		return offset;
	}

	public static int getcurPageNo(int rowCounts, int curPageNO, int pageSize) {
		try {
			// 得到第几页
			if (curPageNO > (int) Math.ceil((double) rowCounts / pageSize)) {
				curPageNO = (int) Math.ceil((double) rowCounts / pageSize);
			}
			if (curPageNO <= 1) {
				curPageNO = 1;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
		return curPageNO;
	}
}