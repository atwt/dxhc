/**
 * Description:等额本息工具类
 * Company: Corporation
 * @author: 凯文加内特
 * @version: 1.0
 * Created at: 2015年11月30日 下午3:45:46
 * Modification History:
 * Modified by : 
 */
package luxing.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 等额本息还款，也称定期付息，即借款人每月按相等的金额偿还贷款本息，其中每月贷款利息按月初剩余贷款本金计算并逐月结清。把按揭贷款的本金总额与利息总额相加，
 * 然后平均分摊到还款期限的每个月中。作为还款人，每个月还给银行固定金额，但每月还款额中的本金比重逐月递增、利息比重逐月递减。
 */
public class AverageCapitalPlusInterestUtil {

	/**
	 * 等额本息计算获取还款方式为等额本息的每月偿还本金和利息
	 * 
	 * 公式：每月偿还本息=〔贷款本金×月利率×(1＋月利率)＾还款月数〕÷〔(1＋月利率)＾还款月数-1〕
	 * 
	 * @param loanAmount
	 *            总借款额（贷款本金）
	 * @param rate
	 *            月利率
	 * @param period
	 *            还款总月数
	 * @return 每月偿还本金和利息
	 */
	public static BigDecimal getPerMonthPrincipalInterest(BigDecimal loanAmount, BigDecimal rate, int period) {
		BigDecimal monthIncome = loanAmount
				.multiply(rate.multiply(new BigDecimal(Math.pow(1 + rate.doubleValue(), period))))
				.divide(new BigDecimal(Math.pow(1 + rate.doubleValue(), period) - 1), 1, BigDecimal.ROUND_DOWN);
		return monthIncome;
	}

	/**
	 * 等额本息计算获取还款方式为等额本息的每月偿还利息
	 * 
	 * 公式：每月偿还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
	 * 
	 * @param loanAmount
	 *            总借款额（贷款本金）
	 * @param rate
	 *            月利率
	 * @param period
	 *            还款总月数
	 * @return 每月偿还利息
	 */
	public static Map<Integer, BigDecimal> getPerMonthInterest(BigDecimal loanAmount, BigDecimal rate, int period) {
		Map<Integer, BigDecimal> map = new HashMap<Integer, BigDecimal>();
		BigDecimal monthInterest;
		for (int i = 1; i < period + 1; i++) {
			BigDecimal multiply = loanAmount.multiply(rate);
			BigDecimal sub = new BigDecimal(Math.pow(rate.doubleValue() + 1, period))
					.subtract(new BigDecimal(Math.pow(rate.doubleValue() + 1, i - 1)));
			monthInterest = multiply.multiply(sub).divide(new BigDecimal(Math.pow(rate.doubleValue() + 1, period) - 1),
					1, BigDecimal.ROUND_DOWN);
			monthInterest = monthInterest.setScale(1, BigDecimal.ROUND_DOWN);
			map.put(i, monthInterest);
		}
		return map;
	}

	/**
	 * 等额本息计算获取还款方式为等额本息的每月偿还本金
	 * 
	 * @param loanAmount
	 *            总借款额（贷款本金）
	 * @param rate
	 *            月利率
	 * @param period
	 *            还款总月数
	 * @return 每月偿还本金
	 */
	public static Map<Integer, BigDecimal> getPerMonthPrincipal(BigDecimal loanAmount, BigDecimal rate, int period) {
		BigDecimal monthIncome = loanAmount
				.multiply(rate.multiply(new BigDecimal(Math.pow(rate.doubleValue() + 1, period))))
				.divide(new BigDecimal(Math.pow(rate.doubleValue() + 1, period) - 1), 1, BigDecimal.ROUND_DOWN);
		Map<Integer, BigDecimal> mapInterest = getPerMonthInterest(loanAmount, rate, period);
		Map<Integer, BigDecimal> mapPrincipal = new HashMap<Integer, BigDecimal>();

		for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
			mapPrincipal.put(entry.getKey(), monthIncome.subtract(entry.getValue()));
		}
		return mapPrincipal;
	}

	/**
	 * 等额本息计算获取还款方式为等额本息的总利息
	 * 
	 * @param loanAmount
	 *            总借款额（贷款本金）
	 * @param rate
	 *            月利率
	 * @param period
	 *            还款总月数
	 * @return 总利息
	 */
	public static BigDecimal getTotalInterest(BigDecimal loanAmount, BigDecimal rate, int period) {
		BigDecimal count = new BigDecimal(0);
		Map<Integer, BigDecimal> mapInterest = getPerMonthInterest(loanAmount, rate, period);
		for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
			count = count.add(entry.getValue());
		}
		return count;
	}

	/**
	 * 应还本金总和
	 * 
	 * @param loanAmount
	 *            总借款额（贷款本金）
	 * @param rate
	 *            月利率
	 * @param period
	 *            还款总月数
	 * @return 应还本金总和
	 */
	public static BigDecimal getTotalPrincipalInterest(BigDecimal loanAmount, BigDecimal rate, int period) {
		BigDecimal perMonthInterest = loanAmount
				.multiply(rate.multiply(new BigDecimal(Math.pow(rate.doubleValue() + 1, period))))
				.divide(new BigDecimal(Math.pow(rate.doubleValue() + 1, period) - 1), 1, BigDecimal.ROUND_DOWN);
		BigDecimal count = perMonthInterest.multiply(new BigDecimal(period));
		count = count.setScale(1, BigDecimal.ROUND_DOWN);
		return count;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BigDecimal loanAmount = new BigDecimal(10000); // 本金
		int period = 10;
		BigDecimal rate = new BigDecimal(0.01); // 月利率
		BigDecimal perMonthPrincipalInterest = getPerMonthPrincipalInterest(loanAmount, rate, period);
		System.out.println("等额本息---每月还款本息：" + perMonthPrincipalInterest);
		Map<Integer, BigDecimal> mapInterest = getPerMonthInterest(loanAmount, rate, period);
		System.out.println("等额本息---每月还款利息：" + mapInterest);
		Map<Integer, BigDecimal> mapPrincipal = getPerMonthPrincipal(loanAmount, rate, period);
		System.out.println("等额本息---每月还款本金：" + mapPrincipal);
		BigDecimal count = getTotalInterest(loanAmount, rate, period);
		System.out.println("等额本息---总利息：" + count);
		BigDecimal principalInterestCount = getTotalPrincipalInterest(loanAmount, rate, period);
		System.out.println("等额本息---应还本息总和：" + principalInterestCount);
	}
}