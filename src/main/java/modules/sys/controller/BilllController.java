package modules.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.LoginUserUtil;
import luxing.web.model.DataGrid;
import modules.sys.entity.Bill;
import modules.sys.service.BillService;

/**
 * 账单明细
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("billController")
public class BilllController extends BaseController {

	private static final Logger logger = Logger.getLogger(BilllController.class);

	@Autowired
	private BillService billService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index() {
		return new ModelAndView("modules/sys/bill-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param bill
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Bill bill, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Bill.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, bill, request.getParameterMap());
			cq.eq("tenant.id", LoginUserUtil.getLoginUser().getTenantId());
			cq.add();
			billService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("账单明细查询失败", e);
			throw new BusinessException("账单明细查询失败", e);
		}
	}

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "manageIndex")
	public ModelAndView manageIndex() {
		return new ModelAndView("manage/tenant/bill-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param bill
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "manageList")
	public void manageList(Bill bill, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Bill.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, bill, request.getParameterMap());
			cq.add();
			billService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("账单明细查询失败", e);
			throw new BusinessException("账单明细查询失败", e);
		}
	}

}