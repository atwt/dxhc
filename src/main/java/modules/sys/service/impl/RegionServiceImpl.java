package modules.sys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import luxing.common.service.impl.CommonServiceImpl;
import modules.sys.entity.Region;
import modules.sys.service.RegionService;

/**
 * 地区接口实现类
 * 
 * @author 方文荣
 *
 */
@Service("regionService")
public class RegionServiceImpl extends CommonServiceImpl implements RegionService {

	public List<Region> listProvince() {
		String hql = "from Region where pid=1 order by id asc";
		return listByHql(hql);
	}

	public List<Region> listCity(String pid) {
		String hql = "from Region where pid=? order by id asc";
		return listByHql(hql, pid);
	}

	public String getLicensePlate(String tenantId) {
		String hql = "select r from Region r,Tenant t where t.city.id=r.id and t.id=?";
		Region region = getUniqueByHql(hql, tenantId);
		return region.getLicensePlate();
	}
}
