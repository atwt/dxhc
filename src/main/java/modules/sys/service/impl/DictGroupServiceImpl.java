package modules.sys.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.sys.entity.DictGroup;
import modules.sys.service.DictGroupService;

/**
 * 
 * @author zzc
 *
 */
@Service("dcitGroupService")
@Transactional
public class DictGroupServiceImpl extends CommonServiceImpl implements DictGroupService {

	public DictGroup getByName(String name) {
		return getUniqueByProperty(DictGroup.class, "name", name);
	}

	public DictGroup getByCode(String code) {
		return getUniqueByProperty(DictGroup.class, "code", code);
	}

}
