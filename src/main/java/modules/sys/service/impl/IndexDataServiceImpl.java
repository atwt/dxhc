package modules.sys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.sys.entity.IndexData;
import modules.sys.service.IndexDataService;

/**
 * 
 * @author zzc
 *
 */
@Service("indexDataService")
@Transactional
public class IndexDataServiceImpl extends CommonServiceImpl implements IndexDataService {

	public List<IndexData> listByType(String type) {
		String hql = "from IndexData where type=? and tenantId=?";
		return listByHql(hql, type, LoginUserUtil.getLoginUser().getTenantId());
	}
}
