package modules.sys.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.sys.service.BillService;

/**
 * 
 * @author zzc
 *
 */
@Service("billService")
@Transactional
public class BillServiceImpl extends CommonServiceImpl implements BillService {

}
