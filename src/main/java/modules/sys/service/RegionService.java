package modules.sys.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.sys.entity.Region;

public interface RegionService extends CommonService {

	List<Region> listProvince();

	List<Region> listCity(String pid);

	String getLicensePlate(String tenantId);

}
