package modules.sys.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.sys.entity.IndexData;

public interface IndexDataService extends CommonService {

	List<IndexData> listByType(String type);

}
