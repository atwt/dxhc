package modules.sys.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 首页数据
 * 
 * @author zzc
 */
@Entity
@Table(name = "sys_index_data")
@DynamicInsert
@DynamicUpdate
public class IndexData implements Serializable {

	private static final long serialVersionUID = -1100935233970769313L;

	public static final String TYPE_CLDT = "1";// 车辆动态
	public static final String TYPE_CLZT = "2";// 车辆状态
	public static final String TYPE_TX10 = "3";// 到期提醒10天
	public static final String TYPE_TX30 = "4";// 到期提醒30天
	public static final String TYPE_TX60 = "5";// 到期提醒60天
	public static final String NAME_CURRENTAMOUNT = "currentAmount";// 当前车辆
	public static final String NAME_MONTHADD = "monthAdd";// 当月新增
	public static final String NAME_MONTHREDUCE = "monthReduce";// 当月注销
	public static final String NAME_YEARDYNAMIC = "yearDynamic";// 当年动态

	private String id;
	/** 租户 */
	private String tenantId;
	/** 名称 */
	private String name;
	/** 值 */
	private Integer value;
	/** 类型 */
	private String type;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "value")
	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "类型不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}