package modules.sys.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import manage.tenant.entity.Tenant;

/**
 * 账单明细
 * 
 * @author zzc
 */
@Entity
@Table(name = "sys_bill")
@DynamicInsert
@DynamicUpdate
public class Bill implements Serializable {

	private static final long serialVersionUID = 8233672706684160587L;
	public static final String TYPE_PAY = "1";// 充值
	public static final String TYPE_EXPENSE = "2";// 消费

	private String id;
	/** 租户 */
	private Tenant tenant;
	/** 名称 */
	private String name;
	/** 类型 */
	private String type;
	/** 金额 */
	private BigDecimal amount;
	/** 处理时间 */
	private Date dealDate;
	/** 操作人 */
	private String operator;
	/** 备注 */
	private String remarks;

	public Bill() {
		super();
	}

	public Bill(Tenant tenant, String name, String type, BigDecimal amount, Date dealDate, String operator,
			String remarks) {
		super();
		this.tenant = tenant;
		this.name = name;
		this.type = type;
		this.amount = amount;
		this.dealDate = dealDate;
		this.operator = operator;
		this.remarks = remarks;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tenant_id")
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", nullable = false, length = 10)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "deal_date")
	public Date getDealDate() {
		return dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	@Column(name = "operator", nullable = false, length = 50)
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}