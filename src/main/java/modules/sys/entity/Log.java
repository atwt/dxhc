package modules.sys.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import manage.tenant.entity.Tenant;

/**
 * TLog entity.
 * 
 * @author zzc
 */
@Entity
@Table(name = "sys_log")
@DynamicInsert
@DynamicUpdate
public class Log implements Serializable {

	private static final long serialVersionUID = 8031761754790652666L;

	public static final String OPERATE_LOGIN = "1"; // 登陆
	public static final String OPERATE_EXIT = "2"; // 退出
	public static final String OPERATE_ADD = "3"; // 增加
	public static final String OPERATE_DEL = "4"; // 删除
	public static final String OPERATE_UPDATE = "5"; // 修改
	public static final String OPERATE_UPLOAD = "6"; // 上传
	public static final String OPERATE_SMS = "7"; // 短信
	public static final String OPERATE_EXCEPTION = "9"; // 异常

	/** id */
	private String id;
	/** 租户 */
	private Tenant tenant;
	/** 手机 */
	private String phone;
	/** 用户名 */
	private String userName;
	/** 内容 */
	private String content;
	/** 操作类型 */
	private String operateType;
	/** IP */
	private String ip;
	/** 时间 */
	private Date operateTime;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tenant_id")
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Column(name = "phone", length = 11)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "user_name", length = 50)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "content", nullable = false, length = 1500)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "operate_type", nullable = false, length = 10)
	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	@Column(name = "ip", length = 300)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "operate_time", nullable = false)
	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

}