package modules.loan.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import modules.arc.entity.Owner;

/**
 * 贷款合同
 * 
 * @author zzc
 */
@Entity
@Table(name = "loan_contract")
@DynamicInsert
@DynamicUpdate
public class LoanContract implements Serializable {

	private static final long serialVersionUID = 8389823941183714255L;
	public static final String STATUS_NO = "0";// 未还款
	public static final String STATUS_ING = "1";// 还款中
	public static final String STATUS_YES = "2";// 已还完

	// average capita 等额本金
	public static final String REPAYMENTTYPE_AC = "1";
	// average capital plus intere 等额本息
	public static final String REPAYMENTTYPE_ACPI = "2";
	// equal interest and interest 等本等息
	public static final String REPAYMENTTYPE_EIAI = "3";

	private String id;
	/** 租户 */
	private String tenantId;
	/** 编号 DK20171202001 */
	private String sn;
	/** 责任人 */
	private Owner owner;
	/** 借款金额 */
	private BigDecimal principal;
	/** 月利率 % */
	private BigDecimal rate;
	/** 利息 */
	private BigDecimal interest;
	/** 期数 */
	private Integer period;
	/** 应还总额= 贷款金额+利息 */
	private BigDecimal totalAmount;
	/** 起息日(首次还款日) */
	private Date startDate;
	/** 还款方式 */
	private String repaymentType;
	/** 实还本金 */
	private BigDecimal realPrincipal;
	/** 实还利息 */
	private BigDecimal realInterest;
	/** 实还总额 */
	private BigDecimal realTotalAmount;
	/** 尚欠总额 */
	private BigDecimal oweAmount;
	/** 状态 */
	private String status;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 修改时间 */
	private Date updateDate;
	/** 修改人 */
	private String updateBy;
	/** 贷款集合 */
	private List<Loan> loanList = new ArrayList<Loan>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "sn", nullable = false, length = 50)
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Column(name = "principal", scale = 1, length = 10, nullable = false)
	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	@Column(name = "period", nullable = false)
	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	@Column(name = "rate", scale = 2, length = 10, nullable = false)
	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Column(name = "start_date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "repayment_type", nullable = false, length = 10)
	@NotBlank(message = "还款方式不能为空")
	public String getRepaymentType() {
		return repaymentType;
	}

	public void setRepaymentType(String repaymentType) {
		this.repaymentType = repaymentType;
	}

	@Column(name = "interest", scale = 1, length = 10, nullable = false)
	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	@Column(name = "total_amount", scale = 1, length = 10, nullable = false)
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Column(name = "real_principal", scale = 1, length = 10, nullable = false)
	public BigDecimal getRealPrincipal() {
		return realPrincipal;
	}

	public void setRealPrincipal(BigDecimal realPrincipal) {
		this.realPrincipal = realPrincipal;
	}

	@Column(name = "real_interest", scale = 1, length = 10, nullable = false)
	public BigDecimal getRealInterest() {
		return realInterest;
	}

	public void setRealInterest(BigDecimal realInterest) {
		this.realInterest = realInterest;
	}

	@Column(name = "real_total_amount", scale = 1, length = 10, nullable = false)
	public BigDecimal getRealTotalAmount() {
		return realTotalAmount;
	}

	public void setRealTotalAmount(BigDecimal realTotalAmount) {
		this.realTotalAmount = realTotalAmount;
	}

	@Column(name = "owe_amount", scale = 1, length = 10, nullable = false)
	public BigDecimal getOweAmount() {
		return oweAmount;
	}

	public void setOweAmount(BigDecimal oweAmount) {
		this.oweAmount = oweAmount;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "loanContract")
	@Fetch(FetchMode.SUBSELECT)
	public List<Loan> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<Loan> loanList) {
		this.loanList = loanList;
	}

	@Column(name = "create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", length = 50)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "update_date")
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "update_by", length = 50)
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}