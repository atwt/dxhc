package modules.loan.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 到期还款
 * 
 * @author zzc
 */
@Entity
@Table(name = "loan_loan")
@DynamicInsert
@DynamicUpdate
public class Loan implements Serializable {

	private static final long serialVersionUID = -9218662730549043814L;

	public static final String STATUS_NO = "0";// 未还款
	public static final String STATUS_ING = "1";// 还款中
	public static final String STATUS_YES = "2";// 已还完

	private String id;
	/** 租户 */
	private String tenantId;
	/** 编号 JK20171202001-01 */
	private String sn;
	/** 借款合同 */
	private LoanContract loanContract;
	/** 当前期数 */
	private Integer period;
	/** 应还总额= 应还本金+应还利息 */
	private BigDecimal totalAmount;
	/** 应还本金 */
	private BigDecimal principal;
	/** 应还利息 */
	private BigDecimal interest;
	/** 实还本金 */
	private BigDecimal realPrincipal;
	/** 实还利息 */
	private BigDecimal realInterest;
	/** 实还总额 */
	private BigDecimal realTotalAmount;
	/** 尚欠总额 */
	private BigDecimal oweAmount;
	/** 到期日期 */
	private Date endDate;
	/** 状态 */
	private String status;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 修改时间 */
	private Date updateDate;
	/** 修改人 */
	private String updateBy;
	/** 还款集合 */
	private List<LoanRepayment> loanRepaymentList = new ArrayList<LoanRepayment>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "sn", nullable = false, length = 50)
	@NotBlank(message = "编号不能为空")
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "loan_contract_id")
	public LoanContract getLoanContract() {
		return loanContract;
	}

	public void setLoanContract(LoanContract loanContract) {
		this.loanContract = loanContract;
	}

	@Column(name = "period", nullable = false)
	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	@Column(name = "total_amount", scale = 1, length = 10, nullable = false)
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Column(name = "principal", scale = 1, length = 10, nullable = false)
	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	@Column(name = "interest", scale = 1, length = 10, nullable = false)
	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	@Column(name = "real_principal", scale = 1, length = 10, nullable = false)
	public BigDecimal getRealPrincipal() {
		return realPrincipal;
	}

	public void setRealPrincipal(BigDecimal realPrincipal) {
		this.realPrincipal = realPrincipal;
	}

	@Column(name = "real_interest", scale = 1, length = 10, nullable = false)
	public BigDecimal getRealInterest() {
		return realInterest;
	}

	public void setRealInterest(BigDecimal realInterest) {
		this.realInterest = realInterest;
	}

	@Column(name = "real_total_amount", scale = 1, length = 10, nullable = false)
	public BigDecimal getRealTotalAmount() {
		return realTotalAmount;
	}

	public void setRealTotalAmount(BigDecimal realTotalAmount) {
		this.realTotalAmount = realTotalAmount;
	}

	@Column(name = "owe_amount", scale = 1, length = 10, nullable = false)
	public BigDecimal getOweAmount() {
		return oweAmount;
	}

	public void setOweAmount(BigDecimal oweAmount) {
		this.oweAmount = oweAmount;
	}

	@Column(name = "end_date", nullable = false)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "loan")
	@Fetch(FetchMode.SUBSELECT)
	public List<LoanRepayment> getLoanRepaymentList() {
		return loanRepaymentList;
	}

	public void setLoanRepaymentList(List<LoanRepayment> loanRepaymentList) {
		this.loanRepaymentList = loanRepaymentList;
	}

	@Column(name = "create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", length = 50)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "update_date")
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "update_by", length = 50)
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}