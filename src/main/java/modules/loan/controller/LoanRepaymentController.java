package modules.loan.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.loan.entity.LoanRepayment;
import modules.loan.service.LoanRepaymentService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/loanRepaymentController")
public class LoanRepaymentController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(LoanRepaymentController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private LoanRepaymentService loanRepaymentService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/loan/loanRepayment-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param repayment
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(LoanRepayment repayment, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(LoanRepayment.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, repayment, request.getParameterMap());
			cq.add();
			loanRepaymentService.listByPage(cq, true);
			dataGrid.setFooter("principal,interest,totalAmount,loan.sn:合计");
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("还款记录查询失败", e);
			throw new BusinessException("还款记录查询失败", e);
		}
	}

	/**
	 * 打印页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goPrint")
	public ModelAndView goPrint(LoanRepayment loanRepayment, HttpServletRequest req) {
		if (StringUtils.isNotBlank(loanRepayment.getId())) {
			loanRepayment = loanRepaymentService.get(LoanRepayment.class, loanRepayment.getId());
			req.setAttribute("loanRepaymentPage", loanRepayment);
		}
		return new ModelAndView("modules/loan/loanRepayment-print");
	}

	/**
	 * 作废
	 * 
	 * @param loanRepayment
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doVoid")
	@ResponseBody
	public Result doVoid(LoanRepayment loanRepayment, HttpServletRequest request) {
		String msg = null;
		try {
			loanRepayment = loanRepaymentService.get(LoanRepayment.class, loanRepayment.getId());

			loanRepayment.setStatus(LoanRepayment.STATUS_DISABLE);
			loanRepaymentService.doVoid(loanRepayment);
			msg = "编号\"" + loanRepayment.getLoan().getSn() + "\"收款作废成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "编号\"" + loanRepayment.getLoan().getSn() + "\"收款作废失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
