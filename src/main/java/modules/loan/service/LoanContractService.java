package modules.loan.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.loan.entity.LoanContract;

public interface LoanContractService extends CommonService {

	void save(LoanContract loanContract);

	List<LoanContract> listByOwner(String ownerId);

	List<LoanContract> listNonByOwner(String ownerId);
}
