package modules.loan.service;

import luxing.common.service.CommonService;
import modules.loan.entity.Loan;
import modules.loan.entity.LoanRepayment;

public interface LoanService extends CommonService {

	void update(Loan loan);

	void repayment(Loan loan, LoanRepayment loanRepayment);

}
