package modules.ins.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * 保险险种
 * 
 * @author zzc
 */
@Entity
@Table(name = "ins_insurance_kind")
@DynamicInsert
@DynamicUpdate
public class InsuranceKind implements Serializable {

	private static final long serialVersionUID = 749395611613590986L;

	private String id;
	/** 租户 */
	private String tenantId;
	/** 保险 */
	private Insurance insurance;
	/** 险种 */
	private String insKind;
	/** 保险金额/责任限额 */
	private BigDecimal insAmount;
	/** 保险费 */
	private BigDecimal premium;
	/** 险种名称 */
	private String insKindName;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "insurance_id")
	public Insurance getInsurance() {
		return insurance;
	}

	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}

	@Column(name = "ins_kind", nullable = false, length = 32)
	public String getInsKind() {
		return insKind;
	}

	public void setInsKind(String insKind) {
		this.insKind = insKind;
	}

	@Column(name = "ins_amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getInsAmount() {
		return insAmount;
	}

	public void setInsAmount(BigDecimal insAmount) {
		this.insAmount = insAmount;
	}

	@Column(name = "premium", nullable = false, scale = 1, length = 10)
	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	@Transient
	public String getInsKindName() {
		return insKindName;
	}

	public void setInsKindName(String insKindName) {
		this.insKindName = insKindName;
	}

}