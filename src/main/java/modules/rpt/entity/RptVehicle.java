package modules.rpt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 车辆动态报表
 * 
 * @author zzc
 */
@Entity
@Table(name = "rpt_vehicle")
@DynamicInsert
@DynamicUpdate
public class RptVehicle implements Serializable {

	private static final long serialVersionUID = 8157561130632088669L;

	public static final String TYPE_DYNAMIC = "1";// 车辆动态报表
	public static final String TYPE_STATUS = "2";// 车辆状态报表

	private String id;
	/** 租户 */
	private String tenantId;
	/** 类型:1、车辆动态报表，2、车辆状态报表 */
	private String type;
	/** 分类名称 */
	private String name;
	/** 报表年份 */
	private String year;
	/** 排序 */
	private Integer sort;
	/** 1月 */
	private Integer m1;
	/** 2月 */
	private Integer m2;
	/** 3月 */
	private Integer m3;
	/** 4月 */
	private Integer m4;
	/** 5月 */
	private Integer m5;
	/** 6月 */
	private Integer m6;
	/** 7月 */
	private Integer m7;
	/** 8月 */
	private Integer m8;
	/** 9月 */
	private Integer m9;
	/** 10月 */
	private Integer m10;
	/** 11月 */
	private Integer m11;
	/** 12月 */
	private Integer m12;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "类型不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "rpt_year", nullable = false, length = 10)
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Column(name = "sort")
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Column(name = "m1")
	public Integer getM1() {
		return m1;
	}

	public void setM1(Integer m1) {
		this.m1 = m1;
	}

	@Column(name = "m2")
	public Integer getM2() {
		return m2;
	}

	public void setM2(Integer m2) {
		this.m2 = m2;
	}

	@Column(name = "m3")
	public Integer getM3() {
		return m3;
	}

	public void setM3(Integer m3) {
		this.m3 = m3;
	}

	@Column(name = "m4")
	public Integer getM4() {
		return m4;
	}

	public void setM4(Integer m4) {
		this.m4 = m4;
	}

	@Column(name = "m5")
	public Integer getM5() {
		return m5;
	}

	public void setM5(Integer m5) {
		this.m5 = m5;
	}

	@Column(name = "m6")
	public Integer getM6() {
		return m6;
	}

	public void setM6(Integer m6) {
		this.m6 = m6;
	}

	@Column(name = "m7")
	public Integer getM7() {
		return m7;
	}

	public void setM7(Integer m7) {
		this.m7 = m7;
	}

	@Column(name = "m8")
	public Integer getM8() {
		return m8;
	}

	public void setM8(Integer m8) {
		this.m8 = m8;
	}

	@Column(name = "m9")
	public Integer getM9() {
		return m9;
	}

	public void setM9(Integer m9) {
		this.m9 = m9;
	}

	@Column(name = "m10")
	public Integer getM10() {
		return m10;
	}

	public void setM10(Integer m10) {
		this.m10 = m10;
	}

	@Column(name = "m11")
	public Integer getM11() {
		return m11;
	}

	public void setM11(Integer m11) {
		this.m11 = m11;
	}

	@Column(name = "m12")
	public Integer getM12() {
		return m12;
	}

	public void setM12(Integer m12) {
		this.m12 = m12;
	}

}