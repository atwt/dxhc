package modules.rpt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.web.model.DataGrid;
import modules.fin.entity.Income;
import modules.fin.service.IncomeService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/rptIncomedController")
public class RptIncomedController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RptIncomedController.class);

	@Autowired
	private IncomeService incomeService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/rpt/rptIncomed-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param income
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Income income, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Income.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, income, request.getParameterMap());
			// 费用类型
			String exs = request.getParameter("exs");
			if (StringUtils.isNotBlank(exs)) {
				cq.eq("expenses.id", exs);
			}
			cq.eq("status", Income.STATUS_YES);
			cq.add();
			incomeService.listByPage(cq, true);
			dataGrid.setFooter("incomeAmount,discountAmount,settleAmount,vehicle.sn:合计");
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("应收车辆款报表查询失败", e);
			throw new BusinessException("应收车辆款报表查询失败", e);
		}
	}

}
