package modules.warn.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.common.service.SmsService;
import luxing.constant.SmsConfig;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.DateUtil;
import luxing.util.JsonUtil;
import luxing.util.LoginUserUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import manage.tenant.entity.Tenant;
import manage.tenant.service.TenantService;
import modules.ins.entity.Insurance;
import modules.ins.service.InsuranceService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/insuranceWarnController")
public class InsuranceWarnController extends BaseController {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(InsuranceWarnController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private InsuranceService insuranceService;
	@Autowired
	private TenantService tenantService;
	@Autowired
	private SmsService smsService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/warn/insuranceWarn-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param insurance
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Insurance insurance, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Insurance.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, insurance, request.getParameterMap());
			// 提醒状态
			String warn = request.getParameter("ws");
			warn = StringUtils.isBlank(warn) ? Insurance.WARN_NORMAL : warn;
			if (!warn.equals(Insurance.WARN_ALL)) {
				cq.eq("warn", warn);
			}
			// 提醒类型
			String warnDateType = request.getParameter("warnDateType");
			String endDateBegin = request.getParameter("endDate_begin");
			String endDateEnd = request.getParameter("endDate_end");
			if (StringUtils.isNotBlank(endDateBegin) || StringUtils.isNotBlank(endDateEnd)
					|| (warnDateType != null && warnDateType.equals("2"))) {
				// 按时间范围
				if (StringUtils.isNotBlank(endDateBegin)) {
					cq.ge("endDate", DateUtil.strToDate(endDateBegin, "yyyy-MM-dd"));
				}
				if (StringUtils.isNotBlank(endDateEnd)) {
					cq.le("endDate", DateUtil.strToDate(endDateEnd, "yyyy-MM-dd"));
				}
			} else if ((warnDateType == null || warnDateType.equals("1"))
					&& (StringUtils.isBlank(endDateBegin) && StringUtils.isBlank(endDateEnd))) {
				// 下月到期
				Calendar calBegin = Calendar.getInstance();
				calBegin.add(Calendar.YEAR, -100);
				cq.ge("endDate", calBegin.getTime());
				Calendar calEnd = Calendar.getInstance();
				calEnd.add(Calendar.MONTH, 2);
				calEnd.set(Calendar.DATE, 0);
				cq.le("endDate", calEnd.getTime());
			}
			cq.add();
			insuranceService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("保险到期查询失败", e);
			throw new BusinessException("保险到期查询失败", e);
		}
	}

	/**
	 * 发送短信页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goSendSms")
	public ModelAndView goSendSms(Insurance insurance, HttpServletRequest req) {
		if (StringUtils.isNotBlank(insurance.getId())) {
			insurance = insuranceService.get(Insurance.class, insurance.getId());
			req.setAttribute("insurancePage", insurance);
		}
		return new ModelAndView("modules/warn/insuranceWarn-sms");
	}

	/**
	 * 发送短信
	 * 
	 * @param insurance
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doSendSms")
	@ResponseBody
	public Result doSendSms(Insurance insurance, HttpServletRequest request) {
		String msg = null;
		String phone = request.getParameter("ownerPhone");
		try {
			Tenant tenant = tenantService.get(Tenant.class, LoginUserUtil.getLoginUser().getTenantId());
			// 判断用户余额
			if (tenant.getBalance().compareTo(Tenant.BALANCE_MIN) < 0) {
				msg = "账户余额不足,发送失败.";
				return Result.error(msg);
			}
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("plateNumber", insurance.getVehicle().getPlateNumber());
			if (insurance.getType().equals(Insurance.TYPE_SALI)) {
				paramMap.put("warnTypeName", "交强险");
			} else {
				paramMap.put("warnTypeName", "商业险");
			}
			paramMap.put("warnDate", DateUtil.dateToStr(insurance.getEndDate(), "yyyy-MM-dd"));
			String templateParam = JsonUtil.toJson(paramMap);
			smsService.sendSms(phone, SmsConfig.template_vehicle_warn, templateParam, tenant);
			msg = "号码\"" + phone + "\"短信发送成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_SMS);
		} catch (Exception e) {
			msg = "号码\"" + phone + "\"短信发送失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 启动
	 * 
	 * @return
	 */
	@RequestMapping(params = "doStart")
	@ResponseBody
	public Result doStart(Insurance insurance, HttpServletRequest request) {
		String msg = null;
		try {
			insurance = insuranceService.get(Insurance.class, insurance.getId());
			insurance.setWarn(Insurance.WARN_NORMAL);
			insuranceService.saveOrUpdate(insurance);
			msg = "车辆\"" + insurance.getVehicle().getPlateNumber() + "\"保险提醒启动成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "车辆\"" + insurance.getVehicle().getPlateNumber() + "\"保险提醒启动失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 暂停
	 * 
	 * @return
	 */
	@RequestMapping(params = "doPause")
	@ResponseBody
	public Result doPause(Insurance insurance, HttpServletRequest request) {
		String msg = null;
		try {
			insurance = insuranceService.get(Insurance.class, insurance.getId());
			insurance.setWarn(Insurance.WARN_PAUSE);
			insuranceService.saveOrUpdate(insurance);
			msg = "车辆\"" + insurance.getVehicle().getPlateNumber() + "\"保险提醒暂停成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "车辆\"" + insurance.getVehicle().getPlateNumber() + "\"保险提醒暂停失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
