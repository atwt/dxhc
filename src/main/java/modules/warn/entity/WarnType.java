package modules.warn.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * 提醒类型
 * 
 * @author zzc
 */
@Entity
@Table(name = "warn_type")
@DynamicInsert
@DynamicUpdate
public class WarnType implements Serializable {

	private static final long serialVersionUID = -3429536919279188658L;

	public static final String STATUS_ENABLE = "0";// 启用
	public static final String STATUS_DISABLE = "1";// 禁用

	private String id;
	/** 租户 */
	private String tenantId;
	/** 名称 */
	@Excel(name = "名称")
	private String name;
	/** 状态 */
	@Excel(name = "状态 ", replace = { "启用_0", "禁用_1" })
	private String status;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}