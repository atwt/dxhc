package modules.safe.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import modules.arc.entity.Vehicle;

/**
 * 事故记录
 * 
 * @author zzc
 */
@Entity
@Table(name = "safe_accident")
@DynamicInsert
@DynamicUpdate
public class Accident implements Serializable {

	private static final long serialVersionUID = 1126348252016029844L;

	public static final String STATUS_NO = "0";// 未处理
	public static final String STATUS_YES = "1";// 已处理

	private String id;
	/** 租户 */
	private String tenantId;
	/** 车辆 */
	private Vehicle vehicle;
	/** 驾驶员 */
	private String driverName;
	/** 事故地点 */
	private String location;
	/** 事故时间 */
	private Date accidentDate;
	/** 事故经过 */
	private String detail;
	/** 登记人 */
	private String registerBy;
	/** 登记时间 */
	private Date registerDate;
	/** 状态 */
	private String status;
	/** 事故性质 */
	private String type;
	/** 责任比例(%) */
	private Integer responsible;
	/** 报赔金额 */
	private BigDecimal applyAmount;
	/** 实赔金额 */
	private BigDecimal realAmount;
	/** 保险公司 */
	private String insCompany;
	/** 结案日期 */
	private Date endDate;
	/** 备注 */
	private String remarks;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@Column(name = "driver_name", nullable = false, length = 50)
	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	@Column(name = "location", nullable = false, length = 50)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name = "accident_date", nullable = false)
	public Date getAccidentDate() {
		return accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}

	@Column(name = "detail", length = 255)
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Column(name = "register_by", length = 32)
	public String getRegisterBy() {
		return registerBy;
	}

	public void setRegisterBy(String registerBy) {
		this.registerBy = registerBy;
	}

	@Column(name = "register_date")
	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	@Column(name = "status", nullable = false, length = 10)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "type", length = 10)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "responsible")
	public Integer getResponsible() {
		return responsible;
	}

	public void setResponsible(Integer responsible) {
		this.responsible = responsible;
	}

	@Column(name = "apply_amount", scale = 1, length = 10)
	public BigDecimal getApplyAmount() {
		return applyAmount;
	}

	public void setApplyAmount(BigDecimal applyAmount) {
		this.applyAmount = applyAmount;
	}

	@Column(name = "real_amount", scale = 1, length = 10)
	public BigDecimal getRealAmount() {
		return realAmount;
	}

	public void setRealAmount(BigDecimal realAmount) {
		this.realAmount = realAmount;
	}

	@Column(name = "ins_company", length = 32)
	public String getInsCompany() {
		return insCompany;
	}

	public void setInsCompany(String insCompany) {
		this.insCompany = insCompany;
	}

	@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}