package modules.safe.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.safe.service.AccidentService;

/**
 * 
 * @author zzc
 *
 */
@Service("accidentService")
@Transactional
public class AccidentServiceImpl extends CommonServiceImpl implements AccidentService {

}
