package modules.fin.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 费用类型
 * 
 * @author zzc
 */
@Entity
@Table(name = "fin_expenses")
@DynamicInsert
@DynamicUpdate
public class Expenses implements Serializable {

	private static final long serialVersionUID = 5916810888468872105L;

	public static final String TYPE_INCOME = "1";// 收车辆款
	public static final String TYPE_OUTLAY = "2";// 付车辆款
	public static final String TYPE_INCOME_UNIT = "3";// 收单位款
	public static final String TYPE_OUTLAY_UNIT = "4";// 付单位款
	public static final String STATUS_ENABLE = "0";// 启用
	public static final String STATUS_DISABLE = "1";// 禁用

	private String id;
	/** 租户 */
	private String tenantId;
	/** 名称 */
	private String name;
	/** 应用范围 */
	private String type;
	/** 费用标准 */
	private BigDecimal amount;
	/** 状态 */
	private String status;
	/** 排序 */
	private Integer sort;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "amount", scale = 1, length = 10)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "应用范围不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "sort")
	@Min(0)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}