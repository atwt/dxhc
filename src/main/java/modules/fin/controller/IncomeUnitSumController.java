package modules.fin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.LoginUserUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.fin.entity.IncomeUnitSum;
import modules.fin.service.IncomeUnitService;
import modules.sys.entity.Dict;
import modules.sys.entity.Log;
import modules.sys.service.DictService;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/incomeUnitSumController")
public class IncomeUnitSumController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IncomeUnitSumController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private IncomeUnitService incomeUnitService;
	@Autowired
	private DictService dictService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/fin/incomeUnitSum-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param incomeUnitSum
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(IncomeUnitSum incomeUnitSum, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(IncomeUnitSum.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, incomeUnitSum, request.getParameterMap());
			cq.add();
			incomeUnitService.listByPage(cq, true);
			dataGrid.setFooter("amount,sn:合计");
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("收款记录查询失败", e);
			throw new BusinessException("收款记录查询失败", e);
		}
	}

	/**
	 * 详情页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goDetail")
	public ModelAndView goDetail(IncomeUnitSum incomeUnitSum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(incomeUnitSum.getId())) {
			incomeUnitSum = incomeUnitService.get(IncomeUnitSum.class, incomeUnitSum.getId());
			req.setAttribute("incomeUnitSumPage", incomeUnitSum);
		}
		return new ModelAndView("modules/fin/incomeUnitSum-detail");
	}

	/**
	 * 打印页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goPrint")
	public ModelAndView goPrint(IncomeUnitSum incomeUnitSum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(incomeUnitSum.getId())) {
			incomeUnitSum = incomeUnitService.get(IncomeUnitSum.class, incomeUnitSum.getId());
			Dict unitDict = dictService.get(Dict.class, incomeUnitSum.getUnit());
			if (unitDict != null) {
				req.setAttribute("unit", unitDict.getName());
			}
			req.setAttribute("incomeUnitSumPage", incomeUnitSum);
		}
		return new ModelAndView("modules/fin/incomeUnitSum-print");
	}

	/**
	 * 核销明细
	 * 
	 * @return
	 */
	@RequestMapping(params = "settleTab")
	public ModelAndView settleTab(HttpServletRequest req) {
		String id = req.getParameter("id");
		IncomeUnitSum incomeUnitSum = incomeUnitService.get(IncomeUnitSum.class, id);
		req.setAttribute("totalAmount", incomeUnitSum.getAmount());
		req.setAttribute("incomeUnitList", incomeUnitSum.getIncomeUnitList());
		return new ModelAndView("modules/fin/incomeUnitSumDetail-tab");
	}

	/**
	 * 作废
	 * 
	 * @param incomeUnitSum
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doVoid")
	@ResponseBody
	public Result doVoid(IncomeUnitSum incomeUnitSum, HttpServletRequest request) {
		String msg = null;
		try {
			incomeUnitSum = incomeUnitService.get(IncomeUnitSum.class, incomeUnitSum.getId());
			incomeUnitSum.setStatus(IncomeUnitSum.STATUS_DISABLE);
			incomeUnitSum.setVoidBy(LoginUserUtil.getUser().getName());
			incomeUnitSum.setVoidDate(new Date());
			incomeUnitService.doVoid(incomeUnitSum);
			msg = "编号\"" + incomeUnitSum.getSn() + "\"收款作废成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "编号\"" + incomeUnitSum.getSn() + "\"收款作废失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
