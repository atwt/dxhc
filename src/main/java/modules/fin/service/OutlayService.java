package modules.fin.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.fin.entity.Outlay;
import modules.fin.entity.OutlaySum;

public interface OutlayService extends CommonService {

	void settle(OutlaySum outlaySum);

	void doVoid(OutlaySum outlaySum);

	List<Outlay> listByVehicleAndStatus(String vehicleId, String status);

	List<Outlay> listByVehicle(String vehicleId);

	List<Outlay> listByOwnerAndStatus(String ownerId, String status);

	List<OutlaySum> listByOwner(String ownerId);

	List<Outlay> listByExpenses(String expensesId);

	String getSn();
}
