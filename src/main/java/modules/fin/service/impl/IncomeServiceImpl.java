package modules.fin.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.DateUtil;
import luxing.util.LoginUserUtil;
import modules.fin.entity.Income;
import modules.fin.entity.IncomeSum;
import modules.fin.service.IncomeService;

/**
 * 
 * @author zzc
 *
 */
@Service("incomeService")
@Transactional
public class IncomeServiceImpl extends CommonServiceImpl implements IncomeService {

	public void settle(IncomeSum incomeSum) {
		save(incomeSum);
		for (Income income : incomeSum.getIncomeList()) {
			// 核销时间 核销人
			income.setSettleBy(LoginUserUtil.getUser().getName());
			income.setSettleDate(new Date());
			income.setIncomeSum(incomeSum);
			saveOrUpdate(income);
		}
	}

	public void doVoid(IncomeSum incomeSum) {
		for (Income income : incomeSum.getIncomeList()) {
			income.setIncomeSum(null);
			income.setSettleBy(null);
			income.setSettleDate(null);
			income.setDiscountAmount(new BigDecimal(0));
			income.setSettleAmount(new BigDecimal(0));
			income.setStatus(Income.STATUS_NO);
			super.saveOrUpdate(income);
		}
		super.saveOrUpdate(incomeSum);
	}

	public List<Income> listByVehicleAndStatus(String vehicleId, String status) {
		String hql = "from Income where vehicle.id=? and status=? and tenantId=?";
		return listByHql(hql, vehicleId, status, LoginUserUtil.getLoginUser().getTenantId());
	}

	public List<Income> listByOwnerAndStatus(String ownerId, String status) {
		String hql = "from Income where vehicle.owner.id=? and status=? and tenantId=?";
		return listByHql(hql, ownerId, status, LoginUserUtil.getLoginUser().getTenantId());
	}

	public List<IncomeSum> listByOwner(String ownerId) {
		String hql = "from IncomeSum where owner.id=?";
		return listByHql(hql, ownerId);
	}

	public List<Income> listByExpenses(String expensesId) {
		String hql = "from Income where expenses.id=?  and tenantId=?";
		return listByHql(hql, expensesId, LoginUserUtil.getLoginUser().getTenantId());
	}

	public String getSn() {
		String hql = "from IncomeSum where date(createDate)=? and tenantId=? order by sn desc";
		List<IncomeSum> incomeSumList = listByHql(hql, new Date(), LoginUserUtil.getLoginUser().getTenantId());
		String sn = "SK" + DateUtil.dateToStr(new Date(), "yyyyMMdd") + "001";
		if (CollectionUtils.isNotEmpty(incomeSumList)) {
			String endNum = incomeSumList.get(0).getSn().substring(10, 13);
			if (StringUtils.isNumeric(endNum)) {
				endNum = String.valueOf(Integer.valueOf(endNum).intValue() + 1);
				if (endNum.length() == 1) {
					endNum = "00" + endNum;
				} else if (endNum.length() == 2) {
					endNum = "0" + endNum;
				}
				sn = "SK" + DateUtil.dateToStr(new Date(), "yyyyMMdd") + endNum;
				return sn;
			}
		}
		return sn;
	}

}
