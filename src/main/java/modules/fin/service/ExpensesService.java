package modules.fin.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.fin.entity.Expenses;

public interface ExpensesService extends CommonService {

	List<Expenses> listByTypeAndStatus(String type, String status);

}
