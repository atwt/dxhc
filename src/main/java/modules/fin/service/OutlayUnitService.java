package modules.fin.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.fin.entity.OutlayUnit;
import modules.fin.entity.OutlayUnitSum;

public interface OutlayUnitService extends CommonService {

	void settle(OutlayUnitSum outlayUnitSum);

	void doVoid(OutlayUnitSum outlayUnitSum);

	List<OutlayUnit> listByUnitAndExpensesAndStatus(String unit, String expensesId, String status);

	List<OutlayUnit> listByExpenses(String expensesId);

	String getSn();

}
