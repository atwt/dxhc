package modules.arc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * 车辆档案
 * 
 * @author zzc
 */
@Entity
@Table(name = "arc_vehicle")
@DynamicInsert
@DynamicUpdate
public class Vehicle implements Serializable {

	private static final long serialVersionUID = 8653750861485859276L;

	public static final String TYPE_TRACTOR = "1";// 牵引车
	public static final String TYPE_TRAILER = "2";// 挂车
	public static final String TYPE_WHOLE = "3";// 整车
	public static final String GENERAL = "1";// 普货
	public static final String DANGER = "2";// 危货
	public static final String STATUS_DISABLE = "-1";// 注销

	private String id;
	/** 租户 */
	private String tenantId;
	/** 责任人 */
	private Owner owner;
	/** 编号 */
	@Excel(name = "编号")
	private String sn;
	/** 车牌 */
	@Excel(name = "车牌")
	private String plateNumber;
	/** 车辆类型 */
	@Excel(name = "类型 ", replace = { "牵引车_1", "挂车_2", "整车_3" })
	private String type;
	/** 状态 */
	private String status;
	/** 普货/危货 */
	private String generalDanger;
	/** 登记日期 */
	private Date registerDate;
	/** 关联车辆 */
	private String relationVehicleId;
	/** 备注 */
	private String remarks;
	/** 司机集合 */
	private List<Driver> driverList = new ArrayList<Driver>();
	/** 创建时间 */
	private Date createDate;
	/** 创建人ID */
	private String createBy;
	/** 修改时间 */
	private Date updateDate;
	/** 修改人 */
	private String updateBy;
	/** 注销时间 */
	private Date voidDate;
	/** 注销人 */
	private String voidBy;

	public Vehicle() {
		super();
	}

	public Vehicle(String id) {
		super();
		this.id = id;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Column(name = "sn", length = 50)
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "plate_number", nullable = false, length = 50)
	@NotBlank(message = "车牌不能为空")
	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "类型不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "status", nullable = false, length = 32)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "general_danger", nullable = false, length = 10)
	@NotBlank(message = "普货/危货不能为空")
	public String getGeneralDanger() {
		return generalDanger;
	}

	public void setGeneralDanger(String generalDanger) {
		this.generalDanger = generalDanger;
	}

	@Column(name = "register_date")
	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "relation_vehicle_id", length = 32)
	public String getRelationVehicleId() {
		return relationVehicleId;
	}

	public void setRelationVehicleId(String relationVehicleId) {
		this.relationVehicleId = relationVehicleId;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinTable(name = "arc_vehicle_driver", joinColumns = { @JoinColumn(name = "vehicle_id") }, inverseJoinColumns = {
			@JoinColumn(name = "driver_id") })
	@Fetch(FetchMode.SUBSELECT)
	public List<Driver> getDriverList() {
		return driverList;
	}

	public void setDriverList(List<Driver> driverList) {
		this.driverList = driverList;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "update_date", nullable = true)
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "update_by", nullable = true, length = 32)
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	@Column(name = "void_date", nullable = true)
	public Date getVoidDate() {
		return voidDate;
	}

	public void setVoidDate(Date voidDate) {
		this.voidDate = voidDate;
	}

	@Column(name = "void_by", nullable = true, length = 32)
	public String getVoidBy() {
		return voidBy;
	}

	public void setVoidBy(String voidBy) {
		this.voidBy = voidBy;
	}

}