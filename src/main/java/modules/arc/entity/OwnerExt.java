package modules.arc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * 档案扩展
 * 
 * @author zzc
 */
@Entity
@Table(name = "arc_owner_ext")
@DynamicInsert
@DynamicUpdate
public class OwnerExt implements Serializable {

	private static final long serialVersionUID = 3295512187627115763L;

	private String id;
	/** 租户 */
	private String tenantId;
	/** 责任人 */
	private String ownerId;
	/** 扩展字段 */
	private String arcExtId;
	/** 值 */
	private String value;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "owner_id", nullable = false, length = 32)
	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	@Column(name = "arc_ext_id", nullable = false, length = 32)
	public String getArcExtId() {
		return arcExtId;
	}

	public void setArcExtId(String arcExtId) {
		this.arcExtId = arcExtId;
	}

	@Column(name = "value", nullable = false, length = 50)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}