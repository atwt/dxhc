package modules.arc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * 档案扩展
 * 
 * @author zzc
 */
@Entity
@Table(name = "arc_vehicle_ext")
@DynamicInsert
@DynamicUpdate
public class VehicleExt implements Serializable {

	private static final long serialVersionUID = 8233672706684160587L;

	private String id;
	/** 租户 */
	private String tenantId;
	/** 车辆 */
	private String vehicleId;
	/** 扩展字段 */
	private String arcExtId;
	/** 值 */
	private String value;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "vehicle_id", nullable = false, length = 32)
	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	@Column(name = "arc_ext_id", nullable = false, length = 32)
	public String getArcExtId() {
		return arcExtId;
	}

	public void setArcExtId(String arcExtId) {
		this.arcExtId = arcExtId;
	}

	@Column(name = "value", nullable = false, length = 50)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}