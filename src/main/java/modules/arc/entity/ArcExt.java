package modules.arc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 档案扩展
 * 
 * @author zzc
 */
@Entity
@Table(name = "arc_ext")
@DynamicInsert
@DynamicUpdate
public class ArcExt implements Serializable {

	private static final long serialVersionUID = 8452768744018622500L;

	public static final String TYPE_VEHICLE = "1";// 车辆
	public static final String TYPE_OWNER = "2";// 责任人
	public static final String TYPE_DRIVER = "3";// 司机
	public static final String TYPE_OWNER_DRIVER = "4";// 责任人兼司机(页面选择,不入数据库 )

	private String id;
	/** 租户 */
	private String tenantId;
	/** 名称 */
	private String name;
	/** 档案类型 */
	private String type;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "类型不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}