package modules.arc.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import luxing.common.service.CommonService;
import modules.arc.entity.ArcExt;
import modules.arc.entity.Driver;
import modules.arc.entity.Owner;
import modules.arc.entity.OwnerExt;

public interface OwnerService extends CommonService {

	void save(Owner owner, List<ArcExt> ownerArcExtList, Driver driver, List<ArcExt> driverArcExtList,
			HttpServletRequest request);

	void save(Owner owner, List<ArcExt> ownerArcExtList, HttpServletRequest request);

	void update(Owner owner, List<OwnerExt> ownerExtList);

	void delete(Owner owner);

	Owner getByName(String name);

	Owner getByPhone(String phone);

	Owner getByIdCard(String idCard);
}
