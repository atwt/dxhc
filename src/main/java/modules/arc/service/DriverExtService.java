package modules.arc.service;

import java.util.Map;

import luxing.common.service.CommonService;

public interface DriverExtService extends CommonService {

	Map<String, String> mapByDriver(String driverId);
}
