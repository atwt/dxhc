package modules.arc.service;

import java.util.Map;

import luxing.common.service.CommonService;

public interface OwnerExtService extends CommonService {

	Map<String, String> mapByOwner(String ownerId);
}
