package modules.arc.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.VehicleWarn;
import modules.arc.service.VehicleWarnService;
import modules.warn.entity.VehicleWarnHis;

/**
 * @author zzc
 */
@Service("vehicleWarnService")
@Transactional
public class VehicleWarnServiceImpl extends CommonServiceImpl implements VehicleWarnService {

    public Map<String, Date> mapByVehicle(String vehicleId) {
        Map<String, Date> dateMap = new HashMap<>();
        String hql = "from VehicleWarn where vehicle.id=? and tenantId=?";
        List<VehicleWarn> list = listByHql(hql, vehicleId, LoginUserUtil.getLoginUser().getTenantId());
        for (VehicleWarn vehicleWarn : list) {
            dateMap.put(vehicleWarn.getWarnType().getId(), vehicleWarn.getWarnDate());
        }
        return dateMap;
    }

    public List<VehicleWarn> listByWarnTypeAndStatus(String warnType, String status) {
        String hql = "from VehicleWarn where warnType.id=? and status=? and tenantId=?";
        return listByHql(hql, warnType, status, LoginUserUtil.getLoginUser().getTenantId());

    }

    public void save(List<VehicleWarn> list) {
        String hql = "delete from VehicleWarn where vehicle.id=? and warnType.id=? and tenantId=?";
        for (VehicleWarn vehicleWarn : list) {
            super.executeHql(hql, vehicleWarn.getVehicle().getId(), vehicleWarn.getWarnType().getId(),
                    LoginUserUtil.getLoginUser().getTenantId());
        }
        super.batchSave(list);
    }

    public void handle(VehicleWarn vehicleWarn, VehicleWarnHis vehicleWarnHis) {
        super.saveOrUpdate(vehicleWarn);
        super.save(vehicleWarnHis);
    }

}
