package modules.arc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.DriverExt;
import modules.arc.service.DriverExtService;

/**
 * 
 * @author zzc
 *
 */
@Service("driverExtService")
@Transactional
public class DriverExtServiceImpl extends CommonServiceImpl implements DriverExtService {

	public Map<String, String> mapByDriver(String driverId) {
		Map<String, String> valueMap = new HashMap<>();
		String hql = "from DriverExt where tenantId=? and driverId=?";
		List<DriverExt> list = listByHql(hql, LoginUserUtil.getLoginUser().getTenantId(), driverId);
		for (DriverExt driverExt : list) {
			valueMap.put(driverExt.getArcExtId(), driverExt.getValue());
		}
		return valueMap;
	}

}
