package modules.arc.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aliyun.oss.OSSClient;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.constant.StorageConfig;
import luxing.util.DateUtil;
import luxing.util.LoginUserUtil;
import modules.arc.entity.ArcExt;
import modules.arc.entity.Vehicle;
import modules.arc.entity.VehicleExt;
import modules.arc.entity.VehiclePic;
import modules.arc.entity.VehicleWarn;
import modules.arc.service.VehicleService;
import modules.ins.entity.Insurance;
import modules.warn.entity.WarnType;

/**
 * 
 * @author zzc
 *
 */
@Service("vehicleService")
@Transactional
public class VehicleServiceImpl extends CommonServiceImpl implements VehicleService {

	public void save(Vehicle vehicle, List<ArcExt> arcExtList, List<WarnType> warnTypeList,
			HttpServletRequest request) {
		// 车辆
		save(vehicle);
		// 扩展属性
		for (ArcExt arcExt : arcExtList) {
			String value = request.getParameter(arcExt.getId());
			if (StringUtils.isNotBlank(value)) {
				VehicleExt vehicleExt = new VehicleExt();
				vehicleExt.setVehicleId(vehicle.getId());
				vehicleExt.setArcExtId(arcExt.getId());
				vehicleExt.setValue(value);
				save(vehicleExt);
			}
		}
		// 到期提醒
		for (WarnType warnType : warnTypeList) {
			String value = request.getParameter(warnType.getId());
			if (StringUtils.isNotBlank(value)) {
				VehicleWarn vehicleWarn = new VehicleWarn();
				vehicleWarn.setVehicle(vehicle);
				vehicleWarn.setWarnType(warnType);
				vehicleWarn.setWarnDate(DateUtil.strToDate(value, "yyyy-MM-dd"));
				vehicleWarn.setStatus(VehicleWarn.STATUS_NORMAL);
				save(vehicleWarn);
			}
		}
		// 关联车辆
		if (StringUtils.isNotBlank(vehicle.getRelationVehicleId())) {
			Vehicle relationVehicle = get(Vehicle.class, vehicle.getRelationVehicleId());
			relationVehicle.setRelationVehicleId(vehicle.getId());
			super.saveOrUpdate(relationVehicle);
		}

	}

	public void update(Vehicle vehicle, List<VehicleExt> vehicleExtList) {
		// 车辆
		saveOrUpdate(vehicle);
		// 关联车辆
		if (StringUtils.isNotBlank(vehicle.getRelationVehicleId())) {
			Vehicle relationVehicle = get(Vehicle.class, vehicle.getRelationVehicleId());
			relationVehicle.setRelationVehicleId(vehicle.getId());
			super.saveOrUpdate(relationVehicle);
		}
		// 扩展属性,先删除再重新增加
		String hql = "delete from VehicleExt where vehicleId=?";
		executeHql(hql, vehicle.getId());
		for (VehicleExt vehicleExt : vehicleExtList) {
			save(vehicleExt);
		}
	}

	public void delete(Vehicle vehicle) {
		// 解除关联车辆
		String relationHql = "update Vehicle set relationVehicleId=null where relationVehicleId=? and tenantId=?";
		executeHql(relationHql, vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
		// 删除扩展信息
		String extHql = "delete from VehicleExt where vehicleId=? and tenantId=?";
		executeHql(extHql, vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
		// 删除提醒信息
		String warnHql = "delete from VehicleWarn where vehicle.id=? and tenantId=?";
		executeHql(warnHql, vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
		// 删除收车辆款信息
		String incomeHql = "delete from Income where vehicle.id=? and tenantId=?";
		executeHql(incomeHql, vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
		// 删除付车辆款信息
		String outlayHql = "delete from Outlay where vehicle.id=? and tenantId=?";
		executeHql(outlayHql, vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
		// 删除保险信息
		String insuranceHql = "delete from Insurance where vehicle.id=? and tenantId=?";
		executeHql(insuranceHql, vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
		// 删除图片信息
		String vehiclePicHql = "from VehiclePic where vehicle.id=? and tenantId=?";
		List<VehiclePic> vehiclePicList = listByHql(vehiclePicHql, vehicle.getId(),
				LoginUserUtil.getLoginUser().getTenantId());
		OSSClient client = new OSSClient(StorageConfig.endPoint, StorageConfig.accessKeyId,
				StorageConfig.accessKeySecret);
		for (VehiclePic vehiclePic : vehiclePicList) {
			client.deleteObject(StorageConfig.bucketVehicle, vehiclePic.getThumbPath());
			client.deleteObject(StorageConfig.bucketVehicle, vehiclePic.getPath());
			super.delete(vehiclePic);
		}
		// 删除车辆
		super.delete(vehicle);
	}

	public Vehicle getByPlateNumber(String plateNumber) {
		String hql = "from Vehicle where plateNumber=? and tenantId=?";
		return getUniqueByHql(hql, plateNumber, LoginUserUtil.getLoginUser().getTenantId());
	}

	public List<Vehicle> listEnableBySn(String sn) {
		String hql = "from Vehicle where sn=? and status!=? and tenantId=?";
		return listByHql(hql, sn, Vehicle.STATUS_DISABLE, LoginUserUtil.getLoginUser().getTenantId());
	}

	public void disable(Vehicle vehicle) {
		// 暂停车辆提醒
		String vehicleWarnHql = "update VehicleWarn set status=? where vehicle.id=? and status=? and tenantId=?";
		executeHql(vehicleWarnHql, VehicleWarn.STATUS_PAUSE, vehicle.getId(), VehicleWarn.STATUS_NORMAL,
				LoginUserUtil.getLoginUser().getTenantId());
		// 暂停保险提醒
		String insuranceHql = "update Insurance set warn=? where vehicle.id=? and warn=? and tenantId=?";
		executeHql(insuranceHql, Insurance.WARN_PAUSE, vehicle.getId(), Insurance.WARN_NORMAL,
				LoginUserUtil.getLoginUser().getTenantId());
		// 注销车辆
		String hql = "update Vehicle set plateNumber=?,status=?,remarks=? where id=? and tenantId=?";
		executeHql(hql, vehicle.getPlateNumber().concat("(注销)"), Vehicle.STATUS_DISABLE, vehicle.getRemarks(),
				vehicle.getId(), LoginUserUtil.getLoginUser().getTenantId());
	}

}
