package modules.arc.service;

import java.util.Map;

import luxing.common.service.CommonService;

public interface VehicleExtService extends CommonService {

	Map<String, String> mapByVehicle(String vehicleId);
}
