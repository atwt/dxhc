package modules.arc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.ValidatorUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.arc.entity.ArcExt;
import modules.arc.service.ArcExtService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/arcExtController")
public class ArcExtController extends BaseController {

	private static final Logger logger = Logger.getLogger(ArcExtController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private ArcExtService arcExtService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/arc/arcExt-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param arcExt
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(ArcExt arcExt, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(ArcExt.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, arcExt, request.getParameterMap());
			cq.add();
			arcExtService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("档案扩展查询失败", e);
			throw new BusinessException("档案扩展查询失败", e);
		}
	}

	/**
	 * 增加页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(HttpServletRequest req) {
		return new ModelAndView("modules/arc/arcExt-add");
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(ArcExt arcExt, HttpServletRequest req) {
		if (StringUtils.isNotBlank(arcExt.getId())) {
			arcExt = arcExtService.get(ArcExt.class, arcExt.getId());
			req.setAttribute("arcExtPage", arcExt);
		}
		return new ModelAndView("modules/arc/arcExt-update");
	}

	/**
	 * 增加
	 * 
	 * @param arcExt
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public Result doAdd(ArcExt arcExt, HttpServletRequest request) {
		String msg = null;
		try {
			// bean校验
			msg = ValidatorUtil.validate(arcExt);
			if (StringUtils.isNotBlank(msg)) {
				return Result.error(msg);
			}
			arcExtService.save(arcExt);
			msg = "档案扩展\"" + arcExt.getName() + "\"增加成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_ADD);
		} catch (Exception e) {
			msg = "档案扩展\"" + arcExt.getName() + "\"增加失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 修改
	 * 
	 * @param arcExt
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public Result doUpdate(ArcExt arcExt, HttpServletRequest request) {
		String msg = null;
		try {
			arcExtService.saveOrUpdate(arcExt);
			msg = "档案扩展\"" + arcExt.getName() + "\"修改成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "档案扩展\"" + arcExt.getName() + "\"修改失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public Result doDel(ArcExt arcExt, HttpServletRequest request) {
		String msg = null;
		try {
			arcExt = arcExtService.get(ArcExt.class, arcExt.getId());
			arcExtService.delete(arcExt);
			msg = "档案扩展\"" + arcExt.getName() + "\"删除成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_DEL);
		} catch (Exception e) {
			msg = "档案扩展\"" + arcExt.getName() + "\"删除失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
